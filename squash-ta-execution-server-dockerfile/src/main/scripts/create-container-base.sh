#!/bin/sh
#
#     This file is part of the Squashtest platform.
#     Copyright (C) 2011 - 2020 Henix
#
#     See the NOTICE file distributed with this work for additional
#     information regarding copyright ownership.
#
#     This is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Lesser General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     this software is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Lesser General Public License for more details.
#
#     You should have received a copy of the GNU Lesser General Public License
#     along with this software.  If not, see <http://www.gnu.org/licenses />.
#


use_message()
{
	echo $(basename $0) [options] >&2
	echo options are key-value pairs as follows: >&2
	echo  --name \<container name\> >&2
	echo  --image \<image name\> >&2
	echo  --repository \<external repository path\> >&2
}

if [ "$1" = "--help" ]
then
    use_message
    exit 0
fi

while [ $# -ge 2 ]
do
	echo $@
	case $1 in
		--image)
			image=$2
			shift
			;;
		--name)
			name=$2
			shift
			;;
		--repository)
			repository=$2
			shift
			;;
		*)
			echo "Unexpected $1 qualifier"
			use_message
			exit 2
			;;
	esac
	shift
done

echo $@

if [ $# -gt 0 ]; then
	echo "Unrecognized parameter $1"
	use_message
	exit 2
fi

if [ "$image" = "" ]; then
	image=squash/squash-tf-execution-server:latest-SNAPSHOT
fi

if [ "$name" = "" ]; then
	name=Armando
fi

if [ "$repository" = "" ]; then
	repository=$HOME/.m2/repository
fi

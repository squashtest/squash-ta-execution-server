#
#     This file is part of the Squashtest platform.
#     Copyright (C) 2011 - 2020 Henix
#
#     See the NOTICE file distributed with this work for additional
#     information regarding copyright ownership.
#
#     This is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Lesser General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     this software is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Lesser General Public License for more details.
#
#     You should have received a copy of the GNU Lesser General Public License
#     along with this software.  If not, see <http://www.gnu.org/licenses />.
#

FROM openjdk:8u171-jdk-stretch

ADD squash-tf-execution-server-linux-installer.jar /tmp/squash-tf-execution-server-linux-installer.jar

ADD autoinstall.xml /tmp/autoinstall.xml

RUN mkdir -p /opt/squash-ta-server/ ; chmod a+x /opt/ ; chmod a+rwx /opt/squash-ta-server

RUN java -jar /tmp/squash-tf-execution-server-linux-installer.jar /tmp/autoinstall.xml

ADD entrypoint.sh /opt/squash-ta-server/scripts/

ENTRYPOINT /opt/squash-ta-server/scripts/entrypoint.sh /opt/squash-ta-server/scripts/

EXPOSE 8080/tcp

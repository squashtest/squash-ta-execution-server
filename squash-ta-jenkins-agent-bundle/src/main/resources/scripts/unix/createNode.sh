#!/bin/sh
#
#     This file is part of the Squashtest platform.
#     Copyright (C) 2011 - 2020 Henix
#
#     See the NOTICE file distributed with this work for additional
#     information regarding copyright ownership.
#
#     This is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Lesser General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     this software is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Lesser General Public License for more details.
#
#     You should have received a copy of the GNU Lesser General Public License
#     along with this software.  If not, see <http://www.gnu.org/licenses />.
#

export JENKINS_URL=${jenkins.url}
export JENKINS_USER=${jenkins.username}
export JENKINS_PWD=${jenkins.password}

echo Unix - Squash TF agent creating on $HOSTNAME
cat configAgent.xml | java -jar ../libs/jenkins-cli.jar -http -s $JENKINS_URL -auth $JENKINS_USER:$JENKINS_PWD create-node 

export NODE_NAME=$(java -jar "../libs/squash-ta-jenkins-agent-helpers.jar" "getNode")
export JENKINS_URL=$JENKINS_URL"/"

export NODE_URL=$JENKINS_URL"computer/"$NODE_NAME"/slave-agent.jnlp"
echo $NODE_URL

export SECRET_KEY=$(java -jar "../libs/squash-ta-jenkins-agent-helpers.jar" "getSecret" "$NODE_URL" "$JENKINS_USER:$JENKINS_PWD")
echo $SECRET_KEY

head -n 29 parm.sh > parmTemp.sh
mv parmTemp.sh parm.sh

echo >>parm.sh
echo export NODE_NAME=$NODE_NAME>>parm.sh
echo export NODE_URL=$NODE_URL>>parm.sh
echo export NODE_WD=${INSTALL_PATH}/workspace>>parm.sh
echo export SECRET_KEY=$SECRET_KEY>>parm.sh

chmod +x launch.sh

for file in createNode.sh configAgent.xml; do
    rm $(dirname $0)/$file
done
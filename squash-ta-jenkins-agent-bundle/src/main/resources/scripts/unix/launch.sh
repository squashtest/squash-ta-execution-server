#
#     This file is part of the Squashtest platform.
#     Copyright (C) 2011 - 2020 Henix
#
#     See the NOTICE file distributed with this work for additional
#     information regarding copyright ownership.
#
#     This is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Lesser General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     this software is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Lesser General Public License for more details.
#
#     You should have received a copy of the GNU Lesser General Public License
#     along with this software.  If not, see <http://www.gnu.org/licenses />.
#

export SQUASH_TA_HOME=$(cd $(dirname $0)/..;pwd)
. $(dirname $0)/parm.sh
export PATH=$SQUASH_TA_HOME/@subdir.maven@/bin:$PATH
export PATH=@jdkPath@/bin:$PATH
java -jar $(dirname $0)/../libs/agent.jar -jnlpUrl $NODE_URL -secret $SECRET_KEY -workDir $NODE_WD

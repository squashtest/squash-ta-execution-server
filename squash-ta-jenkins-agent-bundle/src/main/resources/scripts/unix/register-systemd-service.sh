#!/bin/sh
#
#     This file is part of the Squashtest platform.
#     Copyright (C) 2011 - 2020 Henix
#
#     See the NOTICE file distributed with this work for additional
#     information regarding copyright ownership.
#
#     This is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Lesser General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     this software is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Lesser General Public License for more details.
#
#     You should have received a copy of the GNU Lesser General Public License
#     along with this software.  If not, see <http://www.gnu.org/licenses />.
#


OLD_CD=$(pwd)
cd $(dirname $0/..)
export SQUASH_TA_HOME=$(pwd)
cd $OLD_CD

cat > ./squash-tf-agent.service <<EOF
[Unit]
Description=Squash TF Execution Agent
After=network-online.target
[Install]
WantedBy=multi-user.target
[Service]
Type=simple
ExecStart=$SQUASH_TA_HOME/scripts/launch.sh
User=jenkins-agent
EOF

sudofile=$(which sudo)

if [ "$sudofile" != "" -a -x $sudofile ]; then
  sudo systemctl disable squash-tf-agent || echo 'No previous unit description found'
  sudo systemctl enable $(dirname $0)/squash-tf-agent.service
  sudo systemctl daemon-reload
  sudo systemctl start squash-tf-agent
else
  systemctl disable squash-tf-agent || echo 'No previous unit description found'
  systemctl enable $(dirname $0)/squash-tf-agent.service
  systemctl daemon-reload
  systemctl start squash-tf-agent
fi
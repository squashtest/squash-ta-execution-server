@REM
@REM     This file is part of the Squashtest platform.
@REM     Copyright (C) 2011 - 2020 Henix
@REM
@REM     See the NOTICE file distributed with this work for additional
@REM     information regarding copyright ownership.
@REM
@REM     This is free software: you can redistribute it and/or modify
@REM     it under the terms of the GNU Lesser General Public License as published by
@REM     the Free Software Foundation, either version 3 of the License, or
@REM     (at your option) any later version.
@REM
@REM     this software is distributed in the hope that it will be useful,
@REM     but WITHOUT ANY WARRANTY; without even the implied warranty of
@REM     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
@REM     GNU Lesser General Public License for more details.
@REM
@REM     You should have received a copy of the GNU Lesser General Public License
@REM     along with this software.  If not, see <http://www.gnu.org/licenses />.
@REM

@echo off

@rem This file is part of the Squashtest platform. Copyright (C) 2019 Henix 
@rem See the NOTICE file distributed with this work for additional 
@rem information regarding copyright ownership. All rights reserved, please contact 
@rem Henix to get a license. 

IF "%OS%" == "Windows_NT" setlocal
setLocal EnableDelayedExpansion
call parm.cmd
IF "%SQUASH-TA_HOME%" == "" SET SQUASH-TA_HOME=%SQUASH_TA_WINS_SLAVE%
IF "%SERVICE_LOG_DIR%" == "" SET SERVICE_LOG_DIR=%SQUASH-TA_HOME%\logs

IF NOT EXIST "%SERVICE_LOG_DIR%" MKDIR "%SERVICE_LOG_DIR%"

IF %1 == start GOTO doStart
IF %1 == stop GOTO doStop

:doStart
:: Start the service
echo Starting the service 'Forge-Agent' ...
call %SQUASH-TA_HOME%\launch.cmd /
EXIT /B 0

:doStop
:: Stop the service
echo Stopping the service 'Squash-TA' ...
set /p JNLPPID=<%SERVICE_LOG_DIR%\squash-tf-agent.pid
echo parent PID is %JNLPPID%
wmic PROCESS where (ParentPRocessId=%JNLPPID% and Caption='java.exe') call terminate
taskkill /im %SQUASH-TA_HOME%\launch.cmd
exit /B 0

:end
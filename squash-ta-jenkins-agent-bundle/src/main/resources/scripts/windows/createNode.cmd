@REM
@REM     This file is part of the Squashtest platform.
@REM     Copyright (C) 2011 - 2020 Henix
@REM
@REM     See the NOTICE file distributed with this work for additional
@REM     information regarding copyright ownership.
@REM
@REM     This is free software: you can redistribute it and/or modify
@REM     it under the terms of the GNU Lesser General Public License as published by
@REM     the Free Software Foundation, either version 3 of the License, or
@REM     (at your option) any later version.
@REM
@REM     this software is distributed in the hope that it will be useful,
@REM     but WITHOUT ANY WARRANTY; without even the implied warranty of
@REM     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
@REM     GNU Lesser General Public License for more details.
@REM
@REM     You should have received a copy of the GNU Lesser General Public License
@REM     along with this software.  If not, see <http://www.gnu.org/licenses />.
@REM

@echo off

call ./nodeCreationParm.cmd
del nodeCreationParm.cmd

echo "Windows - Squash TF agent creating on %HOSTNAME%"
echo "[INFO]----------%JENKINS_URL%----------"
type configAgent.xml | java -jar "../libs/jenkins-cli.jar" -http -s %JENKINS_URL% -auth %JENKINS_USER%:%JENKINS_PWD% create-node

for /f "tokens=* USEBACKQ" %%i IN (`java -jar "../libs/squash-ta-jenkins-agent-helpers.jar" "getNode"`) DO (
set NODE_NAME=%%i)

set JENKINS_URL=%JENKINS_URL%/

set NODE_URL=%JENKINS_URL%computer/%NODE_NAME%/slave-agent.jnlp
echo %NODE_URL%


for /f "tokens=* USEBACKQ" %%i IN (`java -jar "../libs/squash-ta-jenkins-agent-helpers.jar" "getSecret" "%NODE_URL%" "%JENKINS_USER%:%JENKINS_PWD%"`) DO (
set SECRET_KEY=%%i)
echo %SECRET_KEY%

findstr /v NODE parm.cmd > parmTemp.cmd
findstr /v SECRET parmTemp.cmd > parm.cmd
del parmTemp.cmd

echo set NODE_NAME=%NODE_NAME%>>parm.cmd
echo set NODE_URL=%NODE_URL%>>parm.cmd
echo set NODE_WD=${INSTALL_PATH}\workspace>>parm.cmd
echo set SECRET_KEY=%SECRET_KEY%>>parm.cmd

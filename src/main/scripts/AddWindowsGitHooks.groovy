/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */

/* This may grit Groovy purist's teeth, but way way less than those cabalistic "no such property messages for missing classes do grit mine. */
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.attribute.PosixFilePermission

def gitRepo = new File(".git/hooks")
def preCommitHook = new File("pre-commit", gitRepo)

def lines = []

if (preCommitHook.exists()) {
    preCommitHook.eachLine { line -> lines << line }
} else {
    preCommitHook << "#!/bin/sh\n"
}

containsPreCommit = lines.find { it ==~ /^\s*\. src\/main\/scripts\/precommit.sh\s*/ }

if (containsPreCommit) {
    println "precommit hook found, nothing to do"
    return
}

println "no precommit hook, will add hook definition"

def old = new File("pre-commit.old", gitRepo)
if(preCommitHook.exists()){
    old.withWriter { w -> lines.each { w.writeLine it } }
}

preCommitHook << ". src/main/scripts/precommit.sh\n"

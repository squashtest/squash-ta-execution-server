/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.custom.izpack.components.helpers;

import java.io.IOException;
import java.net.MalformedURLException;

import org.slf4j.Logger;

import com.izforge.izpack.panels.userinput.processorclient.ProcessingClient;

public class URLValidatorMalFormedURL extends URLValidator {
	// using LoggingHelper to get a logger with a configuration that is usefull in izpack.
    private static final Logger LOGGER = LoggingHelper.INSTANCE.getIzpackAdaptedLogger(URLValidatorMalFormedURL.class);
	
	@Override
	protected boolean validateResponseCode(int code) {
		return true;
	}

	@Override
	public boolean validate(ProcessingClient client) {
		boolean reachable = true;
		try {
			reachable=performCall(client);
		} catch(MalformedURLException e) {
			LOGGER.info("MalformedURLException found:", e);
			reachable=false;
		} catch (IOException e) {
			LOGGER.debug("IOException accepted here because another class will detect and report it", e);
		} 
		return reachable;
	}
}

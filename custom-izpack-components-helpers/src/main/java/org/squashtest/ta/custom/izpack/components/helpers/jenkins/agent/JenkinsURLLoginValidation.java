/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.custom.izpack.components.helpers.jenkins.agent;

import com.izforge.izpack.api.data.InstallData;
import org.squashtest.ta.custom.izpack.components.helpers.URLLoginValidation;

/**
 *
 * @author qtran
 */
public class JenkinsURLLoginValidation extends URLLoginValidation{

    public JenkinsURLLoginValidation(InstallData installData) {
        super(installData);
    }

    @Override
    protected String getURL() {
        return getUserInput("jenkins.url");
    }

    @Override
    protected String getUsername() {
        return getUserInput("jenkins.username");
    }

    @Override
    protected String getPassword() {
        return getUserInput("jenkins.password");
    }

    private String getUserInput(String input) {
        return installData.getVariable(input);
    }
}

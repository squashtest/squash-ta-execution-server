/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.custom.izpack.components.helpers;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import com.izforge.izpack.panels.userinput.processorclient.ProcessingClient;
import com.izforge.izpack.panels.userinput.validator.Validator;
import org.slf4j.Logger;

public abstract class URLValidator implements Validator {
    
    // using LoggingHelper to get a logger with a configuration that is usefull in izpack.
    private static final Logger LOGGER = LoggingHelper.INSTANCE.getIzpackAdaptedLogger(URLValidator.class);

    //interface + default connection factory for testability
    static interface ConnectionFactory {
        HttpURLConnection getConnection(URL url) throws IOException;
    }
    
    static ConnectionFactory connectionFactory = new ConnectionFactory() {

        @Override
        public HttpURLConnection getConnection(URL url) throws IOException {
            return (HttpURLConnection) url.openConnection();
        }
    };

    /**
     * Concrete daughter classes must implement validate thus :
     *
     * boolean reachable = true; try { reachable=performCall(client); } catch
     * (... ... here is the real difference : exceptions are LOGGED, and each
     * daughter class decides, for each exception type, if it ignores it, or
     * returns validate=false because they are meant to detect this exception. }
     *
     */
    public abstract boolean validate(ProcessingClient client);

    /**
     * This method performs the actual URL open, gets the HTTP return code and
     * submits it to the {@link #validateResponseCode(int)} method implemented
     * by the concrete daughter class, which implements its specific rule for
     * the HTTP return code and returns true or false accordingly.
     *
     * @param client
     * @return
     * @throws IOException
     */
    protected boolean performCall(ProcessingClient client) throws IOException {
        return validateResponseCode(getResponseCode(client));
    }

    protected int getResponseCode(ProcessingClient client) throws IOException {
        String providedUrl = client.getText();
        LOGGER.debug("Validating provided URL '{}' through {} validator.", providedUrl, getClass());
        URL url = new URL(providedUrl);
        HttpURLConnection connection = connectionFactory.getConnection(url);

        connection.connect();
        int responseCode = connection.getResponseCode();
        LOGGER.info("Http response code returned: '{}'.", responseCode);
        return responseCode;
    }

    /**
     * This method is implemented by daughter classes to decide which HTTP code
     * is valid or invalid, depending on the rule it is responsible for.
     *
     * @param code the HTTP return code to validate.
     * @return
     */
    protected abstract boolean validateResponseCode(int code);

    //this methode is for JUnit tests on MalFormedURLExceptions
    public boolean malFormedExceptionJUnitTest(ProcessingClient client) {
        boolean reachable = true;
        try {
            getResponseCode(client);

        } catch (MalformedURLException e) {
            LOGGER.debug("MalformedURLException for JUnit test!", e);
            reachable = false;
        } catch (IOException e) {
            LOGGER.debug("IOException accepted here!", e);
        }
        return reachable;
    }

    //this methode is for JUnit tests on IOExceptions
    public boolean iOExceptionJUnitTest(ProcessingClient client) {
        boolean reachable = true;
        try {
            getResponseCode(client);

        } catch (MalformedURLException e) {
            LOGGER.debug("MalformedURLException acccepted here!", e);
        } catch (IOException e) {
            LOGGER.debug("IOException for JUnit test!", e);
            reachable = false;
        }
        return reachable;
    }
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.custom.izpack.components.helpers;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;

import com.izforge.izpack.panels.process.AbstractUIProcessHandler;

public abstract class UnixScriptRightChanger {
	// using LoggingHelper to get a logger with a configuration that is usefull in izpack.
    private static final Logger LOGGER=LoggingHelper.INSTANCE.getIzpackAdaptedLogger(UnixScriptRightChanger.class);
    
    /**
     * introducing one more level of indirection to make the {@link UnixScriptRightChanger} class testable.
     */
    static interface ProcessManager{
        ProcessFactory buildProcessFactory(String... command);
        void endOfCurrentProcess(int code);
    }
    
    private ProcessManager factoryFactory;
    
        static class ProcessFactory {
		private ProcessBuilder builder;

		public ProcessFactory(String... command) {
			builder = new ProcessBuilder(command);
		}

		public List<String> command() {
			return builder.command();
		}

		public void directory(File dir) {
			builder.directory(dir);
		}

		public File directory() {
			return builder.directory();
		}

		public Process start() throws IOException {
			return builder.start();
		}
	}

        /**
         * This constructor exists to make this testable.
         * @param factory a mock processFactoryFactory.
         */
        UnixScriptRightChanger(ProcessManager factory) {
            this.factoryFactory=factory;
        }
        
        /**
         * Call this from normal code.
         */
        public UnixScriptRightChanger(){
            this.factoryFactory=new ProcessManager() {
                @Override
                public ProcessFactory buildProcessFactory(String... command) {
                    return new ProcessFactory(command);
                }

                @Override
                public void endOfCurrentProcess(int code) {
                    System.exit(code);
                }
            };
        }
        
	public abstract void run(AbstractUIProcessHandler handler, String[] args) throws IOException;
	
	protected void callMarkScripts(int j, String[] args, String stringCode) throws IOException {
		for (int i=j; i<args.length; ++i) {
			File scriptPath = new File(args[i]);
			markScriptsAsExecutableFiles(scriptPath, stringCode);
		}
	}
	
	protected void noInputArgWarning(String[] args, int i) {
		if (args.length <= i) {
			LOGGER.warn("Usage: UnixScriptRightChanger (<scriptdir> | <scriptfile>)*\n"
					+ "scriptdir: a directory where all files are scripts. Only the first level is treated: "
					+ "if needed, add relevant subdirectories to argument list.");
			factoryFactory.endOfCurrentProcess(1);
		}
	}
	
	protected void markScriptsAsExecutableFiles(File scriptPath, String stringCode) throws IOException {
		ProcessFactory builder = factoryFactory.buildProcessFactory("chmod", "-R", stringCode);
		
		if (!scriptPath.isDirectory()) {
			scriptPath = scriptPath.getParentFile();
		}
		
		List<File> asList = Arrays.asList(scriptPath.listFiles());
		for (File script : asList) {
			List<String> commandLine = builder.command();
			String path;
			if (script.isAbsolute()) {
				path = script.getPath();
			} else {
				path = script.getAbsolutePath();
			}
			LOGGER.info("Listing file '{}' for execution right.", path);
			commandLine.add(path);
		}
		
		builder.directory(scriptPath);
		StringBuilder commandLog = new StringBuilder(
				"Launching execution rights commandline:\n");
		commandLog.append(builder.directory()).append("# ");
		for (String arg : builder.command()) {
			commandLog.append(arg).append(" ");
		}
		LOGGER.info(commandLog.toString().trim());	
		
		Process chmod = builder.start();
		int code;
		try {
			code = chmod.waitFor();
		} catch (InterruptedException ie) {
			LOGGER.error("chmod was interrupted!", ie);
			code = chmod.exitValue();
		}
		if (code != 0) {
			LOGGER.error("chmod failed with '{}' ", code);
		}
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.squashtest.ta.custom.izpack.components.helpers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class helps adapt the use of loggers to izpack5 quirks
 * in izpack installer customization code.
 * When in izpack, forget about 
 * 
 * @author edegenetais
 */
public class LoggingHelper {
    public static final LoggingHelper INSTANCE=new LoggingHelper();
    //we need to add this prefix to logger names in order to have loggers that log, because of how izpack manages logs...
    public static final String IZPACK_LOGGER_PREFIX = "com.izforge.izpack.panels.userinput.validator.";
    
    static{
        LoggerFactory.getLogger(LoggingHelper.class).
                info("When using logs from this class outside izpack, remember that to have logs in izpack we add the prefix '{}' to all logger names.",
                        IZPACK_LOGGER_PREFIX);
    }
    
    /**
     * This method returns a logger that will be usefull when used in an izpack installer.
     * 
     * @param classe the class you want to log for.
     * 
     * @return 
     */
    public Logger getIzpackAdaptedLogger(Class<?> classe){
        return LoggerFactory.getLogger(IZPACK_LOGGER_PREFIX+classe.getName());
    }
}

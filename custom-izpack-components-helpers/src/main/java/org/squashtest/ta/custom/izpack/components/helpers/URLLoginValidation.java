/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.custom.izpack.components.helpers;

import com.izforge.izpack.api.handler.Prompt;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.xml.bind.DatatypeConverter;

import org.slf4j.Logger;

import com.izforge.izpack.api.data.InstallData;
import com.izforge.izpack.panels.userinput.action.ButtonAction;
import com.izforge.izpack.util.Console;

public abstract class URLLoginValidation extends ButtonAction {
    // using LoggingHelper to get a logger with a configuration that is usefull in izpack.
    private static final Logger LOGGER = LoggingHelper.INSTANCE.getIzpackAdaptedLogger(URLLoginValidation.class);

    private static final String ERROR = "error";

    //interface + default connection factory for testability
    static interface ConnectionFactory {

        HttpURLConnection getConnection(URL url) throws IOException;
    }
    static ConnectionFactory connectionFactory = new ConnectionFactory() {
        @Override
        public HttpURLConnection getConnection(URL url) throws IOException {
            return (HttpURLConnection) url.openConnection();
        }
    };

    public URLLoginValidation(InstallData installData) {
        super(installData);
    }

    @Override
    public boolean execute() {
        boolean reachable = false;
        String urlString = getURL();
        String username = getUsername();
        String pwd = getPassword();
        try {
            URL url = new URL(urlString);
            String user = username;
            String pass = pwd;
            String authStr = user + ":" + pass;
            String encoding = DatatypeConverter.printBase64Binary(authStr.getBytes("utf-8"));
            HttpURLConnection connection = connectionFactory.getConnection(url);
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setRequestProperty("Authorization", "Basic " + encoding);
            connection.setAllowUserInteraction(true);

            connection.connect();
            int responseCode = connection.getResponseCode();
            if ((responseCode >= 200 && responseCode < 300) || (responseCode == 403)) {
                reachable = true;
            }
        } catch (IOException e) {
            LOGGER.info("IOException found:", e);
        }
        return reachable;
    }

    @Override
    public boolean execute(Console console) {
        if (!execute()) {
            console.println(messages.get(ERROR));
            return false;
        }
        return true;
    }

    @Override
    public boolean execute(Prompt prompt) {
        if (!execute()) {
            prompt.warn(messages.get(ERROR));
            return false;
        }
        return true;
    }

    protected abstract String getURL();

    protected abstract String getUsername();

    protected abstract String getPassword();
}

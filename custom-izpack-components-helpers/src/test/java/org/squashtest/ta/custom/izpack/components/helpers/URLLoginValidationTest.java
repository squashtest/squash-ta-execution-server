/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.custom.izpack.components.helpers;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.izforge.izpack.api.data.InstallData;
import com.izforge.izpack.api.handler.Prompt;
import com.izforge.izpack.util.Console;
import org.squashtest.ta.custom.izpack.components.helpers.URLLoginValidation.ConnectionFactory;
import org.squashtest.ta.custom.izpack.components.helpers.jenkins.agent.JenkinsURLLoginValidation;

public class URLLoginValidationTest {
	
	private HttpURLConnection connection;
	private InstallData installData;
	private JenkinsURLLoginValidation testee;
	private Console console;
	private Prompt prompt;
	
	@Before
	public void setConnectionFactory() throws IOException {
		
		ConnectionFactory connectionFactory=Mockito.mock(ConnectionFactory.class);
		URLLoginValidation.connectionFactory=connectionFactory;
		
		connection=Mockito.mock(HttpURLConnection.class);
		Mockito.when(connectionFactory.getConnection(Mockito.any(URL.class))).thenReturn(connection);
	}
	
	@Before
	public void setTesteeObjectUp() {
		installData=Mockito.mock(InstallData.class);
		Mockito.when(installData.getVariable("jenkins.url")).thenReturn("http://jenkins.squashtest.org/jenkins");
		console = Mockito.mock(Console.class);
		prompt = Mockito.mock(Prompt.class);
		testee=new JenkinsURLLoginValidation(installData);
	}
	
	@Test
	public void noProblemIfURLIsOk() throws IOException {
		Mockito.when(connection.getResponseCode()).thenReturn(200);
		assertTrue("Authentication passed.", testee.execute());
		assertTrue("Login confirmation in console passed with Http Code response 200.", testee.execute(console));
		assertTrue("Login confirmation in UI passed with Http Code response 200.", testee.execute(prompt));
	}
	
	@Test
	public void noProblemIfGet403Error() throws IOException {
		Mockito.when(connection.getResponseCode()).thenReturn(403);
		assertTrue("Login failed but URl, Username and Password were all right inserted.", testee.execute());
		assertTrue("Login confirmation in console passed with Http Code response 403.", testee.execute(console));
		assertTrue("Login confirmation in UI passed with Http Code response 403.", testee.execute(prompt));
	}
	
	@Test
	public void problemIfGet400Error() throws IOException {
		Mockito.when(connection.getResponseCode()).thenReturn(400);
		assertFalse("URL bad request!", testee.execute());
		assertFalse("Login confirmation in console failed with 400 Error.", testee.execute(console));
		assertFalse("Login confirmation in UI failed with 400 Error.", testee.execute(prompt));
	}
	
	@Test
	public void problemIfGet404Error() throws IOException {
		Mockito.when(connection.getResponseCode()).thenReturn(404);
		assertFalse("URL request not found!", testee.execute());
		assertFalse("Login confirmation in console failed with 404 Error.", testee.execute(console));
		assertFalse("Login confirmation in UI failed with 404 Error.", testee.execute(prompt));		
	}
}

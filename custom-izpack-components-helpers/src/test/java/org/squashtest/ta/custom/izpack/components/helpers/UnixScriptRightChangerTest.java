/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.custom.izpack.components.helpers;

import com.izforge.izpack.panels.process.AbstractUIProcessHandler;
import java.io.File;
import java.io.IOException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.squashtest.ta.api.test.toolkit.ResourceExtractorTestBase;

/**
 *
 * @author edegenetais
 */
public class UnixScriptRightChangerTest extends ResourceExtractorTestBase{
    UnixScriptRightChanger testee;
    UnixScriptRightChanger.ProcessManager mockProcessManager;
    
    private class EndOfTestException extends RuntimeException{};
    
    @Before
    public void setTesteeUp(){
        mockProcessManager=Mockito.mock(UnixScriptRightChanger.ProcessManager.class);
        testee=new UnixScriptRightChanger(mockProcessManager){
            @Override
            public void run(AbstractUIProcessHandler handler, String[] args) throws IOException {
                noInputArgWarning(args, 0);
                
                callMarkScripts(0, args, "777");
            }
        };
    }
    
    @Test(expected=EndOfTestException.class)
    public void testNoargExit() throws IOException{
        Mockito.doThrow(new EndOfTestException()).when(mockProcessManager).endOfCurrentProcess(1);
        testee.run(null, new String[]{});
    }
    
    @Test
    public void testMarkOneScriptDir() throws IOException{
        File dummyScript=createNtrackFile();
        final UnixScriptRightChanger.ProcessFactory mockProcessFactory = Mockito.mock(UnixScriptRightChanger.ProcessFactory.class);
        Mockito.when(mockProcessFactory.start()).thenReturn(Mockito.mock(Process.class));
        Mockito.when(mockProcessManager.buildProcessFactory((String[])Mockito.any())).thenReturn(mockProcessFactory);
        testee.run(null, new String[]{dummyScript.getCanonicalPath()});
        Mockito.verify(mockProcessManager).buildProcessFactory("chmod","-R","777");
        Mockito.verify(mockProcessFactory).directory(dummyScript.getCanonicalFile().getParentFile());
    }
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.custom.izpack.components.helpers;

import static org.junit.Assert.assertFalse;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.izforge.izpack.panels.userinput.processorclient.ProcessingClient;
import org.squashtest.ta.custom.izpack.components.helpers.URLValidator.ConnectionFactory;

public class URLValidatorTest {
	private HttpURLConnection connection;
	private ProcessingClient client;
	private URLValidatorMalFormedURL testee3;
	private URLValidatorOtherIOError testee4;
	
	@Before
	public void setConnectionFactory( ) throws IOException {
		ConnectionFactory connectionFactory = Mockito.mock(ConnectionFactory.class);
                URLValidator.connectionFactory = connectionFactory;
		
		connection = Mockito.mock(HttpURLConnection.class);
		Mockito.when(connectionFactory.getConnection(Mockito.any(URL.class))).thenReturn(connection);
		
	}
	
	@Before
	public void setTestees() {
		client = Mockito.mock(ProcessingClient.class);
		Mockito.when(client.getText()).thenReturn("http://jenkins.squashtest.org/jenkins");

		testee3 = new URLValidatorMalFormedURL();
		testee4 = new URLValidatorOtherIOError();
	}
	
	@Test
	public void problemIfGetMalFormedURLException() throws IOException {
		Mockito.when(connection.getResponseCode()).thenThrow(new MalformedURLException());
		assertFalse("OK", testee3.malFormedExceptionJUnitTest(client));
	}
	
	@Test
	public void problemIfGetIOException() throws IOException {
		Mockito.when(connection.getResponseCode()).thenThrow(new IOException());
		assertFalse("OK", testee4.iOExceptionJUnitTest(client));
	}
}

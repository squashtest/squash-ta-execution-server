/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.custom.izpack.components.helpers;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.izforge.izpack.panels.userinput.processorclient.ProcessingClient;

public class URLValidator4xxErrorTest {
	private ProcessingClient client;
	private URLValidator4xxError testee;
	
	@Before
	public void setTestees() {
		client = Mockito.mock(ProcessingClient.class);
		Mockito.when(client.getText()).thenReturn("http://jenkins.squashtest.org/jenkins");

		testee = new URLValidator4xxError();
	}
	
        @Test
        public void okWith300Code(){
		assertTrue("Test passed with Http reponse code type of 300", testee.validateResponseCode(300));
	}
        @Test
        public void okWith500Code(){
		assertTrue("Test passed with Http reponse code type of 500", testee.validateResponseCode(500));
	}
        
        @Test
        public void failWith400Code(){
		assertFalse("Test failed with Http reponse code type of 400", testee.validateResponseCode(400));
	}
}

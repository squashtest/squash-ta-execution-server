#!/bin/sh
#
#     This file is part of the Squashtest platform.
#     Copyright (C) 2011 - 2020 Henix
#
#     See the NOTICE file distributed with this work for additional
#     information regarding copyright ownership.
#
#     This is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Lesser General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     this software is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Lesser General Public License for more details.
#
#     You should have received a copy of the GNU Lesser General Public License
#     along with this software.  If not, see <http://www.gnu.org/licenses />.
#

. "`dirname "$0"`/env.sh"
echo Squash TF execution server starting on $HOSTNAME
OLDCD=`pwd`
mkdir -p "$TOMCAT_HOME/logs"
cd "$TOMCAT_HOME/bin"
./startup.sh

if [ "$START_SAHI_PROXY" = "YES" ];	then
	if [ ! -d "$SAHI_TA_HOME/logs" ]; then
		mkdir "$SAHI_TA_HOME/logs"
	fi
	cd "$SAHI_TA_HOME/bin"
	./sahi.sh 2>&1 > "$SAHI_TA_HOME/logs/console" &
	echo $! > "$SQUASH_TA_HOME/scripts/sahi.pid"
fi

cd "$OLDCD"

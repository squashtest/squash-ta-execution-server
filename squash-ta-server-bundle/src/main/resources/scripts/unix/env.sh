#!/bin/sh
#
#     This file is part of the Squashtest platform.
#     Copyright (C) 2011 - 2020 Henix
#
#     See the NOTICE file distributed with this work for additional
#     information regarding copyright ownership.
#
#     This is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Lesser General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     this software is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Lesser General Public License for more details.
#
#     You should have received a copy of the GNU Lesser General Public License
#     along with this software.  If not, see <http://www.gnu.org/licenses />.
#

OLDCD=`pwd`
cd "`dirname "$0"`/.."
export SQUASH_TA_HOME="`pwd`"
cd "$OLDCD"

export JAVA_HOME=@jdkPath@

# FIX: Handles jdk packaging changes starting with jdk 9.0 
if [ -d @jdkPath@/jre ]; then
  export JRE_HOME=@jdkPath@/jre
else 
  export JRE_HOME=@jdkPath@
fi

echo Running Squash TF execution server from $SQUASH_TA_HOME, using JDK $JAVA_HOME and JRE $JRE_HOME
export MAVEN_HOME="$SQUASH_TA_HOME/@subdir.maven@"
export MVN_HOME="$MAVEN_HOME"
export MAVEN_OPTS="-Xms512m -Xmx512m -XX:PermSize=256m -XX:MaxPermSize=512m"
export SAHI_TA_HOME="$SQUASH_TA_HOME/@subdir.sahi@"
export PATH="$JAVA_HOME/bin":"$MVN_HOME/bin":$PATH
export START_SAHI_PROXY="$SAHI_SELECTED"
export TOMCAT_HOME="$SQUASH_TA_HOME/@subdir.tomcat@"

# protect the execution server tomcat environment against system-wide variable settings
export CATALINA_HOME=
# additionnally, unlock self-published content to make Squash TA reports work, 
# pending adjustments in their code to make them work with default Content Secutiry Policy
export CATALINA_OPTS="-Dhudson.model.DirectoryBrowserSupport.CSP=\"default-src 'none'; img-src 'self'; style-src 'self'; child-src 'self'; frame-src 'self'; script-src 'self' 'unsafe-inline';\""

export JENKINS_HOME="$SQUASH_TA_HOME/execution_home"
export CONF_HOME="$SQUASH_TA_HOME/execution_home"
export HOSTNAME=`hostname`

echo Squash TF Server environment $SQUASH_TA_HOME@$HOSTNAME

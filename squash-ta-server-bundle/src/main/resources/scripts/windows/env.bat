@REM
@REM     This file is part of the Squashtest platform.
@REM     Copyright (C) 2011 - 2020 Henix
@REM
@REM     See the NOTICE file distributed with this work for additional
@REM     information regarding copyright ownership.
@REM
@REM     This is free software: you can redistribute it and/or modify
@REM     it under the terms of the GNU Lesser General Public License as published by
@REM     the Free Software Foundation, either version 3 of the License, or
@REM     (at your option) any later version.
@REM
@REM     this software is distributed in the hope that it will be useful,
@REM     but WITHOUT ANY WARRANTY; without even the implied warranty of
@REM     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
@REM     GNU Lesser General Public License for more details.
@REM
@REM     You should have received a copy of the GNU Lesser General Public License
@REM     along with this software.  If not, see <http://www.gnu.org/licenses />.
@REM

@echo off
hostname > .\.hostname.tmp.txt
set /p HOSTNAME=<.\.hostname.tmp.txt
del .\.hostname.tmp.txt

set OLDCD=%CD%
cd ..
set SQUASH_TA_HOME=%CD%
cd "%OLDCD%"

set JAVA_HOME=$jdkPath

@REM FIX: Handles jdk packaging changes starting with jdk 9.0
if exist $jdkPath\jre (
  set JRE_HOME=$jdkPath\jre
) else (
  set JRE_HOME=$jdkPath
)

echo "Running Squash TF execution server from %SQUASH_TA_HOME%, using JDK %JAVA_HOME% and JRE %JRE_HOME%"
set MAVEN_HOME=%SQUASH_TA_HOME%\${subdir.maven}
set MVN_HOME=%MAVEN_HOME%
set MAVEN_OPTS=-Xms512m -Xmx512m -XX:PermSize=256m -XX:MaxPermSize=512m
set SAHI_TA_HOME=%SQUASH_TA_HOME%\${subdir.sahi}
set PATH="%JAVA_HOME%\bin";"%MVN_HOME%\bin";%PATH%
set START_SAHI_PROXY=$SAHI_SELECTED
set TOMCAT_HOME=%SQUASH_TA_HOME%\${subdir.tomcat}

@REM protect the execution server tomcat environment against system-wide variable settings
set CATALINA_HOME=
@REM additionnally, unlock self-published content to make Squash TA reports work, 
@REM pending adjustments in their code to make them work with default Content Secutiry Policy
set CATALINA_OPTS=-Dhudson.model.DirectoryBrowserSupport.CSP="default-src 'none'; img-src 'self'; style-src 'self'; child-src 'self'; frame-src 'self'; script-src 'self' 'unsafe-inline';"

set JENKINS_HOME=%SQUASH_TA_HOME%\execution_home
set CONF_HOME=%SQUASH_TA_HOME%\execution_home

@REM default name if installing as a Windows Service
set SERVICE_NAME=SquashTA_Server

echo "Squash TF Server environment %SQUASH_TA_HOME%@%HOSTNAME%"
@REM
@REM     This file is part of the Squashtest platform.
@REM     Copyright (C) 2011 - 2020 Henix
@REM
@REM     See the NOTICE file distributed with this work for additional
@REM     information regarding copyright ownership.
@REM
@REM     This is free software: you can redistribute it and/or modify
@REM     it under the terms of the GNU Lesser General Public License as published by
@REM     the Free Software Foundation, either version 3 of the License, or
@REM     (at your option) any later version.
@REM
@REM     this software is distributed in the hope that it will be useful,
@REM     but WITHOUT ANY WARRANTY; without even the implied warranty of
@REM     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
@REM     GNU Lesser General Public License for more details.
@REM
@REM     You should have received a copy of the GNU Lesser General Public License
@REM     along with this software.  If not, see <http://www.gnu.org/licenses />.
@REM

@echo off

echo "Retrieving Sahi Proxy PID of Squash TF Server in: %SAHI_TA_HOME%\bin"

"%JAVA_HOME%\bin\java" -cp "%SQUASH_TA_HOME%\scripts\lib\squash-ta-server-helper-windows.jar" org.squashtest.ta.server.helper.windows.PIDFinder "SahiProxy_%SQUASH_TA_HOME:~0,50%" > .\.sahi.pid 2>nul
set /p SAHI_PID=<.\.sahi.pid
del .\.sahi.pid

if "%SAHI_PID%"=="null" (
	echo No Sahi process to kill
) else (
	echo Killing sahi process #%SAHI_PID%
	taskkill /PID %SAHI_PID%
)

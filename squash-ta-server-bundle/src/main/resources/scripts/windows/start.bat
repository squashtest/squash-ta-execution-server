@REM
@REM     This file is part of the Squashtest platform.
@REM     Copyright (C) 2011 - 2020 Henix
@REM
@REM     See the NOTICE file distributed with this work for additional
@REM     information regarding copyright ownership.
@REM
@REM     This is free software: you can redistribute it and/or modify
@REM     it under the terms of the GNU Lesser General Public License as published by
@REM     the Free Software Foundation, either version 3 of the License, or
@REM     (at your option) any later version.
@REM
@REM     this software is distributed in the hope that it will be useful,
@REM     but WITHOUT ANY WARRANTY; without even the implied warranty of
@REM     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
@REM     GNU Lesser General Public License for more details.
@REM
@REM     You should have received a copy of the GNU Lesser General Public License
@REM     along with this software.  If not, see <http://www.gnu.org/licenses />.
@REM

@echo off
call env.bat
echo "Squash TF execution server starting on %HOSTNAME%"

set OLDCD=%CD%

IF NOT EXIST "%TOMCAT_HOME%\logs" (
    mkdir "%TOMCAT_HOME%\logs"
)

cd "%TOMCAT_HOME%\bin"

call startup.bat

cd "%SAHI_TA_HOME%\bin"

if "%START_SAHI_PROXY%"=="YES" start "SahiProxy_%SQUASH_TA_HOME:~0,50%" sahi.bat

cd "%OLDCD%"

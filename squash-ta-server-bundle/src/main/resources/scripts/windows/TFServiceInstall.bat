@REM
@REM     This file is part of the Squashtest platform.
@REM     Copyright (C) 2011 - 2020 Henix
@REM
@REM     See the NOTICE file distributed with this work for additional
@REM     information regarding copyright ownership.
@REM
@REM     This is free software: you can redistribute it and/or modify
@REM     it under the terms of the GNU Lesser General Public License as published by
@REM     the Free Software Foundation, either version 3 of the License, or
@REM     (at your option) any later version.
@REM
@REM     this software is distributed in the hope that it will be useful,
@REM     but WITHOUT ANY WARRANTY; without even the implied warranty of
@REM     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
@REM     GNU Lesser General Public License for more details.
@REM
@REM     You should have received a copy of the GNU Lesser General Public License
@REM     along with this software.  If not, see <http://www.gnu.org/licenses />.
@REM

@echo off
call env.bat

if "%~1" == "" GOTO suite
SET "SERVICE_NAME=%~1"
:suite
if "%~2" == "" GOTO done
SET "SERVICE_DESC=%~2"
:done

cd %TOMCAT_HOME%\bin
echo Installing Squash TF execution server as Service named "%SERVICE_NAME%"

start tomcat8.exe //IS//"%SERVICE_NAME%"^
    --Description "%SERVICE_DESC%"  ^
    --DisplayName "%SERVICE_NAME%" ^
    --Install "%TOMCAT_HOME%\bin\tomcat8.exe"  ^
    --LogPath "%TOMCAT_HOME%\logs" ^
	--StdOutput auto ^
    --StdError auto  ^
    --Classpath "%TOMCAT_HOME%\bin\bootstrap.jar;%TOMCAT_HOME%\bin\tomcat-juli.jar" ^
    --Jvm "%JAVA_HOME%\jre\bin\server\jvm.dll" ^
	--JvmOptions "-Dcatalina.home=%TOMCAT_HOME%" ^
	++JvmOptions "-Dcatalina.base=%TOMCAT_HOME%" ^
	++JvmOptions "-Djava.io.tmpdir=%TOMCAT_HOME%\temp"  ^
	++JvmOptions "-Djava.util.logging.manager=org.apache.juli.ClassLoaderLogManager"  ^
	++JvmOptions "-Djava.util.logging.config.file=%TOMCAT_HOME%\conf\logging.properties" ^
    --StartMode jvm ^
    --StopMode jvm ^
    --StartPath "%TOMCAT_HOME%" ^
    --StopPath "%TOMCAT_HOME%" ^
    --StartClass org.apache.catalina.startup.Bootstrap ^
    --StopClass org.apache.catalina.startup.Bootstrap ^
    --StartParams start ^
    --StopParams stop  ^
    --Startup auto ^
	--Environment  "SQUASH_TA_HOME=%SQUASH_TA_HOME%" ++Environment "JAVA_HOME=%JAVA_HOME%"^
	  ++Environment "MAVEN_HOME=%MAVEN_HOME%" ++Environment "MVN_HOME=%MVN_HOME%"^
	  ++Environment "MAVEN_OPTS=%MAVEN_OPTS%" ++Environment "TOMCAT_HOME=%TOMCAT_HOME%"^
	  ++Environment "CATALINA_HOME=" ++Environment "CATALINA_OPTS="^
	  ++Environment "JENKINS_HOME=%JENKINS_HOME%" ++Environment "CONF_HOME=%CONF_HOME%"	^
	  ++Environment 'PATH=%PATH%' 

ping -n 3 localhost > nul 

echo Starting Squash TF Server:"%SERVICE_NAME%"
  net start "%SERVICE_NAME%"
  
 @REM  waiting before start service
 ping -n 3 localhost > nul 
 
 cd ..
 cd ..
 cd scripts\windows
 
pause
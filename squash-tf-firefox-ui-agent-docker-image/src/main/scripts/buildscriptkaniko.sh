#!/bin/sh
#
#     This file is part of the Squashtest platform.
#     Copyright (C) 2011 - 2020 Henix
#
#     See the NOTICE file distributed with this work for additional
#     information regarding copyright ownership.
#
#     This is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Lesser General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     this software is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Lesser General Public License for more details.
#
#     You should have received a copy of the GNU Lesser General Public License
#     along with this software.  If not, see <http://www.gnu.org/licenses />.
#

cd $(dirname $0)/../image

if [ $# -lt 2 ] 
then
	version=${project.version}
        snapshot_version=$(echo "${project.version}" | grep -E ".*-SNAPSHOT")
	if [ "$IS_RELEASE" = "true" ]
        then
            tag=latest
        elif [  "$snapshot_version" = "${project.version}" ]
        then
            tag=latest-SNAPSHOT
        else
            tag=lastbuilt
        fi
else
	version=$1
	tag=$2
fi

if [ "$tag" = "" ]
then
	tag=lastbuilt
fi

/kaniko/executor --destination docker.squashtest.org/squash-tf-1/squash-tf-firefox-ui-execution-agent:$version --destination docker.squashtest.org/squash-tf-1/squash-tf-firefox-ui-execution-agent:$tag -f firefox-ui-squash-tf-agent-kaniko.dockerfile --context $(pwd) --build-arg TF_AGENT_VERSION=$version
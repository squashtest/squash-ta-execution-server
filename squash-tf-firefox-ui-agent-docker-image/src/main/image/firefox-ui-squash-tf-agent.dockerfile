#
#     This file is part of the Squashtest platform.
#     Copyright (C) 2011 - 2020 Henix
#
#     See the NOTICE file distributed with this work for additional
#     information regarding copyright ownership.
#
#     This is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Lesser General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     this software is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Lesser General Public License for more details.
#
#     You should have received a copy of the GNU Lesser General Public License
#     along with this software.  If not, see <http://www.gnu.org/licenses />.
#

ARG TF_AGENT_VERSION
FROM squash/squash-tf-x11-execution-agent:${TF_AGENT_VERSION}

ARG FIREFOX_VERSION=60.6.1esr-1~deb9u1


USER root

# Install Firefox-esr - Fixed version for selenium driver compatibility purpose
# TODO : replace this by a nexus-based proxy
COPY local-deb-repo /var/local-deb-repo
RUN apt-get update && apt-get -y install gdebi; echo '>>Installing firefox through gdebi<<';gdebi -n /var/local-deb-repo/firefox-esr_${FIREFOX_VERSION}_amd64.deb;echo '>>Moping up all those things we installed just to get gdebi going !<<';apt-get -y purge bsdmainutils dbus file gdebi gnome-icon-theme libcap2-bin patch psmisc systemd;SUDO_FORCE_REMOVE=yes apt-get autoremove -y --purge
# This should remain after nexus is in use
RUN apt-get update && apt-get -y install firefox-esr=${FIREFOX_VERSION}

#Install geckodriver - Fixed version (the one of the third parties lib) for Firefox binary compatibility
COPY geckodriver.tar.gz /tmp/geckodriver.tar.gz

RUN tar -xvzf /tmp/geckodriver.tar.gz \
  && mv geckodriver /usr/local/bin/ \
  && rm /tmp/geckodriver.tar.gz

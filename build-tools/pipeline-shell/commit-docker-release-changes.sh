#!/bin/sh -xe
#
#     This file is part of the Squashtest platform.
#     Copyright (C) 2011 - 2020 Henix
#
#     See the NOTICE file distributed with this work for additional
#     information regarding copyright ownership.
#
#     This is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Lesser General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     this software is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Lesser General Public License for more details.
#
#     You should have received a copy of the GNU Lesser General Public License
#     along with this software.  If not, see <http://www.gnu.org/licenses />.
#

create_release_image() {
    DOCKER_IMG_PROJECT=$1
    sed -i -E "s/<version>${project_version}<\\/version>/<version>${RELEASE_VERSION}<\\/version>/g" $DOCKER_IMG_PROJECT/pom.xml
    mvn -f $DOCKER_IMG_PROJECT/pom.xml -Dtools.sourceDirectory=${THIRD_PARTY_BINARY_REPOSITORY} org.apache.maven.plugins:maven-assembly-plugin:2.3:single@get-installer
    echo 'Release docker image of project $DOCKER_IMG_PROJECT for version ${RELEASE_VERSION} created'
}

prepare_next_development_version(){
    DOCKER_IMG_PROJECT=$1
    sed -i -E "s/<version>${RELEASE_VERSION}<\\/version>/<version>${NEXT_DEVELOPMENT_VERSION}<\\/version>/g" $DOCKER_IMG_PROJECT/pom.xml
}

git checkout squash-ta-server-umbrella-${RELEASE_VERSION}

#Create temporary branch
git branch temp-release-process
git checkout temp-release-process

create_release_image squash-ta-execution-server-dockerfile
create_release_image squash-tf-agent-docker-image
create_release_image squash-tf-x11-agent-docker-image
create_release_image squash-tf-firefox-ui-agent-docker-image
create_release_image squash-tf-chrome-ui-agent-docker-image

git add .
git commit -m "[pipeline-release-process] preparing docker images release for version ${RELEASE_VERSION}"
echo 'Release docker images builders for version ${RELEASE_VERSION} created and committed'

git tag squash-ta-server-docker-${RELEASE_VERSION}

prepare_next_development_version squash-ta-execution-server-dockerfile
prepare_next_development_version squash-tf-agent-docker-image
prepare_next_development_version squash-tf-x11-agent-docker-image
prepare_next_development_version squash-tf-firefox-ui-agent-docker-image
prepare_next_development_version squash-tf-chrome-ui-agent-docker-image

git add .
git commit -m "[pipeline-release-process] preparing next development version ${NEXT_DEVELOPMENT_VERSION}"
echo 'Changes for next development version ${NEXT_DEVELOPMENT_VERSION} committed'

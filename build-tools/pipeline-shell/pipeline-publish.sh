#!/bin/sh -xe
#
#     This file is part of the Squashtest platform.
#     Copyright (C) 2011 - 2020 Henix
#
#     See the NOTICE file distributed with this work for additional
#     information regarding copyright ownership.
#
#     This is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Lesser General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     this software is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Lesser General Public License for more details.
#
#     You should have received a copy of the GNU Lesser General Public License
#     along with this software.  If not, see <http://www.gnu.org/licenses />.
#

publish_image() {
    PROJECT="$1"
    TAR_RADIX="$2"

    mv $WORKSPACE/$PROJECT/target/$TAR_RADIX.*.tar ${PUBLIC_REPOSITORY}/acceptance
    chmod 644 ${PUBLIC_REPOSITORY}/acceptance/$TAR_RADIX.*.tar
    rm ${PUBLIC_REPOSITORY}/acceptance/$TAR_RADIX.${tag}.tar || echo No previous $TAR_RADIX.${tag} tarball
    
    if [ "${IS_RELEASE}" = "true" ];then 
        ln ${PUBLIC_REPOSITORY}/acceptance/$TAR_RADIX.${RELEASE_VERSION}.tar ${PUBLIC_REPOSITORY}/acceptance/$TAR_RADIX.${tag}.tar
        rm ${PUBLIC_REPOSITORY}/acceptance/$TAR_RADIX.${versionned_tag}.tar || echo No previous $TAR_RADIX.${versionned_tag} tarball
        ln ${PUBLIC_REPOSITORY}/acceptance/$TAR_RADIX.${RELEASE_VERSION}.tar ${PUBLIC_REPOSITORY}/acceptance/$TAR_RADIX.${versionned_tag}.tar
    fi
    if [ "${IS_RELEASE}" != "true" ]; then 
        ln ${PUBLIC_REPOSITORY}/acceptance/$TAR_RADIX.${project_version}.tar ${PUBLIC_REPOSITORY}/acceptance/$TAR_RADIX.${tag}.tar
    fi
}

echo 'Publishing installers'
mv $WORKSPACE/squash-ta-server-bundle/target/squash-tf-execution-server-*-installer.jar ${PUBLIC_REPOSITORY}/acceptance
mv $WORKSPACE/squash-ta-jenkins-agent-bundle/target/squash-tf-execution-agent-*-installer.jar ${PUBLIC_REPOSITORY}/acceptance
echo 'Publishing docker images'

echo '   Server:'
publish_image "squash-ta-execution-server-dockerfile" "squash-tf-execution-server.docker"

echo '   Agent:'
publish_image "squash-tf-agent-docker-image" "squash-tf-execution-agent.docker"

echo '   X11-Agent:'
publish_image "squash-tf-x11-agent-docker-image" "squash-tf-x11-execution-agent.docker"

echo '   Chrome-UI-Agent:'
publish_image "squash-tf-chrome-ui-agent-docker-image" "squash-tf-chrome-ui-execution-agent.docker"

echo '   Firefox-UI-Agent:'
publish_image "squash-tf-firefox-ui-agent-docker-image" "squash-tf-firefox-ui-execution-agent.docker"

if [ "${IS_RELEASE}" = "true" ]; then
    echo 'Pushing Release changes'
    git push --all origin
    git push --tags origin
fi

echo '   ...done'

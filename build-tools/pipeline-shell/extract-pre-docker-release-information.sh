#!/bin/sh -xe
#
#     This file is part of the Squashtest platform.
#     Copyright (C) 2011 - 2020 Henix
#
#     See the NOTICE file distributed with this work for additional
#     information regarding copyright ownership.
#
#     This is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Lesser General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     this software is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Lesser General Public License for more details.
#
#     You should have received a copy of the GNU Lesser General Public License
#     along with this software.  If not, see <http://www.gnu.org/licenses />.
#

mkdir -p target
git rev-parse --short HEAD>target/mvn-release.rev
echo 'MAVEN_RELEASE_TIP revision hash written in target/mvn-release.rev'
mvn -f squash-ta-execution-server-dockerfile/pom.xml help:evaluate -Dexpression=project.version -Doutput=target/version.txt
mkdir -p target
mv squash-ta-execution-server-dockerfile/target/version.txt target/version.txt
echo 'Initial version written in target/version.txt'

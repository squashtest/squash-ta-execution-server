#!/bin/bash
#
#     This file is part of the Squashtest platform.
#     Copyright (C) 2011 - 2020 Henix
#
#     See the NOTICE file distributed with this work for additional
#     information regarding copyright ownership.
#
#     This is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Lesser General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     this software is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Lesser General Public License for more details.
#
#     You should have received a copy of the GNU Lesser General Public License
#     along with this software.  If not, see <http://www.gnu.org/licenses />.
#

jenkinsPluginDir=$1/jenkins-plugins

if [[ -e $jenkinsPluginDir && $(ls -1 $jenkinsPluginDir | wc -l) -gt 0 ]]; then

  echo Jenkins plugin directory found in $1: renaming

  for file in $jenkinsPluginDir/*; do
    version=$(unzip -p $file META-INF/MANIFEST.MF | grep Plugin-Version | cut -d":" -f 2|tr -d " "|tr -d '\r')
    nom=$(unzip -p $file META-INF/MANIFEST.MF | grep Extension-Name | cut -d":" -f 2|tr -d " "|tr -d '\r')
    echo Found Jenkins plugin $nom-$version.jpi
    if [[ $(basename $file) != ${nom}-${version}.jpi ]]; then
      mv $file $(dirname $file)/${nom}-${version}.jpi
    else
      echo No need to rename $(basename $file) : name is already versionned.
    fi
  done
else
  echo No jenkins plugin directory found in $1: noop !
fi

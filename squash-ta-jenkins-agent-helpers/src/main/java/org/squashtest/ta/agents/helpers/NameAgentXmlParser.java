/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.agents.helpers;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.Node;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author qtran
 */
public class NameAgentXmlParser {
    //This is for testing
    static interface HttpConnectionFactory{
        HttpURLConnection getConnection(String urlString) throws IOException;
    }
    
    static HttpConnectionFactory connectionFactory=new HttpConnectionFactory() {
        @Override
        public HttpURLConnection getConnection(String urlString) throws IOException {
            URL url=new URL(urlString);
            return (HttpURLConnection)url.openConnection();
        }
    };
    
    public static void main(String[] args) {
 
        // args[0] corresponds to the first parameter given when NameAgentXmlParser.jar is executed in createNode Script.
       if ("getNode".equals(args[0])) {    
           
           getNode();
           
       } else if ("getSecret".equals(args[0])){
            
            /* To retrieve the agent secret key, we need to give "username:password" (args[2]) 
            and the "url endpoint" of the  Jenkins' slave-agent.jnlp file (args[1]) which contains the secret key.*/
                
            getSecret(args[2], args[1]);                                
        } else {
           System.err.println("Unrecognized command "+args[0]);
       }          
    }
    
    //This method allows to retrieve the name of the agent deployed in Jenkins
    private static void getNode() {

        try {
      
            final DocumentBuilderFactory fac = DocumentBuilderFactory.newInstance();
            
            final DocumentBuilder builder = fac.newDocumentBuilder();

            final Document doc = builder.parse(new File("configAgent.xml"));

            treatDocument(doc);
            

            } catch (ParserConfigurationException | SAXException | IOException ex) {
                Logger.getLogger(NameAgentXmlParser.class.getName()).log(Level.SEVERE, null, ex);
            }
        
    }
    
    
    //This method allows to retrieve the secret key of the deployed Jenkins' agent   
    private static void getSecret(String usernameColonPassword , String url) {
        try {

            HttpURLConnection urlConnection = getConnection(usernameColonPassword, url);

            final BufferedInputStream in = new BufferedInputStream(urlConnection.getInputStream());
                        
            final DocumentBuilderFactory fac = DocumentBuilderFactory.newInstance();
                        
            final DocumentBuilder builder = fac.newDocumentBuilder();
                    
            final Document doc = builder.parse(in);

            treatDocument(doc);
            
        }catch(IOException ex){
            Logger.getLogger(NameAgentXmlParser.class.getName()).log(Level.SEVERE, "Failed to parse agent configuration information.", ex);
        }catch (ParserConfigurationException | SAXException ex) {
            Logger.getLogger(NameAgentXmlParser.class.getName()).log(Level.SEVERE, "Failed to parse agent configuration information.", ex);
        }
    }
    
    
    private static HttpURLConnection getConnection(String usernameColonPassword , String url) {
    
      HttpURLConnection urlConnection = null;
      
      try {
 
            String basicAuthPayload = "Basic " + Base64.getEncoder().encodeToString(usernameColonPassword.getBytes());
            
            // Connect to the web server endpoint            
            urlConnection = connectionFactory.getConnection(url);
            
            // Set HTTP method as GET
            urlConnection.setRequestMethod("GET");

            // Include the HTTP Basic Authentication payload
            urlConnection.addRequestProperty("Authorization", basicAuthPayload);
            
            return urlConnection;
            
        } catch (MalformedURLException ex) {
            Logger.getLogger(NameAgentXmlParser.class.getName()).log(Level.SEVERE, "Failed to set the server connection up : wrong URL.", ex);
        } catch (IOException ex) {
            Logger.getLogger(NameAgentXmlParser.class.getName()).log(Level.SEVERE, "Failed on error while trying to set the server connection up.", ex);
        }
      
        return urlConnection;
    }

    private static void treatDocument(Document document) {
        
        final Element racine = document.getDocumentElement();

        final NodeList racineNoeuds = racine.getChildNodes();
        final int nbRacineNoeuds = racineNoeuds.getLength();

        for (int i=0; i<nbRacineNoeuds; ++i){
                
            if(Node.ELEMENT_NODE == racineNoeuds.item(i).getNodeType()){
                treatElementNode(racineNoeuds, i);
            }
        }        
        
    }
    
    private static void treatElementNode(final NodeList racineNoeuds, int i) throws DOMException {
        
        final Element elmt = (Element) racineNoeuds.item(i);
        
        if ("name".equals(elmt.getNodeName())) {
            
            System.out.println(elmt.getTextContent());//NOSONAR : the whole business of this utility is to print the extracted agent name to stdout
        
        } else if ("application-desc".equals(elmt.getNodeName())) {
            
            System.out.println(elmt.getFirstChild().getTextContent());//NOSONAR : the whole business of this utility is to print the extracted secret key of the agent to stdout
        }
    }
 
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.agents.helpers;

import com.sun.org.apache.xerces.internal.util.URI;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.squashtest.ta.api.test.toolkit.ResourceExtractorTestBase;

/**
 *
 * @author edegenetais
 */
public class NameAgentXmlParserTest extends ResourceExtractorTestBase{
    
    private PrintStream stdout;
    private PrintStream fileStdout;
    private HttpURLConnection connectionMock;
    
    @Before
    public void saveOut(){
        stdout=System.out;
    }
    
    @Before
    public void setURLFactoryUp() throws IOException{
        NameAgentXmlParser.connectionFactory=Mockito.mock(NameAgentXmlParser.HttpConnectionFactory.class);
        connectionMock = Mockito.mock(HttpURLConnection.class);
        Mockito.when(NameAgentXmlParser.connectionFactory.getConnection(Mockito.anyString())).thenAnswer(new GetConnectionAnswer());
    }
    
    protected File sendStdoutToFile() throws IOException{
        File stdoutFile=createNtrackFile();
        fileStdout=new PrintStream(stdoutFile);
        System.setOut(fileStdout);
        return stdoutFile;
    }
    
    @Test
    public void testSecretExtractor() throws IOException{
        File tempStorageRoot=createNtrackDir();
        File secretMockPayload=new File(tempStorageRoot,"jnlpMock");
        extractToFile(secretMockPayload, "jnlpMock");
        File actualStdOut=sendStdoutToFile();

        Mockito.when(connectionMock.getInputStream()).thenReturn(new FileInputStream(secretMockPayload));
        
        NameAgentXmlParser.main(new String[]{"getSecret",secretMockPayload.toURI().toURL().toExternalForm(),"anonymous:thoroughlyBogus"});
        flushOut();
        checkActualContentAgainstExpected(actualStdOut, "expectedSecret");
    }
    
    protected void flushOut(){
        System.setOut(stdout);
        fileStdout.flush();
        fileStdout.close();
    }
    
    @After
    public void restoreOut(){
        System.setOut(stdout);
    }

    private class GetConnectionAnswer implements Answer<HttpURLConnection> {
        @Override
        public HttpURLConnection answer(InvocationOnMock iom) throws Throwable {
            System.err.println("URL is : "+iom.getArgument(0));
            return connectionMock;
        }
    }
}

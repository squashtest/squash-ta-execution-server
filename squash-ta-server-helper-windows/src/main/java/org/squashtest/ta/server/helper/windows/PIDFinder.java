/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.server.helper.windows;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This helper class isolates the sahi process in windows in order for it to get killed.
 * @author edegenetais
 *
 */
public class PIDFinder {
	public static void main(String[] args) throws IOException {
		ProcessBuilder pBuilder=new ProcessBuilder("cmd.exe", "/C","tasklist", "/FI", "\"IMAGENAME eq cmd.exe\"", "/V");
		Process listProcess=pBuilder.start();
		System.err.println("process started");
		
		BufferedReader r=new BufferedReader(new InputStreamReader(listProcess.getInputStream()));
		System.err.println("reader gotten");
		
		String pid=null;
		
		String line=r.readLine();
		while(line!=null){
			System.err.println("line="+line);
			Pattern PIDPattern=Pattern.compile("cmd\\.exe[^0-9]*([0-9]*).*"+Pattern.quote(args[0])+".*");
			Matcher matcher = PIDPattern.matcher(line);
			if(matcher.find()){
				pid = matcher.group(1);
				System.err.println("> PID='"+pid+"'");
			}
			line=r.readLine();
		}
		System.out.println(pid);
	}
}

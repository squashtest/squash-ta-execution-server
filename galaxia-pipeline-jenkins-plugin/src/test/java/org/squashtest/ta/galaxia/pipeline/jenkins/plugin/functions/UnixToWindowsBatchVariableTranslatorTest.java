/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.pipeline.jenkins.plugin.functions;

import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author edegenetais
 */
public class UnixToWindowsBatchVariableTranslatorTest {
    @Test
    public void mustSupportLineWithNoVariableBinding(){
        String input="echo 'You must not look for what\\'s not here \\!'";
        String expected=input;//input should be passed through as is
        String actual=new UnixToWindowsBatchVariableTranslator().apply(input);
        Assert.assertEquals("Should not change line with no variable", expected, actual);
    }
    
    @Test
    public void mustReplaceVariableBindingProperly(){
        String input="mvn -f \"galaxia1722378300778352888pom.xml\" -s \"C:\\Users\\Demo_squashTA\\Documents\\tfAgent20181023/squash_ta_config/mavenSettings.xml\" -Dta.success.threshold=FAIL -Dta.test.suite={file:\"C:\\Users\\Demo_squashTA\\AppData\\Local\\Temp\\testsuite5846401772206372315.json\"} -Dstatus.update.events.url=\"http://172.17.0.1:8090/squash\" -Dsquash.ta.external.id=\"ff80818166a11c0b0166a1be42af0003\" -Djobname=\"00Squash-TA_Template-pipeline\" -Dhostname=\"${HOSTNAME}\" -Dsquash.ta.conf.file=\"C:\\Users\\Demo_squashTA\\Documents\\TA_win_agent_working_space\\workspace\\00Squash-TA_Template-pipeline@tmp\\config7892793014172816474tmp\" -Dta.tmcallback.reportbaseurl=\"http://localhost:8080/jenkins//job\" -Dta.tmcallback.jobexecutionid=\"17\" -Dta.tmcallback.reportname=Squash_TA_HTML_Report -Dta.delete.json.file=true -Dta.temp.directory=\"C:\\Users\\Demo_squashTA\\Documents\\TA_win_agent_working_space\\workspace\\00Squash-TA_Template-pipeline/target/1\" org.squashtest.ta::squash-ta-maven-plugin::run";
        String expected="mvn -f \"galaxia1722378300778352888pom.xml\" -s \"C:\\Users\\Demo_squashTA\\Documents\\tfAgent20181023/squash_ta_config/mavenSettings.xml\" -Dta.success.threshold=FAIL -Dta.test.suite={file:\"C:\\Users\\Demo_squashTA\\AppData\\Local\\Temp\\testsuite5846401772206372315.json\"} -Dstatus.update.events.url=\"http://172.17.0.1:8090/squash\" -Dsquash.ta.external.id=\"ff80818166a11c0b0166a1be42af0003\" -Djobname=\"00Squash-TA_Template-pipeline\" -Dhostname=\"%HOSTNAME%\" -Dsquash.ta.conf.file=\"C:\\Users\\Demo_squashTA\\Documents\\TA_win_agent_working_space\\workspace\\00Squash-TA_Template-pipeline@tmp\\config7892793014172816474tmp\" -Dta.tmcallback.reportbaseurl=\"http://localhost:8080/jenkins//job\" -Dta.tmcallback.jobexecutionid=\"17\" -Dta.tmcallback.reportname=Squash_TA_HTML_Report -Dta.delete.json.file=true -Dta.temp.directory=\"C:\\Users\\Demo_squashTA\\Documents\\TA_win_agent_working_space\\workspace\\00Squash-TA_Template-pipeline/target/1\" org.squashtest.ta::squash-ta-maven-plugin::run";//input should be passed through as is
        String actual=new UnixToWindowsBatchVariableTranslator().apply(input);
        Assert.assertEquals("Should redo variable bindings with %%", expected, actual);
    }
    
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.pipeline.jenkins.plugin.steps;

import hudson.EnvVars;
import java.io.IOException;
import org.jenkinsci.plugins.workflow.steps.StepContext;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

/**
 *
 * @author edegenetais
 */
public class EnvVarExtractorTest {

    @Test
    public void returnValueOfFoundVar() throws IOException, InterruptedException {

        final String varToFind = "myVar";

        final EnvVars envVarsMock = Mockito.mock(EnvVars.class);
        final String expectedValue = "hereIsTheValue";

        Mockito.when(envVarsMock.get(varToFind)).thenReturn(expectedValue);

        final StepContext stepContextMock = Mockito.mock(StepContext.class);
        Mockito.when(stepContextMock.get(EnvVars.class)).thenReturn(envVarsMock);

        String actualValue = new EnvVarExtractor("anyStep").extractEnvParm(stepContextMock, "myVar", "no myVar in context (this should not happen)");

        Mockito.verify(stepContextMock, Mockito.never()).onFailure(Mockito.any(Throwable.class));

        Assert.assertEquals(expectedValue, actualValue);
    }

    @Test(expected = IllegalStateException.class)
    public void throwIfNotFound() throws IOException, InterruptedException {
        final StepContext stepContextMock = Mockito.mock(StepContext.class);
        try {
            final String varToFind = "myVar";

            final EnvVars envVarsMock = Mockito.mock(EnvVars.class);

            Mockito.when(envVarsMock.get(varToFind)).thenReturn(null);

            Mockito.when(stepContextMock.get(EnvVars.class)).thenReturn(envVarsMock);

            new EnvVarExtractor("anyStep").extractEnvParm(stepContextMock, "myVar", "no myVar in context (this should not happen)");
        } catch (IllegalStateException e) {
            Mockito.verify(stepContextMock, Mockito.times(1)).onFailure(e);
            throw e;
        }
    }
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.pipeline.jenkins.plugin.functions.condition;

import java.io.File;
import java.io.IOException;
import java.util.logging.LogManager;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.squashtest.ta.api.test.toolkit.ResourceExtractorTestBase;
import org.squashtest.ta.galaxia.metaexecution.conditions.Condition;
import org.squashtest.ta.galaxia.metaexecution.conditions.InvalidConditionSetupException;
import org.squashtest.ta.galaxia.metaexecution.conditions.SQLCondition;

/**
 *
 * @author edegenetais
 */
public class ConditionAdapterTest extends ResourceExtractorTestBase{
    private ConditionAdapter testee;
    
    @BeforeClass
    public static void setLogsUp() throws IOException{
        LogManager.getLogManager().readConfiguration(ConditionAdapterTest.class.getResourceAsStream("/logging.properties"));
    }
    
    @Test
    public void successfullAxel() throws InvalidConditionSetupException, IOException{
        
        File fakeProjectRoot=createNtrackDir();
        testee=new ConditionAdapter(fakeProjectRoot.getAbsolutePath());
        File cpFile=new File(fakeProjectRoot,"target/squashTA.galaxia/.classpath");
        cpFile.getParentFile().mkdirs();
        extractToFile(cpFile, "classpath.txt");
        File targetFile=new File(fakeProjectRoot,"src/squashTA/targets/target.properties");
        targetFile.getParentFile().mkdirs();
        extractToFile(targetFile, "target.properties");
        SQLCondition condition=new SQLCondition(fakeProjectRoot.getAbsolutePath(), "target", "let's rock' !", "kaboum");
        
        String json=testee.toJson(condition);
        Condition reloaded=testee.fromJson(json);
        Assert.assertEquals("Loss of condition data during 1 and a half json roundtrip",json, testee.toJson(reloaded));
    }
}

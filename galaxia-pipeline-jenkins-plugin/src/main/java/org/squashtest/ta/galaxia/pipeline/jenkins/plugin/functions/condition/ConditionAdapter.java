/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.pipeline.jenkins.plugin.functions.condition;

import com.fasterxml.jackson.annotation.JacksonInject;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.InjectableValues;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import org.squashtest.ta.galaxia.metaexecution.conditions.Condition;
import org.squashtest.ta.galaxia.metaexecution.conditions.SQLCondition;

/**
 * This class allows the plugin to build and propagate conditions
 * in pipelines, and as build parameters.
 * 
 * @author edegenetais
 */
public class ConditionAdapter {
    @JsonTypeInfo(use=JsonTypeInfo.Id.CLASS)
    @JsonIgnoreProperties("initialParameterNames")
    private abstract static class ConditionMixin{
        @JsonAnyGetter
        public abstract Object getInitialParm(String name);
    }
    
    @JsonDeserialize(builder = SQLConditionBuilder.class)
    private abstract static class SQLConditionMixin extends ConditionMixin{
        @JsonProperty
        private String targetName;
        @JacksonInject("projectRoot")
        private String projectRoot;
        @JsonProperty
        private String sqlQuery;
        @JsonProperty
        private String expectedResult;
    }
    

    private JsonFactory factory;
    
    public ConditionAdapter(String projectRoot) {
        ObjectMapper mapper=new ObjectMapper();
        mapper.addMixIn(Condition.class, ConditionMixin.class);
        mapper.addMixIn(SQLCondition.class, SQLConditionMixin.class);
        final HashMap<String, Object> injectableValues = new HashMap<String,Object>();
        injectableValues.put("projectRoot", projectRoot);
        mapper.setInjectableValues(new InjectableValues.Std(injectableValues));
        mapper.configure(MapperFeature.CAN_OVERRIDE_ACCESS_MODIFIERS, true);
        factory=new JsonFactory(mapper);
    }
    
    public String toJson(Condition c) throws IOException{
        final StringWriter destination = new StringWriter();
        JsonGenerator generator=factory.createGenerator(destination);
        generator.writeObject(c);
        return destination.toString();
    }
    
    public Condition fromJson(String json) throws IOException{
        JsonParser parser=factory.createParser(json);
        return parser.readValueAs(Condition.class);
    }
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.pipeline.jenkins.plugin.steps;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import org.jenkinsci.plugins.workflow.steps.StepDescriptor;

/**
 * Descriptor types may be created as very simple classes derived from this one.
 * @author edegenetais
 */
public abstract class DescriptorBase extends StepDescriptor {

    protected final Set<Class<?>> requiredContext;
    protected final String stepName;
    protected final String displayName;

    public DescriptorBase(String stepName, String displayName, Class<?>... requiredContextTypes) {
        this.requiredContext = new HashSet<>(Arrays.asList(requiredContextTypes));
        this.displayName = displayName;
        this.stepName = stepName;
    }

    @Override
    public Set<? extends Class<?>> getRequiredContext() {
        return requiredContext;
    }

    @Override
    public String getFunctionName() {
        return stepName;
    }

    @Override
    public String getDisplayName() {
        return displayName;
    }

}

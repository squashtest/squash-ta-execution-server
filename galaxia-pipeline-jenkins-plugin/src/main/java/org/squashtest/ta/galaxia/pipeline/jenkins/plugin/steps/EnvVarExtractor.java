/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.pipeline.jenkins.plugin.steps;

import hudson.EnvVars;
import java.io.IOException;
import java.io.Serializable;
import org.jenkinsci.plugins.workflow.steps.StepContext;

/**
 * Extracts build environment parameter values.
 *
 * @author edegenetais
 */
class EnvVarExtractor implements Serializable{

    private String stepName;
    
    /**
     * Full initialization constructor.
     *
     * @param stepName step that uses this ojbect, for error reporting.
     */
    public EnvVarExtractor(String stepName) {
        this.stepName = stepName;
    }

    public String extractEnvParm(StepContext sc, String varName, String failureMessage) throws IOException, InterruptedException {
        EnvVars env = sc.get(EnvVars.class);
        String nodeName = env.get(varName);
        
        if (nodeName == null) {
            final IllegalStateException illegalStateException = new IllegalStateException(failureMessage);
            sc.onFailure(illegalStateException);
            throw illegalStateException;
        }
        
        return nodeName;
    }

    public String getStepName() {
        return stepName;
    }
}

/**
 * Copyright (c) 2017 Palantir Solutions
 * 
 * Modified by Henix (c) 2020.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.squashtest.ta.galaxia.pipeline.jenkins.plugin.steps;

import hudson.FilePath;
import hudson.model.FileParameterValue;
import hudson.model.ParameterValue;
import hudson.model.ParametersAction;
import hudson.model.Run;
import java.io.File;
import java.io.IOException;
import jenkins.model.Jenkins;
import org.jenkinsci.plugins.workflow.steps.StepContext;
import org.jenkinsci.plugins.workflow.steps.StepExecution;

/**
 * This is a rewriting of the groovy JENKINS-27413-workaround pipeline library.
 * Rewrinting this code as java step in a plugin avoids headaches with SCM access
 * and scriptApproval for bunches of signatures including some flagged as dangerous 
 * in pipeline scripts.
 *  
 * @author edegenetais
 */
class UnstashFileParamStepExecution extends StepExecution implements Runnable {
    
    private UnstashFileParamStep step;
    private EnvVarExtractor envVarExtractor;
    
    public UnstashFileParamStepExecution(StepContext context, UnstashFileParamStep stepDef) {
        super(context);
        this.step = stepDef;
        this.envVarExtractor=new EnvVarExtractor(new UnstashFileParamStep.UnstashFileParamDescriptor().getFunctionName());
    }

    @Override
    public boolean start() throws Exception {
        new Thread(this, toString()).start();
        return false;
    }

    public void run() {
        StepContext sc = getContext();
        try {
            FileParameterValue fileParamValue = extractFileParameterFromContext(sc);
            
            FilePath workspacePath = extractWorkspaceFilePathFromContext(sc);
            
            String destinationName = this.step.getTargetPath() == null ? fileParamValue.getOriginalFileName() : this.step.getTargetPath();
            FilePath destination = workspacePath.child(destinationName);
            destination.copyFrom(fileParamValue.getFile());
            sc.onSuccess(destinationName);
            
        } catch (Exception e) {
            sc.onFailure(e);
        } catch (Error e) {//NOSONAR Sonar dislikes this, but we DO NEED to intercept this and call onFailure otherwise the other thread will remain stuck
            getContext().onFailure(e);
            //and YES, this is a specific treatment of Errors, since we rethrow them afterwards to make sure that the thread dies properly !
            throw e;
        }
    }

    private FilePath extractWorkspaceFilePathFromContext(StepContext sc) throws IllegalStateException, IOException, InterruptedException {
        
        String nodeName=envVarExtractor.extractEnvParm(sc, "NODE_NAME", "No node in current context.");
        
        String workspace = envVarExtractor.extractEnvParm(sc, "WORKSPACE", "No workspace in current context.");
        
        FilePath workspacePath;
        if ("master".equals(nodeName)) {
            workspacePath = new FilePath(new File(workspace));
        } else {
            workspacePath = new FilePath(Jenkins.getInstance().getComputer(nodeName).getChannel(), workspace);
        }
        return workspacePath;
    }

    private FileParameterValue extractFileParameterFromContext(StepContext sc) throws IllegalArgumentException, InterruptedException, IllegalStateException, IOException {
        
        final String name = this.step.getParamName();
        
        ParameterValue paramValue = getParamByName(sc, name);
        
        FileParameterValue fileParamValue=null;
        if (paramValue instanceof FileParameterValue) {
            fileParamValue = (FileParameterValue) paramValue;
        } else {
            final IllegalArgumentException illegalArgumentException = new IllegalArgumentException("Parameter '" + name + "' of type '" + paramValue.getClass().toString() + "' is not a FileParameter");
            sc.onFailure(illegalArgumentException);
            throw illegalArgumentException;
        }
        
        return fileParamValue;
    }

    private ParameterValue getParamByName(StepContext sc, final String name) throws IOException, InterruptedException, IllegalArgumentException, IllegalStateException {
        
        ParametersAction paramsAction = getParametersFromContext(sc);
        
        ParameterValue paramValue = null;
        for (ParameterValue value : paramsAction.getParameters()) {
            if (value.getName().equals(name)) {
                paramValue = value;
            }
        }
        
        if (paramValue == null) {
            final IllegalArgumentException illegalArgumentException = new IllegalArgumentException("[unstashFileParam] No such parameter : " + name);
            sc.onFailure(illegalArgumentException);
            throw illegalArgumentException;
        }
        return paramValue;
    }


    private ParametersAction getParametersFromContext(StepContext sc) throws IllegalStateException, IOException, InterruptedException {
        Run currentBuild = sc.get(Run.class);
        ParametersAction paramsAction = currentBuild.getAction(ParametersAction.class);
        if (paramsAction == null) {
            final IllegalStateException illegalStateException = new IllegalStateException("No parameters found.");
            sc.onFailure(illegalStateException);
            throw illegalStateException;
        }
        return paramsAction;
    }

}

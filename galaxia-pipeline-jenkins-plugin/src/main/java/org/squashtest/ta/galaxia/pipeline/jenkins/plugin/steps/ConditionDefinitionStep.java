/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.pipeline.jenkins.plugin.steps;

import hudson.Extension;
import hudson.FilePath;
import java.io.Serializable;
import org.jenkinsci.plugins.workflow.steps.Step;
import org.jenkinsci.plugins.workflow.steps.StepContext;
import org.jenkinsci.plugins.workflow.steps.StepExecution;
import org.kohsuke.stapler.DataBoundConstructor;
import org.squashtest.ta.galaxia.pipeline.jenkins.plugin.facades.ConditionDefinition;

/**
 * This step creates a json condition definition that can be stored and/or
 * transmitted for future check.
 *
 * @author edegenetais
 */
public class ConditionDefinitionStep extends Step implements Serializable {

    private ConditionDefinition definition;

    @DataBoundConstructor
    public ConditionDefinitionStep(ConditionDefinition definition) {
        this.definition = definition;
    }

    public ConditionDefinition getDefinition() {
        return definition;
    }

    @Override
    public StepExecution start(StepContext sc) throws Exception {
        try{
        return new ConditionDefinitionExecution(sc, this);
        }catch(Exception e){
            sc.onFailure(e);
            throw e;
        }
    }

    @Extension
    public static class ConditionDefinitionDescriptor extends DescriptorBase{

        public ConditionDefinitionDescriptor() {
            super("jsonTACondition", "Creates a json definition of a stage transition condition on a TA test project.", FilePath.class);
        }

    }
}

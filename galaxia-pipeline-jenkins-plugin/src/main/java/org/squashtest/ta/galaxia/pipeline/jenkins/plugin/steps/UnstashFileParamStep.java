/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.pipeline.jenkins.plugin.steps;

import hudson.EnvVars;
import hudson.Extension;
import hudson.FilePath;
import hudson.model.Run;
import java.io.Serializable;
import org.jenkinsci.plugins.workflow.steps.Step;
import org.jenkinsci.plugins.workflow.steps.StepContext;
import org.jenkinsci.plugins.workflow.steps.StepExecution;
import org.kohsuke.stapler.DataBoundConstructor;

/**
 * This step is a plugin-based workaround for the JENKINS-27413 bug.
 * It allows pipeline to properly use FileParameter contents.
 * @author edegenetais
 */
public class UnstashFileParamStep extends Step implements Serializable{

    private String paramName;
    
    private String targetPath;

    public String getParamName() {
        return paramName;
    }

    public String getTargetPath() {
        return targetPath;
    }

    @DataBoundConstructor
    public UnstashFileParamStep(String paramName, String targetPath) {
        this.paramName = paramName;
        this.targetPath = targetPath;
    }

    @Override
    public StepExecution start(StepContext sc) throws Exception {
        try {
            return new UnstashFileParamStepExecution(sc, this);
        } catch (Exception e) {
            sc.onFailure(e);
            throw e;
        }
    }

     @Extension
    public static class UnstashFileParamDescriptor extends DescriptorBase{
        
        public UnstashFileParamDescriptor() {
            super(
                    "unstashFileParam", 
                    "Step to work around JENKINS-27413 (file parameters are not copied to workspace).", 
                    Run.class,EnvVars.class,FilePath.class
            );
        }
        
    }
    

}

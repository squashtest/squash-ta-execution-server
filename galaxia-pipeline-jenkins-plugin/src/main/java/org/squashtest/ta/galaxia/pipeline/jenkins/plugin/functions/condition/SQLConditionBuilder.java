/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.squashtest.ta.galaxia.pipeline.jenkins.plugin.functions.condition;

import com.fasterxml.jackson.annotation.JacksonInject;
import com.fasterxml.jackson.annotation.JsonSetter;
import org.squashtest.ta.galaxia.metaexecution.conditions.InvalidConditionSetupException;
import org.squashtest.ta.galaxia.metaexecution.conditions.SQLCondition;

/**
 *
 * @author edegenetais
 */
public class SQLConditionBuilder {

    private String targetName;
    private String projectRoot;
    private String sqlQuery;
    private String expectedResult;

    @JacksonInject(value = "projectRoot")
    public SQLConditionBuilder withProjectRoot(String root) {
        projectRoot = root;
        return this;
    }

    @JsonSetter(value = "targetName")
    public SQLConditionBuilder withTargetName(String name) {
        targetName = name;
        return this;
    }

    @JsonSetter(value = "sqlQuery")
    public SQLConditionBuilder withSqlQuery(String query) {
        sqlQuery = query;
        return this;
    }

    @JsonSetter(value = "expectedResult")
    public SQLConditionBuilder withExpectedResult(String expected) {
        expectedResult = expected;
        return this;
    }

    public SQLCondition build() throws InvalidConditionSetupException {
        return new SQLCondition(projectRoot, targetName, sqlQuery, expectedResult);
    }

}

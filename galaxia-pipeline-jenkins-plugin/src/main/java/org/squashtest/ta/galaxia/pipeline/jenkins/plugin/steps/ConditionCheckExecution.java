/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.pipeline.jenkins.plugin.steps;

import hudson.FilePath;
import java.io.File;
import org.jenkinsci.plugins.workflow.steps.StepContext;
import org.jenkinsci.plugins.workflow.steps.StepExecution;
import org.squashtest.ta.galaxia.pipeline.jenkins.plugin.functions.condition.ConditionCheck;

/**
 *
 * @author edegenetais
 */
class ConditionCheckExecution extends StepExecution implements Runnable {
    
    private ConditionCheckStep step;

    public ConditionCheckExecution(StepContext sc, ConditionCheckStep step) {
        super(sc);
        this.step = step;
    }

    @Override
    public boolean start() throws Exception {
        new Thread(this, toString()).start();
        return false;
    }

    @Override
    public void run() {
        try {
            FilePath projectRoot = getContext().get(FilePath.class);
            Boolean result;
            if (projectRoot.isRemote()) {
                result=projectRoot.act(new ConditionCheck(step.getDefinition()));
            } else {
                result=new ConditionCheck(step.getDefinition()).perform(new File(projectRoot.getRemote()));
            }
            getContext().onSuccess(result);
        } catch (Exception ex) {
            getContext().onFailure(ex);
        } catch(Error e) {//NOSONAR go fuck yourself Sonar, we DO NEED to intercept this and call onFailure otherwise the other thread will remain stuck
            getContext().onFailure(e);
            //and YES, this is a specific treatment of Errors, since we rethrow them afterwards to make sure that the thread dies properly !
            throw e;
        }
    }
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.pipeline.jenkins.plugin.steps;

import hudson.Extension;
import hudson.Launcher;
import org.jenkinsci.plugins.workflow.steps.Step;
import org.jenkinsci.plugins.workflow.steps.StepContext;
import org.jenkinsci.plugins.workflow.steps.StepExecution;
import org.jenkinsci.plugins.workflow.steps.durable_task.BatchScriptStep;
import org.jenkinsci.plugins.workflow.steps.durable_task.DurableTaskStep;
import org.jenkinsci.plugins.workflow.steps.durable_task.ShellStep;
import org.kohsuke.stapler.DataBoundConstructor;
import org.squashtest.ta.galaxia.pipeline.jenkins.plugin.functions.UnixToWindowsBatchVariableTranslator;

/**
 * Step to avoid writing the same executable launch twice : if you only use an executable name
 * without extension, commandline arguments and shell variables in the form ${NAME}, it will be seamlessly
 * executed in Unix-like or Windows nodes. We use this to simplify the pipeline while allowing
 * distributed execution without bothering about node operating systems.
 * 
 * @author edegenetais
 */
public class LaunchExecutableInAnyOsStep extends Step{
    
    private String command;
    
    @DataBoundConstructor
    public LaunchExecutableInAnyOsStep(String command) {
        this.command=command;
    }

    public String getCommand() {
        return command;
    }

    @Override
    public StepExecution start(StepContext sc) throws Exception {
        try{
            final Launcher launcher = sc.get(Launcher.class);
            if(launcher.isUnix()){
                return new ShellStep(command).start(sc);
            }else{
                UnixToWindowsBatchVariableTranslator variableTranslator=new UnixToWindowsBatchVariableTranslator();
                return new BatchScriptStep(variableTranslator.apply(command)).start(sc);
            }
        }catch(Exception e){
            sc.onFailure(e);
            throw e;
        }
    }
    
    @Extension
    public static class LaunchExecutableInAnyOsStepDescriptor extends DurableTaskStep.DurableTaskStepDescriptor {

        @Override
        public String getFunctionName() {
            return "launchExecutableAnyOs";
        }

        @Override
        public String getDisplayName() {
            return "Executable launch for any OS - no complex shell logic, variable subsitution only";
        }
        
        
    } 
}

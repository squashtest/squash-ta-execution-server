/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.pipeline.jenkins.plugin.functions;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is a utility to translate unix variable bings (<code>${name}</code> syntax)
 * into windows bindings (<code>%name%</code> syntax).
 * @author edegenetais
 */
public class UnixToWindowsBatchVariableTranslator {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(UnixToWindowsBatchVariableTranslator.class);
    
    private static final Pattern UNIX_VAR_FINDER=Pattern.compile("\\$\\{([^\\}]+)}");
    
    public String apply(String original){
        LOGGER.debug("Translating variable constructs from *nix to m$ in: '{}'",original);
        Matcher variableMatcher=UNIX_VAR_FINDER.matcher(original);
	StringBuffer windozCommandBuilder=new StringBuffer();
		
        while(variableMatcher.find()){
            variableMatcher.appendReplacement(windozCommandBuilder,'%'+variableMatcher.group(1)+'%');
        }
        variableMatcher.appendTail(windozCommandBuilder);
        LOGGER.debug("Translated as: '{}'",windozCommandBuilder);
        return windozCommandBuilder.toString();
    }
}

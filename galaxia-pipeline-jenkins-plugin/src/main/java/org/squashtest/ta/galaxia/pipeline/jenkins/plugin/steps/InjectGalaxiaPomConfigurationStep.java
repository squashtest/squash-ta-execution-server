/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.pipeline.jenkins.plugin.steps;

import hudson.EnvVars;
import hudson.Extension;
import hudson.FilePath;
import org.jenkinsci.plugins.workflow.steps.Step;
import org.jenkinsci.plugins.workflow.steps.StepContext;
import org.jenkinsci.plugins.workflow.steps.StepExecution;
import org.kohsuke.stapler.DataBoundConstructor;

/**
 * Step to inject galaxia configuration in a test project's POM.
 * @author edegenetais
 */
public class InjectGalaxiaPomConfigurationStep extends Step{

    @DataBoundConstructor
    public InjectGalaxiaPomConfigurationStep(){/*only exists to receive @DataBoundConstructor*/}

    @Override
    public StepExecution start(StepContext sc) throws Exception {
        return new InjectGalaxiaPomConfigurationExecution(sc);
    }

    @Extension
    public static class InjectGalaxiaPomConfigurationDescriptor extends DescriptorBase{
        
        public InjectGalaxiaPomConfigurationDescriptor(){
            super(
                    "withGalaxiaConfiguration",
                    "Injects the galaxia configuration into the project pom.",
                    EnvVars.class,FilePath.class
                 );
        }
        
        @Override
        public boolean takesImplicitBlockArgument() {
            return true;
        }
    }
}

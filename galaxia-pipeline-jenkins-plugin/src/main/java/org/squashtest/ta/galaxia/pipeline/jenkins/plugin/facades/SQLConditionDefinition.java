/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.pipeline.jenkins.plugin.facades;

import hudson.remoting.VirtualChannel;
import java.io.File;
import java.io.IOException;
import org.jenkinsci.remoting.RoleChecker;
import org.kohsuke.stapler.DataBoundConstructor;
import org.squashtest.ta.galaxia.metaexecution.conditions.InvalidConditionSetupException;
import org.squashtest.ta.galaxia.metaexecution.conditions.SQLCondition;
import org.squashtest.ta.galaxia.pipeline.jenkins.plugin.functions.RemoteInvokeException;
import org.squashtest.ta.galaxia.pipeline.jenkins.plugin.functions.condition.ConditionAdapter;

/**
 * This callable allows defining an {@link SQLCondition} in the pipeline script, while really 
 * executing the real condition definition and associated configuration check work agent side.
 * 
 * @author edegenetais
 */
public class SQLConditionDefinition implements ConditionDefinition {
    private String targetName;
    private String projectRoot;
    private String sqlQuery;
    private String expectedResult;

    @DataBoundConstructor
    public SQLConditionDefinition(String projectRoot, String targetName, String sqlQuery, String expectedResult) {
        this.targetName = targetName;
        this.projectRoot = projectRoot;
        this.sqlQuery = sqlQuery;
        this.expectedResult = expectedResult;
    }

    public String getTargetName() {
        return targetName;
    }

    public String getProjectRoot() {
        return projectRoot;
    }

    public String getSqlQuery() {
        return sqlQuery;
    }

    public String getExpectedResult() {
        return expectedResult;
    }
    
    @Override
    public String invoke(File file, VirtualChannel vc) throws IOException, InterruptedException {
        try {
            this.projectRoot=file.getCanonicalPath();
            return create();
        } catch (InvalidConditionSetupException ex) {
            throw new RemoteInvokeException(ex);
        }
    }

    public String create() throws InvalidConditionSetupException, IOException {
        SQLCondition c=new SQLCondition(projectRoot, targetName, sqlQuery, expectedResult);
        return new ConditionAdapter(projectRoot).toJson(c);
    }

    @Override
    public void checkRoles(RoleChecker rc) throws SecurityException {
        //noop - until we find out what we might have to do with this ... IF there is indeed something to do.
    }
    
}

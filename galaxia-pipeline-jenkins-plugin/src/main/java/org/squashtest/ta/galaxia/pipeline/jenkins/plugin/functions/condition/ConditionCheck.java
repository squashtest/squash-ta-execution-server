/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.pipeline.jenkins.plugin.functions.condition;

import hudson.FilePath;
import hudson.remoting.VirtualChannel;
import java.io.File;
import java.io.IOException;
import org.jenkinsci.remoting.RoleChecker;
import org.squashtest.ta.galaxia.metaexecution.conditions.Condition;
import org.squashtest.ta.galaxia.metaexecution.conditions.ConditionException;
import org.squashtest.ta.galaxia.pipeline.jenkins.plugin.functions.RemoteInvokeException;

/**
 * This callable allows the execution of the condition check agent side.
 * It must be invoked through the test project root {@link FilePath}, 
 * or if used locally, on the project root {@link File}.
 * @author edegenetais
 */
public class ConditionCheck implements FilePath.FileCallable<Boolean>{

    private String json;

    public ConditionCheck(String json) {
        this.json = json;
    }
    
    @Override
    public Boolean invoke(File file, VirtualChannel vc) throws IOException, InterruptedException {
        try {
            return perform(file);
        } catch (ConditionException ex) {
            throw new RemoteInvokeException(ex);
        }
    }

    /**
     * Performs the real condition check.
     * @param file the project root for condition definition.
     * @return {@link Boolean#TRUE} if the condition is fulfilled, {@link Boolean#FALSE} otherwise.
     * @throws IOException
     * @throws ConditionException 
     */
    public Boolean perform(File file) throws IOException, ConditionException {
        ConditionAdapter adapter=new ConditionAdapter(file.getCanonicalPath());
        Condition c=adapter.fromJson(json);
        return c.check();
    }

    @Override
    public void checkRoles(RoleChecker rc) throws SecurityException {
        //noop - until we find out what we might have to do with this ... IF there is indeed something to do.
    }

}

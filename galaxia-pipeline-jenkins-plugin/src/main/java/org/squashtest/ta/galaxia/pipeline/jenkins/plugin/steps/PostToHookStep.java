/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.pipeline.jenkins.plugin.steps;

import hudson.Extension;
import java.io.Serializable;
import org.jenkinsci.plugins.workflow.steps.Step;
import org.jenkinsci.plugins.workflow.steps.StepExecution;
import org.kohsuke.stapler.DataBoundConstructor;
import org.jenkinsci.plugins.workflow.steps.StepContext;

/**
 * postToHook step to send POST query to webhook indifferenly from a Windows or
 * *nix node.
 *
 * @author edegenetais
 */
public class PostToHookStep extends Step implements Serializable {

    private static final long serialVersionUID = 1L;
    
    String url;

    String message;

    @DataBoundConstructor
    public PostToHookStep(String url, String message) {
        this.url = url;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public String getUrl() {
        return url;
    }

    @Override
    public StepExecution start(StepContext sc) throws Exception {
        return new PostToHookExecution(sc, this);
    }

    @Extension
    public static class PostToHookStepDescriptor extends DescriptorBase{

        public PostToHookStepDescriptor(){
            super(
                    "postToHook",
                    "Post to a hook in java to succeed in windows as well as linux"
                    );
        }
    } 
    
}

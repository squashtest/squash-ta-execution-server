/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.pipeline.jenkins.plugin.steps;

import hudson.EnvVars;
import hudson.Extension;
import hudson.FilePath;
import hudson.model.TaskListener;
import java.io.Serializable;
import org.jenkinsci.plugins.workflow.steps.Step;
import org.jenkinsci.plugins.workflow.steps.StepContext;
import org.jenkinsci.plugins.workflow.steps.StepExecution;
import org.kohsuke.stapler.DataBoundConstructor;
import org.slf4j.LoggerFactory;

/**
 * A step to create the filtered test suite for a given Squash TA test stage in the pipeline.
 * @author edegenetais
 */
public class FilterTestListStep extends Step implements Serializable{
    
    private static final long serialVersionUID=1L;
    
    private String testSpec;

    private String testSuitePath;

    /**
     * Full initialization constructor.
     * @param testSpec comma separated list of test filter specifications.
     * @param testSuitePath path to the initial test suite.
     */
    @DataBoundConstructor
    public FilterTestListStep(String testSpec, String testSuitePath) {
        LoggerFactory.getLogger(FilterTestListStep.class).debug("Step created with testSpec={} and testSuitePath={}",testSpec,testSuitePath);
        this.testSpec = testSpec;
        this.testSuitePath = testSuitePath;
    }

    public String getTestSpec() {
        return testSpec;
    }

    public String getTestSuitePath() {
        return testSuitePath;
    }
    
    @Override
    public StepExecution start(StepContext sc) throws Exception {
        return new FilterTestListExecution(sc,this);
    }
    
    @Extension
    public static class FilterTestListStepDescriptor extends DescriptorBase{

        public FilterTestListStepDescriptor() {
            super(
                    "createStageTestList", 
                    "Filters the test suite to run only the tests scheduled for a given test stage.", 
                    EnvVars.class, FilePath.class, TaskListener.class
            );
        }

    }
}

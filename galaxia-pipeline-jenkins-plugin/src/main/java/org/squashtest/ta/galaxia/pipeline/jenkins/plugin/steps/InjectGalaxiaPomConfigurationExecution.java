/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.pipeline.jenkins.plugin.steps;

import hudson.EnvVars;
import hudson.FilePath;
import java.io.File;
import java.io.IOException;
import org.jenkinsci.plugins.workflow.steps.BodyExecutionCallback;
import org.jenkinsci.plugins.workflow.steps.BodyInvoker;
import org.jenkinsci.plugins.workflow.steps.StepContext;
import org.jenkinsci.plugins.workflow.steps.StepExecution;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.galaxia.pipeline.jenkins.plugin.functions.GalaxiaInjectorForJenkins;

/**
 * Process class for the Galaxia configuration injection step.
 * @author edegenetais
 */
public class InjectGalaxiaPomConfigurationExecution extends StepExecution {

    private static final String GALAXIA_INJECTED_POM_KEY = "GALAXIA_INJECTED_POM";
    private static final String WORKSPACE_KEY = "WORKSPACE";
    
    public InjectGalaxiaPomConfigurationExecution(StepContext sc) {
        super(sc);
    }

    @Override
    public boolean start() throws Exception {
        try{
        FilePath pom=getContext().get(FilePath.class).child("pom.xml");
        EnvVars env=getContext().get(EnvVars.class);
        String galaxiaPomName;
        if(pom.isRemote()){
            galaxiaPomName=pom.act(new GalaxiaInjectorForJenkins());
        }else{
            File injectedPom=new GalaxiaInjectorForJenkins().inject(env.get(WORKSPACE_KEY)+"/pom.xml");
            galaxiaPomName=injectedPom.getName();
        }
        env.put(GALAXIA_INJECTED_POM_KEY, galaxiaPomName);
        BodyInvoker bodyInvoker=getContext().newBodyInvoker();
        bodyInvoker.withCallback(new InjectGalaxiaConfigurationExecutionCallback(galaxiaPomName));
        bodyInvoker.withContext(env).start();
        
        return false;
        }catch(Exception e){
            getContext().onFailure(e);
            throw e;
        }
    }

    private static class InjectGalaxiaConfigurationExecutionCallback extends BodyExecutionCallback {
        
        private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(InjectGalaxiaConfigurationExecutionCallback.class);
        
        private String galaxiaPomName;

        public InjectGalaxiaConfigurationExecutionCallback(String galaxiaPomName) {
            this.galaxiaPomName = galaxiaPomName;
        }
        
        @Override
        public void onSuccess(StepContext sc, Object o) {
            cleanUpGalaxiaPom(sc);
            sc.onSuccess(o);
        }

        private void cleanUpGalaxiaPom(StepContext sc) {
            cleanUpGalaxiaPom(sc, null);
        }

        private void cleanUpGalaxiaPom(StepContext sc, Throwable t) {
            try {
                FilePath cwd=sc.get(FilePath.class);
                FilePath galaxiaPom=cwd.child(galaxiaPomName);
                if(galaxiaPom.exists() && galaxiaPom.delete()){
                    LOGGER.debug("Removed Galaxia injected pom {} after use",galaxiaPom.toURI());
                }else if(galaxiaPom.exists()){
                    LOGGER.warn("Failed to remove Galaxia injected pom {}",galaxiaPom.toURI());
                }
            } catch (IOException | InterruptedException ex) {
                if(t==null){
                    LOGGER.warn("Failed to remove Galaxia injected pom.",galaxiaPomName,ex);
                }else{
                    t.addSuppressed(ex);
                    LOGGER.warn("Failed to remove Galaxia injected pom {}",galaxiaPomName);
                }
            }
        }
        
        @Override
        public void onFailure(StepContext sc, Throwable thrwbl) {
            cleanUpGalaxiaPom(sc, thrwbl);
            sc.onFailure(thrwbl);
        }
    }
    
}

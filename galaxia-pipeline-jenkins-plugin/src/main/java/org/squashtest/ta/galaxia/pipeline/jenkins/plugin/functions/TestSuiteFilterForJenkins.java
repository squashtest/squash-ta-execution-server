/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.pipeline.jenkins.plugin.functions;

import hudson.FilePath;
import hudson.remoting.VirtualChannel;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import org.jenkinsci.remoting.RoleChecker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.galaxia.metaexecution.enginelink.TestSuiteFilter;
import org.squashtest.ta.squash.ta.addon.logic.kit.testspecs.SpecSyntaxException;

/**
 * This class is a facade to make {@link TestSuiteFilter} use in jenkins easier.
 * @author edegenetais
 */
public class TestSuiteFilterForJenkins implements FilePath.FileCallable<String>{
    private static final Logger LOGGER = LoggerFactory.getLogger(TestSuiteFilterForJenkins.class);
    
    private String[] specSet;
    
    public TestSuiteFilterForJenkins(){
        this.specSet=new String[0]; 
    }
    
    public TestSuiteFilterForJenkins(String... specSet) {
        this.specSet=specSet;
    }

    public String[] getSpecSet() {
        return Arrays.copyOf(specSet, specSet.length);
    }

    public void setSpecSet(String[] specSet) {
        this.specSet = Arrays.copyOf(specSet, specSet.length);
    }
    
    /**
     * Filter the json test suite (execution order).
     
     * @see TestSuiteFilter#filter(java.io.File) 
     * @param jsonPath path to the json testsuite input file.
     * @return the path of the json filtered testsuite output file.
     * @throws IOException is case of failure while reading the initial Json 
     * or writing the filtered Json.
     * @throws org.squashtest.ta.squash.ta.addon.logic.kit.testspecs.SpecSyntaxException 
     * if one or more test specification are not recognized as a valid pattern.
     */
    public String filter(String jsonPath) throws IOException, SpecSyntaxException{
        LOGGER.debug("Filtering json from {}",jsonPath);
        TestSuiteFilter filter=new TestSuiteFilter(specSet);
        final String genericJsonPath = jsonPath.replaceAll("\\\\","/");
        LOGGER.trace("Genericised as {}",genericJsonPath);
        File filteredFile=filter.filter(new File(genericJsonPath));
        return filteredFile.getCanonicalPath();
    }
    
    @Override
    public String invoke(File file, VirtualChannel vc) throws IOException, InterruptedException {
        try {
            return filter(file.getCanonicalPath());
        } catch (SpecSyntaxException ex) {
            throw new RemoteInvokeException(ex);
        }
    }

    @Override
    public void checkRoles(RoleChecker rc){
        //noop - we'll figure out what's to do next - IF there is something to do indeed !
    }
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.pipeline.jenkins.plugin.functions;

import hudson.FilePath;
import hudson.remoting.VirtualChannel;
import java.io.File;
import java.io.IOException;
import org.jenkinsci.remoting.RoleChecker;
import org.squashtest.ta.galaxia.metaexecution.enginelink.EngineLinkException;
import org.squashtest.ta.galaxia.metaexecution.enginelink.GalaxiaInjector;

/**
 * This class is a facade to make galaxia injector use in jenkins easier.
 * @author edegenetais
 */
public class GalaxiaInjectorForJenkins implements FilePath.FileCallable<String>{
    
    public File inject(String pomPath) throws EngineLinkException{
        return new GalaxiaInjector().inject(new File(pomPath));
    }

    @Override
    public String invoke(File file, VirtualChannel vc) throws IOException, InterruptedException {
        try {
            return new GalaxiaInjector().inject(file).getName();
        } catch (EngineLinkException ex) {
            throw new RemoteInvokeException(ex);
        }
    }

    @Override
    public void checkRoles(RoleChecker rc) {
        //noop - we'll figure out what we've got to do here (if anything at all)
    }
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.pipeline.jenkins.plugin.steps;

import hudson.EnvVars;
import hudson.FilePath;
import hudson.model.TaskListener;
import java.io.File;
import java.io.IOException;
import org.jenkinsci.plugins.workflow.steps.StepContext;
import org.jenkinsci.plugins.workflow.steps.SynchronousNonBlockingStepExecution;
import org.squashtest.ta.galaxia.pipeline.jenkins.plugin.functions.TestSuiteFilterForJenkins;
import org.squashtest.ta.squash.ta.addon.logic.kit.testspecs.SpecSyntaxException;

/**
 * Filter test list process class.
 * @author edegenetais
 */
public class FilterTestListExecution extends SynchronousNonBlockingStepExecution<String> {
    
    private FilterTestListStep step;

    public FilterTestListExecution(StepContext sc, FilterTestListStep step) {
        super(sc);
        this.step = step;
    }

    @Override
    protected String run() throws Exception {
        
        String filteredSuite;
        final FilePath workspace = getContext().get(FilePath.class);
        if(workspace.isRemote()){
            filteredSuite=doRemoteFilter(workspace);
        }else{
            filteredSuite = doLocalFilter();
        }
        return filteredSuite;
    }

    private String doRemoteFilter(FilePath workspace) throws IOException, InterruptedException {
        String filteredSuite;
        //This is (most probably) remote, let's make it remote
        FilePath initialTsPath=workspace.child(step.getTestSuitePath());
        String[] filter=step.getTestSpec().split(",");
        filteredSuite=initialTsPath.act(new TestSuiteFilterForJenkins(filter));
        if(initialTsPath.delete()){
            getContext().get(TaskListener.class).getLogger().println("Deleted initial test suite "+initialTsPath.absolutize().getRemote());
        }else{
            getContext().get(TaskListener.class).getLogger().println(initialTsPath.absolutize().getRemote()+" leaked ! Finish him!");
        }
        return filteredSuite;
    }
    
    private String doLocalFilter() throws IOException, SpecSyntaxException, InterruptedException {
        
        EnvVars environment = getContext().get(EnvVars.class);
        
        //in this case everybody is in the same boat, let's take the plainest path
        String[] specSet=step.getTestSpec().split(",");
        String initialTestSuitePath;
        if (new File(step.getTestSuitePath()).isAbsolute()) {
            initialTestSuitePath=step.getTestSuitePath();
        } else {
            initialTestSuitePath = environment.get("WORKSPACE") + '/' + step.getTestSuitePath();
        }
        
        String filteredSuite = new TestSuiteFilterForJenkins(specSet).filter(initialTestSuitePath);
        File tsFile=new File(initialTestSuitePath);
        if(tsFile.delete()){
            getContext().get(TaskListener.class).getLogger().println("Deleted initial test suite "+tsFile.getAbsolutePath());
        }else{
            getContext().get(TaskListener.class).getLogger().println(tsFile.getAbsolutePath()+" leaked ! Finish him!");
            tsFile.deleteOnExit();
        }
        return filteredSuite;
    }

}

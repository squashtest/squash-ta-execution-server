..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.


#################################
Execution Server - Generic Update
#################################

.. contents::
   :local:

|

********
Overview
********

This page will help you upgrade your |squashTF| **Execution Server** and your job collection to the latest version available.

Its instructions are generic and need to be followed for each update; however the process can have additionnal steps for some versions.

These specific instructions are documented in their own sections of this guide.

.. note::
    The following procedure is identical whether your Execution Server runs on a Windows or a Linux computer.

.. warning::
    We recommend updating step-by-step from your current version of |squashTF| **execution server** to the next until you reach the latest.

    If you wish to jump ahead to the latest release please read carefully the instructions dedicated to each version of the Execution Server you pass by.

The guide is divided in 3 major parts |_|:

    - First the process to update a physical install of |squashTF| **Execution Server** on a local system.

    - Second the process to update a Docker install.

    - Third the various configurations to update in Jenkins. This part is necessary for both install.

|

--------

**********************
Physical Server update
**********************


=====
Setup
=====


1. Download the version of |squashTF| you wish to install `here <https://squash-tf.readthedocs.io/en/latest/download.html#squash-tf-execution-server>`_.

----------

2. Stop your current server.

----------

3. Install the new TF Execution Server in a separate location as a classic new installation.

----------

=======================
Server & jobs migration
=======================

4. Check the page dedicated to updating to the chosen version of the server and follow the specific instructions in the **Before migrating jobs** section.

--------

5. Copy your custom jobs from **{current_server_path}/execution_home/jobs/** to **{new_server_path}/execution_home/jobs/**

.. warning::
    Do not copy Squash\ |_|\ TF templates (beginning by **00**) and **SquashTAConditionSweepJobs**, so avoid copying the whole jobs directory.

.. image:: ../../_static/execution-server-generic-update/server-update-jobs.png
   :alt: Copying your existing Jenkins' jobs.
   :align: center
   :target: ../../_static/execution-server-generic-update/server-update-jobs.png

----------

6. Copy and replace all .xml files from **{current_server_path}/execution_home/** to **{new_server_path}/execution_home/**

.. image:: ../../_static/execution-server-generic-update/server-update-copy-xml.png
   :alt: Copying your existing Jenkins' jobs.
   :align: center
   :target: ../../_static/execution-server-generic-update/server-update-copy-xml.png

----------

7. Copy and replace the file **server.xml** located in **{current_server_path}/apache-tomcat-8.5.16/conf/** to **{new_server_path}/apache-tomcat-8.5.16/conf/**

.. image:: ../../_static/execution-server-generic-update/server-update-server-xml.png
   :alt: Copying your existing Jenkins' jobs.
   :align: center
   :target: ../../_static/execution-server-generic-update/server-update-server-xml.png

----------


==============
Optional steps
==============


8. If you wish you can copy and replace the **users** directory from **{current_server_path}/execution_home/** to **{new_server_path}/execution_home/**

.. tip::
    If this step isn't done the only user will be the administrator. By default -> login |_|: |_| admin // password |_|: |_| admin


.. _execution.server.update.option.anchor:

----------

9. If you wish you can copy Jenkins plugins you personally installed from **{current_server_path}/execution_home/plugins/** to **{new_server_path}/execution_home/plugins/**

.. caution::
    Be sure to not overwrite one of the plugins we provide with a newer or older version to ensure compatibility.
    You could also re-install your plugins using the Jenkins interface later.

----------


=================================
TF |_| - |_| TM connection update
=================================


10. Copy and replace the file **ta.linkConf.properties** from the root of your old execution server to the root of your new one

.. image:: ../../_static/execution-server-generic-update/server-update-taLinkConf.png
   :alt: Transferring ta.linkConf.properties.
   :align: center
   :target: ../../_static/execution-server-generic-update/server-update-taLinkConf.png

|

--------

*************
Docker update
*************

 If your |squashTF| **Execution Server** is deployed in a Docker container the updating procedure is slightly different.

 In order to keep your Jenkins jobs parameterized and your custom Jenkins configuration, you will need to extract data from you old container (default name |_|: squash-ta-server) and provide it to your new one.

 .. warning::
    We recommend creating a backup of your current Execution Server container before beginning the update process to prevent the loss of data.

 .. tip::
    The files to keep during the update are the same as those transferred during a physical install.

    If you are an experimented Docker user you can choose your preferred method to access and move the data between your containers.

    Simply follow the *physical server update* process to know what files and directories to keep and transfer over.

 .. note::
    In this guide we will assume the name of the container running |squashTF| **Execution Server** and the path to the server's files are the default (i.e. respectively squash-execution-server and /opt/squash-ta-server).

----------

 In this guide we will use the **docker cp** command exclusively to keep things simple but you could also use Docker Volumes or other means to access data from your containers.

 The *cp* command can be used on a running container but we recommend stopping the |squashTF| **Execution Server** container during the update process.

 1. Copy the content of **/opt/squash-ta-server** from the container to the host system |_|:

    .. list-table::

        * - docker cp squash-execution-server:/opt/squash-ta-server {path_of_choice}/squash-server-copy

----------

 2. Check the page dedicated to updating to the chosen version of the server and follow the specific instructions in the **Before migrating jobs** section.

----------

 3. Filter the **execution_home** directory located in squash-server-copy |_|:

    * Keep the **jobs** and **users** directories and all **.xml** files before deleting all the other files and directories in **execution_home**. If you use Jenkins Agents, you can also keep the **nodes** directory. To do so, use the following command |_|:

        .. code-block:: bash

            rm -r {path/to/squash-server-copy}/execution_home/!(\*.xml|"jobs"|"users"|"nodes")

    * Delete the templates provided (beginning by **00** and **SquashTAConditionSweepJobs**) in the **jobs** directory.

----------

 4. Stop (if it was not already done) and rename your old |squashTF| container to avoid naming conflict with the new one you will be creating shortly.

----------

 5. Download the newest version of |squashTF| **Execution Server** Docker image from `this page <https://squash-tf.readthedocs.io/en/latest/download.html#squash-tf-execution-server>`_ or on `DockerHub <https://hub.docker.com/r/squashtest/squash-tf-execution-server>`_.

----------

 6. Follow :ref:`these instructions <execution.server.install.docker.anchor>` to launch the new |squashTF| **Execution Server**.

----------

 7. Stop the new container for now |_|:

    .. list-table::

        * - docker stop squash-tf-execution-server

----------

 8. Copy the content of **{path/to/squash-server-copy}/execution_home** to the **execution_home** directory in the new container |_|:

    .. list-table::

        * - docker cp {/path/of/your/choice/squash-server-copy}/execution_home/. squash-execution-server:/opt/squash-ta-server/execution_home

----------

 9. Copy the server.xml file from **{path/to/squash-server-copy}/apache-tomcat-8.5.16/conf/** in the **/opt/squash-ta-server/apache-tomcat-8.5.16/conf/** in the new container |_|:

    .. list-table::

        * - docker cp {/path/of/your/choice/squash-server-copy}/apache-tomcat-8.5.16/conf/server.xml squash-execution-server:/opt/squash-ta-server/apache-tomcat-8.5.16/conf/

----------

 10. Copy any other data you wish to transfer over to the updated execution server.

----------

 11. If you wish you can follow step 9 or 10 of the :ref:`physical update process <execution.server.update.option.anchor>` to keep any Jenkins plugins you may have installed and/or update your connection with |squashTM| .

----------

 12. Use the docker start command to restart your server properly |_|:

    .. code-block:: bash

        docker start squash-tf-execution-server

|

----------

****************************
Jenkins configuration update
****************************

1. Start the new Execution Server and access the corresponding Jenkins page (login with an administrator account).

.. image:: ../../_static/execution-server-generic-update/server-update-jenkins-home.png
   :alt: Connecting to Jenkins as admin.
   :align: center
   :target: ../../_static/execution-server-generic-update/server-update-jenkins-home.png

----------

2. Go to ``Manage Jenkins`` followed by ``Configure system`` |_|:

        * Find the section ``Global Pipeline Libraries`` and the subsection ``Source Code Management``.
        * In the field ``Repository URL`` replace the path of your old version with the new one (everything before **/execution_home/pipeline-libs/..** ).
        * Check that the Jenkins url is the right one for your new execution server (**TM-TF** link).
        * Click on ``Save``.

.. image:: ../../_static/execution-server-generic-update/server-update-jenkins-configure-system.png
   :alt: Configuring Jenkins
   :align: center
   :target: ../../_static/execution-server-generic-update/server-update-jenkins-configure-system.png

.. image:: ../../_static/execution-server-generic-update/server-update-jenkins-SCM.png
   :alt: Repository SCM Jenkins
   :align: center
   :target: ../../_static/execution-server-generic-update/server-update-jenkins-SCM.png

----------

3. Go to ``Global Tool Configuration`` |_|:

        * Find the ``Maven`` section and click on **Maven installations...** if the section is empty.
        * In the field ``MAVEN_HOME`` replace the path of your old version with the new one (everything before **/apache-maven-3.5.0**).
        * Click on ``Save``.

.. figure:: ../../_static/execution-server-generic-update/server-update-jenkins-tools-config-01.png
    :alt: Configuring Jenkins Tools
    :align: center
    :target: ../../_static/execution-server-generic-update/server-update-jenkins-tools-config-01.png

    Empty Maven section


.. figure:: ../../_static/execution-server-generic-update/server-update-jenkins-tools-config-02.png
    :alt: Configuring Jenkins Tools
    :align: center
    :target: ../../_static/execution-server-generic-update/server-update-jenkins-tools-config-02.png

    New path
 
----------

4. If you have some of your own Jenkins plugins that you didn't copy to the new plugin directory in the new execution server you can download them again at this time in the process.

----------

5. In case of a **TM-TF** link remember to also check Jenkins url in Squash TM (``Administration`` then ``Automation Servers``) |_|:

  .. image:: ../../_static/execution-server-generic-update/TM-admin.png
     :alt: checking Jenkins url in Squash-TM
     :align: center
     :target: ../../_static/execution-server-generic-update/TM-admin.png

----------

6. Check the page dedicated to updating to the chosen version of the server and follow the specific instructions in the **After migrating jobs** section.

----------

7. Finally test your migrated jobs to confirm the update was successfull and your previously working jobs still behave as you want them to.

|

----------

*********************
Jenkins Agents update
*********************

See :ref:`here <execution.agent.update.anchor>`

|

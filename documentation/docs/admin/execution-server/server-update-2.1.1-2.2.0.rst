..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.



######################################
Specific migration from 2.1.1 to 2.2.0
######################################

.. contents::
   :local:

|

=====================
Before migrating jobs
=====================

No particular task needs to be done at this point for this update. Go back to the generic update procedure for now.

|

--------

====================
After migrating jobs
====================

|

********************************
New reports publication settings
********************************

Overview
========

Since |squashTF| **Execution Server 2.2.0**, we have changed our jobs template settings for the reports publication. Now the publication is done according to the operation executed |_|:

* If it's a "list" operation, then we publish the tests list report.
* If it's a "run" operation, then we publish the tests execution report.

The change have been made to these templates |_|:

* 00Squash-TA_Template.
* 00Squash-TF-Java_JUnit_Runner_Template.

**Already existing jobs will have to be updated manually**

.. note::
   This new reports publication settings is mandatory for tests projects using |_|:

   * |squashTF| **Java Junit Runner** |_|: all versions.
   * |squashTF| **Keyword framework (SKF)** |_|: for version 1.13.0 and above.


Update existing jobs
====================

.. admonition:: Prerequisite

   The ``Flexible publish`` plugin should have been installed in Jenkins (done in |squashTF| **Execution Server 2.2.0**).

In your job, go to ``Post-build Actions`` and add a post-build action |_|:

.. image:: ../../_static/execution-server-job-update-version-2.1.1/jenkins-job-update-1.png
   :alt: Updating your existing Jenkins' jobs.
   :align: center
   :target: ../../_static/execution-server-job-update-version-2.1.1/jenkins-job-update-1.png

Select ``Flexible publish`` |_|:

.. image:: ../../_static/execution-server-job-update-version-2.1.1/jenkins-job-update-2.png
   :alt: Updating your existing Jenkins' jobs.
   :align: center
   :target: ../../_static/execution-server-job-update-version-2.1.1/jenkins-job-update-2.png

Configure the conditional action for the goal **list** as shown in the screen below |_|:

.. image:: ../../_static/execution-server-job-update-version-2.1.1/jenkins-job-update-3.png
   :alt: Updating your existing Jenkins' jobs.
   :align: center
   :target: ../../_static/execution-server-job-update-version-2.1.1/jenkins-job-update-3.png

Then add another conditional action and do the same for the goal **run** |_|:

.. image:: ../../_static/execution-server-job-update-version-2.1.1/report-run-ta.png
      :alt: Updating your existing Jenkins' jobs. 
      :align: center
      :target: ../../_static/execution-server-job-update-version-2.1.1/report-run-ta.png

For jobs which run tests using |squashTF| **Java Junit Runner**, also add the "Squash_TF_HTML_Debug_Report" in the goal **run** |_|:

.. image:: ../../_static/execution-server-job-update-version-2.1.1/report-run-junit.png
      :alt: Updating your existing Jenkins' jobs. 
      :align: center
      :target: ../../_static/execution-server-job-update-version-2.1.1/report-run-junit.png

Finally delete all previous post build actions for publication.

.. note:: As explain in :ref:`the specific update documentation from 2.0.0 to 2.1.1 <update_publish_url>`, the report title should contain "_" (and not a space), and ``Escape underscores in Report Title`` should be unchecked.

|

------------------

********************************************************
Rename job execution according to the executed operation
********************************************************

Overview
========
In |squashTF| **Execution Server 2.2.0**, we introduce the renaming of a job execution (according to the executed operation).
To do so, we use the Jenkins ``Build Name and Description Setter`` plugin. This plugin sets the display name of a build to something other than #1, #2, #3, ... so that you can use an identifier that makes more sense in your context (see image).

.. image:: ../../_static/execution-server-job-update-version-2.1.1/build-name-update-1.png
      :alt: Updating your existing Jenkins' jobs. 
      :align: center
      :target: ../../_static/execution-server-job-update-version-2.1.1/build-name-update-1.png


Update existing jobs
====================

.. admonition:: Prerequisite

   The ``Build Name and Description Setter`` plugin should have been installed in Jenkins (done in |squashTF| **Execution Server 2.2.0**).

In your job, go the ``Build Environment`` section and check the box ``Set Build Name``.

In the ``Build Name``, just add |_|:

.. list-table::

    * - #${BUILD_NUMBER}-${operation}

.. image:: ../../_static/execution-server-job-update-version-2.1.1/build-name-update-2.png
      :alt: Updating your existing Jenkins' jobs. 
      :align: center
      :target: ../../_static/execution-server-job-update-version-2.1.1/build-name-update-2.png

|

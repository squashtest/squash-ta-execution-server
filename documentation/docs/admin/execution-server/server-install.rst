..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.



#############################
Execution Server installation
#############################

.. contents::
   :local:

|

.. warning:: In **2.3.0-RELEASE version of the server**, we updated Jenkins to 2.190.1 version. Since 2.165 and newer version, Jenkins no longer supports the old (-remoting) mode in either the client or server. If you have installed a 2.3.0-RELEASE of the server and want to install an agent, you need to download **2.3.0-RELEASE (or newer) version of the agent**.


****************
Headless install
****************

Linux procedure
===============

Pre-requisites
--------------

* A version of the java 8 jdk must be installed on the system.
* The bin directory from this JDK must be included in the PATH shell variable. If necessary, run the following command in the shell you'll be using |_|::

    export PATH=</path/to/the/JDK>/bin:$PATH

Procedure
---------

To perform a headless server installation under linux, you need to perform the following steps |_|:

* Create the installation parameter file.
* Launch the installer from the command line with the installation paramameter file path as argument.

**> Installation parameter file**

The parameter file can be generated from the attached template. Please make sure to adjust the following parameters to the real values on your system |_|:

* **jdkPath** |_|: Path to a java 8 JDK that will be used to run the server.
* **installPath** |_|: Target installation path for the |squashTF| server

:download:`linux-headless-install-parms-template.xml <../../_static/install-headless/linux-headless-install-parms-template.xml>`

**> Installer execution**


To actually install the |squashTF| server, please run the following command |_|::

  java -jar <path/to/the/>squash-tf-execution-server-bundle-<version>-linux-installer.jar <path/to/the/xml-installation-parameter-file>

.. note:: | You can now access to your execution server at |_|: <host_url>:8080/jenkins
          | The default login is |_|: admin |_| / |_| admin

|

Windows procedure
=================

Pre-requisites
--------------

* A version of the java 8 jdk must be installed on the system.
* The bin directory from this JDK must be included in the PATH shell variable. If necessary, run the following command in the shell you'll be using |_|::

    set PATH=<\path\to\the\JDK>\bin;%PATH%

Procedure
---------

To perform a headless server installation under linux, you need to perform the following steps |_|:

* Create the installation parameter file.
* Launch the installer from the command line with the installation paramameter file path as argument.

**> Installation parameter file**


The parameter file Can be generated from the attached template. Please make sure to adjust the following parameters to the real values on your system |_|:

* **jdkPath** |_|: Path to a java 8 JDK that will be used to run the server.
* **installPath** |_|: Target installation path for the |squashTF| server
* **programGroup** |_|: Title for the Squash TF server submenu.

:download:`squash-tf-execution-server-auto-install-windows.xml <../../_static/install-headless/squash-tf-execution-server-auto-install-windows.xml>`

**> Installer execution**

To actually install the |squashTF| server, please run the following command |_|::

  java -jar <path\to\the\>squash-tf-execution-server-bundle-<version>-win64-installer.jar <path\to\the\xml-installation-parameter-file>

.. note:: | You can now access to your execution server at |_|: <host_url>:8080/jenkins
          | The default login is |_|: admin |_| / |_| admin

|

--------

***********
GUI install
***********

Pre-requisites
==============

* A version of the java 8 jdk must be installed on the system.
* The bin directory from this JDK must be included in the PATH shell variable. If necessary run the following command in the shell you'll be using |_|:

  * On Linux |_|::

      export PATH=</path/to/the/JDK>/bin:$PATH

  * On Windows |_|::

      set PATH=<\path\to\the\JDK>\bin;%PATH%

* Download the desired installer version of |squashTF| **Execution Server** `here <https://squash-tf.readthedocs.io/en/latest/download.html#squash-tf-execution-server>`_

Procedure
=========

1. Launch the graphical installer by a double click on the |squashTF| **Execution Server** jar file, or launch it with the following command line |_|::

      java -jar <desired-squash-tf-version>.jar

   Then you should have the language selection screen |_|:

      .. image:: ../../_static/install-gui/step0.png
         :align: center

   * Choose the desired language for the installer then click on ``OK``

   |

#. The *Welcome* screen appears |_|:

      .. image:: ../../_static/install-gui/step1.png
         :align: center

   * Click on ``Next``

   |

#. The *JDK Path* screen appears. This screen allows you to define the path where you java jdk is installed.

      .. image:: ../../_static/install-gui/step2.png
         :align: center

   * Choose the path where your jdk is installed.
   * Then click on ``Next``

   |

#. The *Target Path* Screen appears. It allows you to define the installation directory.

      .. image:: ../../_static/install-gui/step3.png
         :align: center

   * Define the installation directory (or keep the one proposed by default)
   * Then click on ``Next``

   |

#. The *Select Installation Packages* appears. It allows you to choose which package you want to install. Currently,
   you only have the choice to install or not the sahi proxy.

      .. image:: ../../_static/install-gui/step4.png
         :align: center

   * Do your choice
   * Then click on ``Next``

   |

#. The *licenses information* screen appears. It gives you all the informations concerncing the license of the product embedded

    .. image:: ../../_static/install-gui/step5.png
       :align: center

   * Click on ``Next``

   |

#. The *Setup Shortcuts* screen appears. It allows you to define your preferences concerning the shortcuts to create.

    .. image:: ../../_static/install-gui/step6.png
       :align: center

   * Click on ``Next``

   |

#. The *installation tracking* screen appears. This screen show you the progression of the installation.

    .. image:: ../../_static/install-gui/step7.png
       :align: center

   * When the installation is finished, click on ``Next``

   |

#. The *Installation Finished* screen appears |_|:

    .. image:: ../../_static/install-gui/step8.png
       :align: center

   On this screen there is a button ``Generate an automatic installation script``. It offers you to save an xml file which
   you could use to reproduce the same installation (with the same configuration you chose in the previous screens).
   See the headless installation procedure for explanations on how do a new installation using this xml file.

   * Then click on ``Done``

.. note:: | You can now access to your execution server at : <host_url>:8080/jenkins
          | The default login is |_|: admin |_| / |_| admin

.. centered:: **Congratulations ! You have installed the Squash** |_| **TF Execution Server**


.. _execution.server.install.docker.anchor:

|

--------

******
Docker
******

| With version 2.0.0 and above, |squashTF| **Execution Server** also comes with docker images.
| Since version 2.2.0 those images are also available on dockerhub |_|: `<https://hub.docker.com/r/squashtest/squash-tf-execution-server>`_
| However, our images are still available as tarball in our repo (download at `squashtest.com <https://www.squashtest.com/telechargements>`_).

These images are fully parameterized execution servers ready to be deployed.

Below is the procedure to do so.

#. Retrieve the Docker image

   **>> Retrieve from dockerhub**

   .. code-block:: bash

      docker pull squashtest/squash-tf-execution-server:{version}

   where {version} is the downloaded server version.

   **>> Retrieve from our artifacts repository**

   * Download the Docker image of the execution server `here <https://squash-tf.readthedocs.io/en/latest/download.html#squash-tf-execution-server>`_

   * Load the image on your Docker setup with the command |_|:

     .. code-block:: bash

        docker load -i squash-tf-execution-server.docker.{version}.tar

   where {version} is the downloaded server version.

   .. image:: ../../_static/docker-server/server-docker-load.png
      :alt: Load the previously downloaded Docker image.
      :align: center

   |

   .. warning:: Do not execute the "docker import" command on the image archive as it will flatten it and result in the loss of all context data such as entry points.

#. Use the following command to check the correctness of the previous load |_|:

   .. code-block:: bash

      docker images

   On successful load the output looks like |_|:

   .. image:: ../../_static/docker-server/server-docker-load-rslt.png
      :alt: Check the correctness of the previous load on your system.
      :align: center

   |
#. Create a running container of the image using the "docker run" command

   **>> Image from dockerhub**

   .. code-block:: bash

     docker run --publish 1234:8080 --name squash-tf-execution-server squashtest/squash-tf-execution-server:{version}

   **>> Image from our artifacts repository**

   .. code-block:: bash

     docker run --publish 1234:8080 --name squash-tf-execution-server squash/squash-tf-execution-server:{version}

   .. warning:: The difference between the 2 run command is in the image name. The dockerhub one is **squashtest**/squash-tf-execution-server:{version}, the other is **squash**/squash-tf-execution-server:{version}

   * The ``--publish`` option binds a port of the physical machine to the one of the soon to be created container. Here it binds the 1234
     port of the physical machine to the 8080 port of the container. This is **mandatory**\  since the Jenkins installed on the image
     will listen to the 8080 of the container. If a Master/Slave architecture is planned you can open as many tcp port of the container
     as you wish by repeating the --publish option.
   * The ``--name`` option is optional, but quite handy since it allows one to give a non arbitratry name to the container about to be
     be created.

   .. image:: ../../_static/docker-server/server-docker-run.png
      :alt: Create and run the docker container.
      :align: center

.. note:: | You can now access to your execution server at |_|: <host_url>:<host_bound_port>/jenkins
          | The default login is |_|: admin |_| / |_| admin

.. centered::  Congratulations ! You now have a Squash |_| TF Execution Server running inside a Docker container and ready to execute its first tests.

|

----

Once the container is properly created and running, use the docker stop command to shut it down |_|:

.. code-block:: bash

   docker stop squash-tf-execution-server

----

Use the docker start command to restart your server properly and retain your previous configuration
and non-volatile data |_|:

.. code-block:: bash

   docker start squash-tf-execution-server

|

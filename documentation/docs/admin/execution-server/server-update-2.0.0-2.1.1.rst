..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.



######################################
Specific migration from 2.0.0 to 2.1.1
######################################

.. contents::
   :local:

|

.. _execution.server.update.211.before.anchor:

=====================
Before migrating jobs
=====================

**********************
Updating Jenkins users
**********************


Due to security updates on Jenkins LTS 2.107 and 2.138 your users will have to be updated prior to starting the 2.1.1 version of |squashTF| **Execution Server**.

    1. Delete the **users** directory from **{new_server_path}/execution_home/**

    2. Copy the **users** directory from **{current_server_path}/execution_home/** to **{new_server_path}/execution_home/** (keep a backup copy somewhere)

    3. Check that there is NO **users.xml** file at the root of the new **users** directory. It will automatically be created later.

The users' updating process should automatically be done by Jenkins at the first start of the server, later in the generic process.
You can comme back at this time to ensure the users' update was successful :

    * All the users are visible on the people page on Jenkins.
    * A **users.xml** file should appear in the **users** directory of your new |squashTF| **Execution Server** with the list of all your users.
    * Each directory corresponding to a user should have a number following the user's name.

.. figure:: ../../_static/execution-server-update-version-2.0.0-to-2.1.1/server_update_2-0-0_to_2-1-1_users_update.png
    :align: center
    :target: ../../_static/execution-server-update-version-2.0.0-to-2.1.1/server_update_2-0-0_to_2-1-1_users_update.png

|

For more details about this process visit the `Jenkins upgrade guide <https://jenkins.io/doc/upgrade-guide/>`_ 
for versions `2.107 <https://jenkins.io/doc/upgrade-guide/2.107/>`_ and `2.138 <https://jenkins.io/doc/upgrade-guide/2.138/>`_

.. warning::
    This auto update is entirely managed by Jenkins. If you encounter any problem with your users (missing entries, bad API token or credentials...) we recommend retrying
    the process by stopping the server and doing steps 1 to 3 of this guide again.

.. note::
    You could also recreate all the users from scratch. To do so, keep the **users** directory provided in a new install of the |squashTF| **Execution Server 2.1.1** instead of deleting it at step 2.
    Start the server to connect to Jenkins with the default administrator account (login : admin // password : admin) and recreate all your users from there.


Go back to the generic update procedure for now.

|

--------

====================
After migrating jobs
====================

|

*******************
Verify users update
*******************

**Prerequisites** |_|: You have followed the earlier instructions (in the **Before migrating jobs** section) to update Jenkins users.

Start your |squashTF| **Execution Server 2.1.1** and connect to Jenkins as administrator.


Go to ``Manage Jenkins`` then to ``Manage Users`` and check that all your users appear in the list |_|:

.. figure:: ../../_static/execution-server-update-version-2.0.0-to-2.1.1/server_update_2-0-0_to_2-1-1_manage_users.png
    :align: center
    :target: ../../_static/execution-server-update-version-2.0.0-to-2.1.1/server_update_2-0-0_to_2-1-1_manage_users.png

If some of your users are missing you should stop the server, delete the **users** directory in **execution_home** (v 2.1.1) and copy again the old **user** directory (v 2.0.0) in its place
(See :ref:`**Before migrating jobs** <execution.server.update.211.before.anchor>`).

Go back to ``Manage Jenkins`` then to ``Configure Global Security`` |_|:

.. figure:: ../../_static/execution-server-update-version-2.0.0-to-2.1.1/server_update_2-0-0_to_2-1-1_config_security.png
    :align: center
    :target: ../../_static/execution-server-update-version-2.0.0-to-2.1.1/server_update_2-0-0_to_2-1-1_config_security.png

Check the authorization strategy used and verify the rights of each user in case of a matrix-based strategy.

If there is an error at this point you should retrieve a backup version of the 2.0.0 version of **execution_home** ( {old_server_path}/execution_home ) 
and copy the **config.xml** file to {new_server_path}/execution_home/.

(This config file is where the Jenkins authorization strategy is recorded).

|

*********************************************************************
Use an environment variable to define the maven local repository path
*********************************************************************

Overview
========

Previously, the local maven repository path was hardcoded by the installer during the installation process.

Starting from version 2.1.1 this path is define by using an environment variable. This change is not take into account by the generic migration and you have to do it yourself after.

Update the Maven Global Settings
================================

**Prerequisites** |_|: The generic migration should have been done.

Start your |squashTF| **Execution Server 2.1.1** and connect to Jenkins as administrator.

Go to ``Manage Jenkins`` then to ``Config File Management`` |_|:

.. figure:: ../../_static/execution-server-update-version-2.0.0-to-2.1.1/server_update_2-0-0_to_2-1-1_config_files_manage_01.png
    :align: center
    :target: ../../_static/execution-server-update-version-2.0.0-to-2.1.1/server_update_2-0-0_to_2-1-1_config_files_manage_01.png

Open the file *TF_MavenGlobalSettings* and find the property <localRepository>.

Replace the line by |_|::

    <localRepository>${env.SQUASH_TA_HOME}/repository</localRepository>

and submit the changes.

.. figure:: ../../_static/execution-server-update-version-2.0.0-to-2.1.1/server_update_2-0-0_to_2-1-1_config_files_manage_02.png
    :align: center
    :target: ../../_static/execution-server-update-version-2.0.0-to-2.1.1/server_update_2-0-0_to_2-1-1_config_files_manage_02.png

Repeat this procedure for the file *TF_Runners_MavenGlobalSettings.xml* and submit again.

.. figure:: ../../_static/execution-server-update-version-2.0.0-to-2.1.1/server_update_2-0-0_to_2-1-1_config_files_manage_03.png
    :align: center
    :target: ../../_static/execution-server-update-version-2.0.0-to-2.1.1/server_update_2-0-0_to_2-1-1_config_files_manage_03.png

|

----------

.. _update_publish_url:

********************************
New reports publication settings
********************************

Overview
========

The jobs template settings for the reports publication have changed in |squashTF| **Execution Server 2.1.1**. This change is due to a problem in the report publication url in Jenkins which was breaking the **TM-TF** link

This change have been made to all the templates.

**Already existing jobs will have to be updated manually.**

.. note::
   These new reports publication settings are mandatory in case of a **TF-TM** link configuration and highly recommended in other cases.


Update existing jobs
====================

In your job, go to ``Post-build Actions`` |_|:

    * Replace all the spaces in the ``Report title`` fields for each report generated by the job.
    * Click on ``Publishing options`` under each report form.
    * The checkbox ``Escape underscores in Report Title`` now needs to be unchecked.


.. figure:: ../../_static/execution-server-update-version-2.0.0-to-2.1.1/server_update_2-0-0_to_2-1-1_checkbox_and_reports_title_BEFORE.png
    :align: center
    :target: ../../_static/execution-server-update-version-2.0.0-to-2.1.1/server_update_2-0-0_to_2-1-1_checkbox_and_reports_title_BEFORE.png

    Report forms before corrections

----------

.. figure:: ../../_static/execution-server-update-version-2.0.0-to-2.1.1/server_update_2-0-0_to_2-1-1_checkbox_and_reports_title_AFTER.png
    :align: center
    :target: ../../_static/execution-server-update-version-2.0.0-to-2.1.1/server_update_2-0-0_to_2-1-1_checkbox_and_reports_title_AFTER.png

    Report forms after corrections

|

--------

**********
Final step
**********

When all the procedures have been done you should restart your **Execution Server**,
then reconnect to Jenkins and check that all the corrections have been successfully implemented.

|

..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

##################################
Execution Server - Troubleshooting
##################################

|

********************************************
Insufficient Cache warnings in catalina logs
********************************************

During the launch of the execution server, you might encounter a warning message stating that a resource could not be added because there was insufficient free space available in your cache memory (see the full message in the example below) |_|:

.. code-block:: shell

    08-Jul-2019 16:04:23.575 AVERTISSEMENT [Loading plugin Environment Injector Plugin v2.1.6 (envinject)] org.apache.catalina.webresources.Cache.getResource Unable to add the resource at [/WEB-INF/classes/org/jenkinsci/plugins/envinject/Messages_en.properties] to the cache for web application [/jenkins] because there was insufficient free space available after evicting expired cache entries - consider increasing the maximum size of the cache

In order to fix this problem, you have to increase the cache max size. To do so, go in the Apache Tomcat Folder of your execution server and then go to the 'conf' folder.

Open context.xml and in the context block, add the following property |_|:

.. code-block:: xml

    <Resources cachingAllowed="true" cacheMaxSize="100000"/>

.. image:: ../../_static/execution-server-troubleshooting/cache-memory-increase.png
      :alt: increase Apache Tomcat Cache memory. 
      :align: center

|

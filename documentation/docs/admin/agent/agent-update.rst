..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

.. _execution.agent.update.anchor:


########################
Execution Agent - Update
########################

.. contents::
   :local:

|

========
Overview
========

Using a master-agent architecture to run your jobs in Jenkins can be done in a few combinations |_|:
depending on the platform on which your Execution Server runs (Windows, Linux, docker container) and on which the Execution Agents you wish to update will be,
the procedure can be slightly different.

As such this guide will have a two parts structure |_|:

    - Tranferring nodes directory
    - Jenkins configuration update

Each step in these parts will specify if it concerns the server or the agent, and each type of install (Windows, Linux, docker container) will be explained if there are differences.

|

--------

=============
Prerequisites
=============

A |squashTF| **Execution Server** up to date with the version you will update your agent to.

|

--------

============================
Transferring nodes directory
============================

    .. tip::
        
        If you followed the Execution Server update process (physical or docker) you may have already transferred the **nodes** directory from the old Execution Server to the new one.

        In that case you can skip ahead to the 4th step.

1. Stop your current |squashTF| **Execution Server** and the **Jenkins Agent** you wish to update |_|:

    * Using your method of choice in a physical install of the Execution Server (Linux or Windows).
    * Using the **docker stop** command in case your Execution Server is running in a docker container.

----------

2a. On a physical install of the Execution Server simply copy the **nodes** directory from **{current_server_path}/execution_home/** to **{new_server_path}/execution_home/**

.. image:: ../../_static/agent-update/agent-update-copy-nodes.png
    :align: center
    :target: ../../_static/agent-update/agent-update-copy-nodes.png

-----------

2b. On a docker install of the Execution Server use the **docker cp** command to copy the **nodes** directory from your old container to the host system, then from the host system to the new container.


    .. code-block:: bash

        docker cp {old-container}:/opt/squash-ta-server/execution_home/nodes/. {path_of_choice}/nodes-backup
        docker cp {path_of_choice}/nodes-backup/. {new-container}:/opt/squash-ta-server/execution_home/nodes

----------

3. Restart your new Execution Server |_| :

    * Using your method of choice in a physical install of the Execution Server (Linux or Windows).
    * Using the **docker start** command in case your Execution Server is running in a docker container.

----------

4. Choose and download the version of the |squashTF| **Execution Agent** you wish to install `on this page <https://squash-tf.readthedocs.io/en/latest/download.html#squash-tf-execution-server>`_.

----------

5a. If you downloaded a physical installer of the agent |_|:

        Launch the installer of your new |squashTF| **Execution Agent** and follow the :ref:`setup procedure <execution.agent.install.anchor>`.

----------

5b. If you chose to use our docker image |_|:

        * With the new **Execution Server** running connect to Jenkins as an administrator and click on the name of your old agent on the left of the screen.
        * Recover the **secret** of the node and keep it for later.

.. image:: ../../_static/agent-update/agent-update-jenkins-secret-01.png
    :align: center
    :target: ../../_static/agent-update/agent-update-jenkins-secret-01.png

.. image:: ../../_static/agent-update/agent-update-jenkins-secret-02.png
    :align: center
    :target: ../../_static/agent-update/agent-update-jenkins-secret-02.png

|

--------

===================
Jenkins node update
===================

For this section the new Execution Server should be running, either on your machine (Windows or Linux) or in a docker container.

You should also be connected to the corresponding Jenkins as an administrator.

The rest of the procedure will distinguish between a *physically installed* Agent and a an Agent in a *docker container*. The type of install of the **Execution server** is no longer important in this part.

******************
Physical installer
******************

1. Go to ``Manage Jenkins`` then to ``Manage Nodes``.

.. image:: ../../_static/agent-update/agent-update-jenkins-manage-nodes.png
    :align: center
    :target: ../../_static/agent-update/agent-update-jenkins-manage-nodes.png

-----------

2. Click on the **cogwheel** icon on the right of the agent of the agent your are updating to access its configuration screen.

.. image:: ../../_static/agent-update/agent-update-jenkins-configure-agent-01.png
    :align: center
    :target: ../../_static/agent-update/agent-update-jenkins-configure-agent-01.png

-----------

3. In the field ``Remote root directory`` replace the path of your old version with the new one (everything before **/workspace**).

.. image:: ../../_static/agent-update/agent-update-jenkins-configure-agent-02.png
    :align: center
    :target: ../../_static/agent-update/agent-update-jenkins-configure-agent-02.png

-----------

4. On the same screen find the section ``Node Properties`` and the sub-section ``List of tool locations``.

-----------

5. In the field ``Home`` under ``(Maven) maven_ta`` replace the path of your old version with the new one (everything before **/apache-maven-3.5.0**).

.. image:: ../../_static/agent-update/agent-update-jenkins-configure-agent-03.png
    :align: center
    :target: ../../_static/agent-update/agent-update-jenkins-configure-agent-03.png

-----------

6. Check that all your custom configurations are correct then click on **Save**.

-----------

7. Restart your |squashTF| **Execution Server** and launch the agent, then run your jobs to make sure the update was sucessfull.

----------

************
Docker image
************

1. Follow step 9 and step 10 from :ref:`these <execution.agent.install.docker.anchor>` setup instructions to link the agent container to the server container (you can read steps 1 to 8 for context).

-----------

2. Run your jobs on the updated Jenkins agent to make sure the update was successfull.

|

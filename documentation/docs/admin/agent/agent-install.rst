..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

.. _execution.agent.install.anchor:

############################
Execution Agent installation
############################

.. contents::
   :local:

|

|squashTF| **Execution Server** is Jenkins based. As such one can setup a master/slave
architecture with clear separation of concerns. The Jenkins master schedules
test related tasks whereas slave agents are solely responsible for their
executions.

As the setting up of such architectures may be somewhat cumbersome, graphical
agent installers are supplied as well as ready to run docker images.

.. warning:: In **2.3.0-RELEASE version of the server**, we updated Jenkins to 2.190.1 version. Since 2.165 and newer version, Jenkins no longer supports the old (-remoting) mode in either the client or server. If you have installed a 2.3.0-RELEASE of the server and want to install an agent, you need to download **2.3.0-RELEASE (or newer) version of the agent**.

|

****************
Physical install
****************

The graphical agent installers are meant for GNU-Linux-64 or Windows-64
environments. They will install and configure self-contained Jenkins Jnlp agents
with |squashTF| runners handling capabilities. More precisely they will |_|:

1. Declare on the Jenkins master a new Jnlp node.
2. Retrieve the name and the secret of the created node.
3. Install the self contained agent and create a launch script.
4. Configure, on the Jenkins master, the newly created node properties such as
   the location of the embedded Maven.

Pre-requisites
--------------

The following pre-requisites are needed in order to perform the installation |_|:

1. An open port on the Jenkins Master (|squashTF| **Execution Server**) that can be used
   by the agent to connect to the master.
2. A Java Development kit 1.8 since, for licensing reasons, it is not possible to
   ship a our product with an embedded JDK.
3. A Java Virtual Machine with the corresponding environment variables
   JAVA_HOME, JRE_HOME and PATH set to launch the installer.
4. A Code source management client for the agent to retrieve tests sources
   code.
5. If GUI tests are in order, provide a way to keep active the graphical session.
   This may be a technical user account with the correct rights on Windows machines,
   or a X11 virtual frame buffer on GNU-Linux machines.


Procedure
---------

Both Windows and GNU-Linux installers work the same. We display here below the
common procedure |_|:

1. The installers come as executable jar. So either double-click on the the jar or
   run the usual command |_|:

   .. code-block:: shell

      java -jar squash-tf-execution-agent-{version}-{os}.jar

   where {version} is the actual version of your execution server and {os} the
   operating system the agent will run on. One is then greeted by the
   following screen. Select the desired language and click on the OK button.

   .. container:: image-container
 
    .. image:: ../../_static/agent-install-gui/agent-select-lang.png
       :alt: Installer greeter and installer language selection.
       :align: center

2. Fill in the installation directory and click on the next button.

   .. container:: image-container

    .. image:: ../../_static/agent-install-gui/agent-install-dir.png
       :alt: Select installation directory.
       :align: center

3. Go through various non-ambiguous steps until you're prompted to fill in
   the URL of your execution server.

   **WARNING**\  : Before going any further the Jenkins master should be configured so
   it allows "anonymous read access". Indeed at this step the installer will try
   to interact with the Jenkins API. If anonymous reading is not allowed, the
   installer will consider that the given URL is false. To configure it |_|:

   a. Log in administrator mode into Jenkins.
   b. Navigate to "Administer Jenkins/Configure global security".
   c. In the "Autorisations" section, allow anonymous access reads.

   .. container:: image-container

    .. image:: ../../_static/agent-install-gui/agent-master-allow-anonymous-read.png
       :alt: Allow anonymous read acces on the master Jenkins server.
       :align: center

   Once this is done, come back to the installer screen and fill in the
   Jenkins URL.

   .. container:: image-container

    .. image:: ../../_static/agent-install-gui/agent-master-url.png
       :alt: Filling up of Jenkins master URL.
       :align: center

4. Fill in Jenkins administrator credentials, and verify the correcteness of the
   pair URL/credential by clicking the "Test" button.

   .. container:: image-container

    .. image:: ../../_static/agent-install-gui/agent-master-credentials.png
       :alt: Test of Jenkins master URL/credentials pair.
       :align: center

5. Next the installer will perform the actual remote declaration of the node on
   the Jenkins master. Hence, the latter **must**\  be configured to accept
   remote control |_|:

   a. Navigate to "Administer Jenkins/Configure global security"
   b. Autorise (**temporarily**\ ) remote access to Jenkins CLI.

      .. container:: image-container

        .. image:: ../../_static/agent-install-gui/agent-master-cli.png
           :alt: Remote access to Jenkins CLI.
           :align: center

   c. Fill in the port onto which Jnlp agents can connect.

      .. container:: image-container

        .. image:: ../../_static/agent-install-gui/agent-master-tcp-port.png
           :alt: Definition of opened jenkins master server opened tcp port.
           :align: center

   d. Click the "save" button. An alert message should then appear stating that
      allowing remote access to the CLI is not safe. That's normal. Just think
      to disable it after completion of this procedure.

      .. container:: image-container

        .. image:: ../../_static/agent-install-gui/agent-master-cli-warn-message.png
           :alt: Remote CLI access is not safe.
           :align: center

    Once this is done,  come back to the installer screen and click "next". The
    following screen states that everything went well.

      .. container:: image-container

        .. image:: ../../_static/agent-install-gui/agent-installation-finished.png
           :alt: Installation and configuration of Jenkins Jnlp node went fine.
           :align: center

6. Go through the following steps until installation is finished.
7. The newly created agent node is now visible in the left Jenkins menu.
   It appears as disconnected. This is perfectly normal. Indeed the slave has not
   been launched yet.

   .. container:: image-container

    .. image:: ../../_static/agent-install-gui/agent-master-declared-node.png
       :alt: The created node is declared on the Jenkins master server.
       :align: center

8. Launch the agent by executing either {install-dir}\\scripts\\lauch.cmd on
   Windows or {install-dir}/scripts/launch.sh on GNU-Linux, where {install-dir}
   is the directory one chose to install the agent. "INFOS: Connected" is then
   displayed on the console signaling that the agent is launched properly.

   .. container:: image-container

    .. image:: ../../_static/agent-install-gui/agent-connected.png
       :alt: The created node is now connected.
       :align: center

   On the Jenkins master the created node appears now as at "rest".

   .. container:: image-container

    .. image:: ../../_static/agent-install-gui/agent-at-rest.png
       :alt: The created node is now connected and ready to execute tasks.
       :align: center

The procedure is reproducible for each agent node one wants to create. For security
purposes, please ensure , once this is complete, to finally disable remote access
to Jenkins CLI.

|

--------

.. _execution.agent.install.docker.anchor:

*************
Docker setup
*************

| With version 2.1.0 and above, various Docker images of |squashTF| **Execution Agents** are available.
| Since the 2.2.0 version, our docker images are available from dockerhub : `<https://hub.docker.com/u/squashtest>`_
| However, our images are still available as tarball in our repo (download at `squashtest.com <https://www.squashtest.com/telechargements>`_).


Those come in three "flavors" |_|:

* squash-tf-execution-agent |_|: The base image.

    It brings the tools needed to be ran as a Jnlp agent and run headless |squashTF| test.
    The image is built on the the open-jdk image and is thus Debian based.
    The image includes a JDK, a Maven, a Git client, a Mercurial client. And last
    but not least it contains the jars and configured entry-point to run it as a
    Jnlp agent.

* squash-tf-chrome-ui-execution-agent |_|: Chrome-GUI testing specialized image.

    This specialization provides additionally a working graphical session
    and comes equipped with a guaranteed compatible Chrome/Chromedriver couple.
    The graphical stack is built using Xvfb as a fully  in-memory X-11 server.
    Hence no graphical components should be needed on the underlying machine
    servicing the Docker host and running the images.

* squash-tf-firefox-ui-execution-agent |_|: Firefox-GUI testing specialized image.

    Same as the chrome-ui image but comes with a guaranteed compatible
    Firefox/Geckodriver couple instead.

Credits |_|: The docker images with in-memory X-server implementation is heavily based
on work done by `Stephen Fox <https://github.com/stephen-fox/chrome-docker>`_.

Pre-requisites
--------------

The following pre-requisites are needed in order to perform the installation |_|:

* A Jnlp open port on the Jenkins Master (Squash TF Execution Server) that can be used by the agent to connect to the master.
* A Docker compatible operating system on the machine(s) where the images will be running.
* A valid Docker installation with a Docker host.

Procedure
---------

The setup procedure is flavor independent [#]_. For the sake of example,
screenshots displayed below are those from a |squashTF| **Chrome-ui agent** setup.

1. The first step is to declare a new node in Jenkins administration section. Go to
   "Manage Jenkins/Manage Nodes" and click on "New Node" on the left panel.

   .. container:: image-container

    .. image:: ../../_static/docker-setup/new-node-button.png
       :alt: Declare a new Jenkins node.
       :align: center

---------

2. Fill in wanted agent name and select Permanent agent radio button.

   .. container:: image-container

    .. image:: ../../_static/docker-setup/node-name.png
       :alt: Name of the will be declared agent node.
       :align: center

---------

3. The images are built to use /home/jenkins/agent as the working directory.
   Thus fill in distant working dir section and Remote root directory with
   "/home/jenkins/agent".

---------

4. Add as many labels as you wish and a description if wanted. As the Jenkins
   documentation states |_|: *Labels (or tags) are used to group multiple agents into
   one logical group (..) Labels do not necessarily have to represent the operating
   system on the agent; you can also use labels to note the CPU architecture,
   or that a certain tool is installed on the agent*.

   .. container:: image-container

    .. image:: ../../_static/docker-setup/node-working-dir-and-labels.png
       :alt: Remote root and working directory.
       :align: center

---------

5. The agent may not share with the execution server vital -- for |squashTF| tests
   executions -- tools locations. The aforementioned locations can be overridden
   using the dedicated section and the following values |_|:

   a. JDK home : /docker-java-home
   b. Maven home: /usr/share/maven

   .. container:: image-container

    .. image:: ../../_static/docker-setup/tools-location.png
       :alt: Overriding tools location.
       :align: center

   To further clarify tools locations Mercurial and Git are to be found at |_|:

   c. Mercurial : /usr/bin/hg
   d. Git : /usr/bin/git

---------

6. The agent may not share with the execution server vital -- for |squashTF| tests
   executions -- environment variables. Yet, the aforementioned variables are
   already set in the dockerfile. Hence, there is no need to redefine them in the
   concerned section. Nonetheless, for information purposes, here are the values
   of a few of these variables |_|:

         a. JENKINS_HOME : /home/jenkins/agent
         b. SQUASH_TA_HOME : /home/jenkins/agents
         c. MAVEN_HOME : /usr/share/maven
         d. MAVEN_CONFIG : /home/jenkins/.m2
         e. JAVA_HOME : /docker-java-home

---------

7. Click on the "save" button and note the secret of your newly declared node.
   The newly declared node should now appear as offline in the left panel.

   .. container:: image-container

    .. image:: ../../_static/docker-setup/offline.png
       :alt: Declared node is offline.
       :align: center

   Click on the newly created node to retrieve its "secret" phrase. It will prove
   necessary when launching the agent.

   .. container:: image-container

    .. image:: ../../_static/docker-setup/node-secret.png
       :alt: Node secret.
       :align: center

---------

8. You've finished the node declaration. Now you now have to download the agent docker image. Two possibilities |_|:

   * using a docker images from dockerhub.
   * using a docker images package as tarball from our artifacts repository.

   |
   | **>> Download the agent image from dockerhub**

   To donwload the desired image |_|:

   .. code-block:: shell

      docker pull squashtest/squash-tf-{flavor}-execution-agent.docker.{version}

   where {flavor} corresponds to the type of agent you're interested in and {version} is the version of the agent. We recommend you to choose the **version** corresponding
   to your |squashTF| **execution server** for full compatibility. For the sake of information, the Jnlp agent jars used to build the base image are the one
   provided by Jenkins itself.

   |
   | **>> Download the agent image from our artifacts repository**

     a. Download the desired image `here <https://www.squashtest.com/telechargements>`_. Alternatively one can access the image directly from our repository using |_|:

        .. code-block:: shell

           wget http://repo.squashtest.org/distribution/squash-tf-{flavor}-execution-agent.docker.{version}.tar

        where {flavor} corresponds to the type of agent you're interested in and {version}
        is the version of the agent. We recommend you to choose the **version** corresponding
        to your |squashTF| **execution server** for full compatibility. For the sake of
        information, the Jnlp agent jars used to build the base image are the one
        provided by Jenkins itself.

     b. Load the downloaded docker image in your set up using the following command |_|:

        .. code-block:: shell

           docker load -i squash-tf-{flavor}-execution-agent.docker.{version}.tar

        where {flavor} corresponds to the type of agent you're interested in and {version}
        is the version of the agent.

        It is to be noted that one should "**NOT**" use the docker import command as it
        will flatten all layers and will render unusable the image.

   The image should now be visible in your available docker images. This can be
   checked using |_|:

   .. code-block:: shell

      docker images

   .. container:: image-container

    .. image:: ../../_static/docker-setup/docker-load-rslt.png
       :alt: Docker load result.
       :align: center

---------

9. Finally run the docker image using |_|:

   **>> For the image from dockerhub**

   .. code-block:: shell

     docker run --name demo-tf-agent --user jenkins --env "JENKINS_AGENT_NAME={agent_name}" --env "JENKINS_SECRET={secret}" squashtest/squash-tf-{flavor}-execution-agent:{version} -url http://{jenkins_url}

   The command should be understood as following |_|:

   a. docker run : Runs a docker container.
   b. --name demo-tf-agent : The nickname of the container that will be created.
      Can be chosen to one's liking. If not set a random name will be assigned
      to the container.
   c. --user jenkins : The technical user with proper rights to run Jenkins.
   d. --env "JENKINS_AGENT_NAME={agent_name}" : Enables one to specify the
      name of the declared node in Jenkins. Modify {agent_name} accordingly.
   e. --env "JENKINS_SECRET={secret}" : Enables one to specify the
      secret of the declared node in Jenkins. Modify {secret} accordingly.
   f. squashtest/squash-tf-{flavor}-execution-agent:{version} : Selects the image
      one wants to run, where {flavor} is the type of image one is interested in
      and {version} the version targeted.
   g. -url http://{jenkins_url} : Enables one to specify the URL of the Jenkins
      master server.

   **>> For the image from our artifact repository**

   .. code-block:: shell

     docker run --name demo-tf-agent --user jenkins --env "JENKINS_AGENT_NAME={agent_name}" --env "JENKINS_SECRET={secret}" squash/squash-tf-{flavor}-execution-agent:{version} -url http://{jenkins_url}

   The command should be understood as following |_|:

   a. docker run : Runs a docker container.
   b. --name demo-tf-agent : The nickname of the container that will be created.
      Can be chosen to one's liking. If not set a random name will be assigned
      to the container.
   c. --user jenkins : The technical user with proper rights to run Jenkins.
   d. --env "JENKINS_AGENT_NAME={agent_name}" : Enables one to specify the
      name of the declared node in Jenkins. Modify {agent_name} accordingly.
   e. --env "JENKINS_SECRET={secret}" : Enables one to specify the
      secret of the declared node in Jenkins. Modify {secret} accordingly.
   f. squash/squash-tf-{flavor}-execution-agent:{version} : Selects the image
      one wants to run, where {flavor} is the type of image one is interested in
      and {version} the version targeted.
   g. -url http://{jenkins_url} : Enables one to specify the URL of the Jenkins
      master server.

   .. warning:: The difference between the 2 run command is in the image name. The dockerhub one is **squashtest**/squash-tf-{flavor}-execution-agent:{version}, the other is **squash**/squash-tf-{flavor}-execution-agent:{version}


   Once done, the node should appear as online on the Jenkins interface and its
   build queue should be in "idle".

   .. container:: image-container

    .. image:: ../../_static/docker-setup/online.png
       :alt: The agent is now online.
       :align: center

   The agent is now ready to execute its first |squashTF| tests.

.. _docker_graphical:

Particularity of graphical session providing docker images
----------------------------------------------------------

The docker images with X11 have some particularities |_|:


1. The latter images can be ran in "debug mode" using the --x11 vnc-debug option.
   In such a mode a VNC (Virtual Network Computing) server is also launched
   allowing remote access to the graphical session of the running container. If
   launched the server listen on port 5900. One should thus bind the container
   5900 port to a physical port of the machine hosting Docker. The full
   command is then |_|:

   .. code-block:: shell

     docker run -p 5900:5900 --name demo-tf-agent --user jenkins --env "JENKINS_AGENT_NAME={agent_name}" --env "JENKINS_SECRET={secret}" squash/squash-tf-{flavor}-execution-agent:{version} -url http://{jenkins_url} --x11-vnc-debug

   Once the container launched in debug mode, its graphical session can be accessed
   using a VNC client targeting the Docker hosting machine on the binded port.
   For example using vncviewer |_|:

   .. container:: image-container

    .. image:: ../../_static/docker-setup/vnc-viewer.png
       :alt: Connection to the VNC server through binded port of the Docker hosting machine.
       :align: center

   .. container:: image-container

    .. image:: ../../_static/docker-setup/remote-graphical-session.png
       :alt: remote graphical session of the running container.
       :align: center

--------

2. Due to Chrome own limitations the user running it shoud be "Privileged". This
   can be set using the --privileged options in the docker command. The command
   to run the Chrome flavor of the agent is then |_|:

   .. code-block:: shell

     docker run --name demo-tf-agent --user jenkins --privileged --env "JENKINS_AGENT_NAME={agent_name}" --env "JENKINS_SECRET={secret}" squash/squash-tf-chrome-ui-execution-agent:{version} -url http://{jenkins_url}


Both of these options can be run simultaneously.

Troubleshooting
---------------

Here is a non exhaustive list of possible slight configuration/integration issues.

1. Problems linked to tools |_|:
   Java, Maven, git, mercurial is not found, problems while checking out project, etc...
   Jenkins master tools configuration may be interfering with the agent one.
   Try overriding their location using the "tools location" section in the node
   configuration page on Jenkins.

   .. container:: image-container

    .. image:: ../../_static/docker-setup/tools-location-troobleshooting.png
       :alt: Overriding agent tools location.
       :align: center

--------

2. Something is wrong with the environment, cannot find any parasable pom, working
   directory is weirdly set. Jenkins master environment variables configuration
   may be interfering with the agent one. Try overriding their values using the
   "environment variables" section in the node configuration page on Jenkins.

   .. container:: image-container

    .. image:: ../../_static/docker-setup/env-variables-troobleshooting.png
       :alt: Overriding agent environment variables.
       :align: center

--------

3. Jenkins is not reachable since it is behind a firewall. Use the Jenkins anticipated
   mechanism and specify the environment variable --env "JENKINS_TUNNEL : HOST:PORT"
   for a tunnel to route TCP traffic to Jenkins host, when jenkins can't
   be directly accessed over network

--------

4. Our own tests revealed that too many layers of virtualization may render the
   use of the images unstable. For instance we found that on our setup, trying to run
   the images on the "virtualization sandwich" Windows 10/Virtualbox/Debian 9/Docker
   results in an immediate crash of the container without any further ado.

--------

5. Time problems in reports. By default our containers are in UTC. This could lead to unexpected
   time value in the produced reports. You could solve this problem by specifying a timezone in
   your docker run command line. Example |_|:

   .. code-block:: none

      --env "TZ=Europe/Paris"

--------

6. When you execute a test with a job using the Chrome agent which fails when it tries to open Chrome,
   and you have an error stack which looks like |_|:

   .. code-block:: none

      org.squashtest.ta.framework.exception.InstructionRuntimeException: Junit test execution for [engine:junit-jupiter]/[class:XXX]/[method:YYY] failed on error : unknown error: Chrome failed to start: crashed
      (unknown error: DevToolsActivePort file doesn't exist)
      (The process started from chrome location /usr/bin/google-chrome is no longer running, so ChromeDriver is assuming that Chrome has crashed.)

  Then have you run your container in privileged mode as explain in the doc |_|? See |_|: :ref:`docker_graphical`

--------

.. [#] No evident link has been found with Quantum Chromodynamics. Yet, we let the demonstration of the invariance under SU(3) transformations as an exercise to the reader.

|

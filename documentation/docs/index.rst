..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

##########################
Squash TF Execution Server
##########################

.. toctree::
   :hidden:
   :maxdepth: 2

   < Squash TF Doc Homepage > <https://squash-tf.readthedocs.io>
   Administration <admin/admin.rst>
   Usage <usage/usage.rst>
   Use Squash TM with Squash TF <tm-tf-link/tm-tf.rst>
   Download <https://squash-tf.readthedocs.io/en/latest/download.html#squash-tf-execution-server>


********
Overview
********

Squash TF Execution Server is our server to execute test on distributed environments

It use the master - agents architecture :

* the master: It's the scheduler which handle the jobs to execute.
* the agents: The handle execution on multiple environments or technologies:

  * environments: development, acceptance, integration, ...
  * technologies:

    * OS : windows /linux
    * Browser : chrome/firefox/...

--------------------

Your jobs are on the master. We provide some sample jobs you can duplicate to create your project job according to the type of execution you want to do.
See usage part of the documentation for more information on job creation. In this job we define :

* Your job parameters
* The way to retrieve your automated tests project sources
* The command line to execute your test
* The post action to publish the reports

--------------------

Squash TF is based on jenkins which bring us all its ecosystem capacity:

* scm connectors (git, mercurial, svn, ...) to retrieve tests project sources
* distributed execution through master agent architecture
* pipeline
* report publishing
* API Rest for job remote launch

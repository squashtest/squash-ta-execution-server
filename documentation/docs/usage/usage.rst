..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.



##############################
Use Squash TF Execution Server
##############################

.. toctree::
   :hidden:

   Create a job <create-job.rst>
   Java Junit runner job usage <run-junit5.rst>
   Cucumber Java runner job usage <run-cucumber.rst>
   Robot Framework Job use <run-robot.rst>
   SKF / TA job usage <run-ta.rst>

:ref:`Create a job <create_job>`

Use a job based on:

* :ref:`Java Junit runner job template<junit5_job_use>`

* :ref:`Cucumber Java runner job template<cucumber_job_use>`

* :ref:`Robot Framework job template<robotframework_job_use>`

* :ref:`SKF / TA job template<ta_job_use>`
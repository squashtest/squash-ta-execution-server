..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

.. _cucumber_job_use:

###############################################
Use a job based on Cucumber runner job template
###############################################

.. contents::
   :local:

|

*****************
Selecting the job
*****************

Once you've :ref:`created a job using the Squash-TA Template <create_job>`, select it in the list of jobs available |_|:

    .. container:: image-container

        .. image:: ../_static/run-cucumber/cucumber-job-template-select-job.png

On the job's page, you'll be able to launch a build, access various informations regarding the job or make modifications.

.. Note::
   You'll have to be logged in as an administrator to modify your job (rename, delete or configure) or to launch a build.

To launch a build, click on *'Build with Parameters'* |_|:

    .. container:: image-container

        .. image:: ../_static/run-cucumber/cucumber-job-template-build-with-parameters.png

|

***********************************
Setting the parameters of the build
***********************************

You can then configure some of the parameters of the build |_|:

    .. container:: image-container

        .. image:: ../_static/run-cucumber/cucumber-job-template-build-job.png

-----------------------
Operation and TestList
-----------------------

You can specify two types of goals to execute in the *'operation'* field: **dryrun** or **run**. 

    **> dryrun** |_|: This will check if the tests specified in the *'testList'* field or in a user created *json* file are runnable by Cucumber 
    (meaning that they are implemented).

    **> run** |_|: This will run all the tests specified in the *'testList'* field or in a user created *json* file.

    If you are using the **TM-TF** link, |squashTM| will generate and transmit to |squashTF| a *testsuite.json* file containing the list of tests to execute.
    In that case you don't have to alter the field *'testList'* or specify a *json* file.

    Otherwise, if you want to provide manually to your **run** the list of tests to execute, you can procede in two ways |_|:

        * Enter the relative path (to the root of your project) of a single test (*.feature* type file) you wish to execute in the *'testList'* field. 
          Alternatively, you can specify the path of a folder containing several test files.
          You can also have the build execute all the tests present in you project, by leaving the *'testList'* field blank.

        * Provide a *json* type file (by clicking on the *'Choose File'* button on the *'testsuite.json'* line) containing the list of tests you wish to execute,
          and fill the *'testList'* field with |_|: **{\file:testsuite.json}**.

            **Exemple of each method** |_|:

                .. container:: image-container

                        .. image:: ../_static/run-cucumber/cucumber-job-template-test-tree.png

--------
Executor
--------

If you want to launch the build on an |squashTF| agent located on a distant machine and properly configured 
(see :ref:`this page <execution.agent.install.anchor>` for the agent installation), 
enter the exact name of the agent, or its label, in the *'executor'* field.
Click on the *'Show nodes'* button on the bottom right of the field to validate that you've entered a correct name |_|:

    .. container:: image-container

        .. image:: ../_static/run-ta/skf-job-template-choose-executor.png

|

*****************
Launching a build
*****************

Once you've specified the parameters of your build, click on the *'Build'* button to launch the build.

Clicking on the dot (grey, red or green depending on the status of the build) next to the build name in the *'Build History'* window will show the console output.

    .. container:: image-container

        .. image:: ../_static/run-cucumber/cucumber-job-template-build-list-building-steps.png

|

*************
Build outputs
*************
-----------
HTML report
-----------

After the first build (**dryrun** or **run**) has been performed, the generated *'Squash_TF_HTML_Report'* of the last build executed will be available on the job's page |_|:

    .. container:: image-container

        .. image:: ../_static/run-cucumber/cucumber-job-template-html-report-after-build.png

The content of the HTML report will differ according to the type of build (**dryrun** or **run**) that has been executed.

If you click directly on a build's name, you'll have access to its page with the corresponding HTML report |_|:

    .. container:: image-container

        .. image:: ../_static/run-cucumber/cucumber-job-template-build-select.png

|

----------

.. Note::
   For more indepth details about the Cucumber runner, please consult its `dedicated section <https://squash-tf.readthedocs.io/projects/runner-cucumber-java/en/doc-stable/>`_.

|

..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

.. _create_job:


################################################
Create a job in Squash\ |_|\ TF Execution Server
################################################

*NB : this procedure requires authentication with a user account with job creation rights*

*********************
Choosing the template
*********************

The |squashTF| **Execution Server** offers various templates, depending on the test you
want to run.

   .. image:: ../_static/create-job/jobTemplates.png

You may make your choice according to the following table :

.. csv-table:: 
   :header: "Test project","Template"

   "Junit 4 based test project (including Selenium)","``00Squash-TF-Java_JUnit_Runner_Template``"
   "Junit 5 based test project (including Selenium)","``00Squash-TF-Java_JUnit_Runner_Template``"
   "Test project managed from Squash TM's Gherkin Test cases","``00Squash-TF-Cucumber_Template``"
   "Robot Framework test project","``00Squash-TF_Robotframework_Template``"
   "Keywork Framework [#dslKwDef]_ test project","``00Squash-TA_Template``"
   "Keywork Framework [#dslKwDef]_ test project with two execution phases [#speakOf2phase]_ ","``00Squash-TA_Template-pipeline``"

****************
Creating the job
****************

Once you have chosen the appropriate template, create your job from this template through the **New Item** menu.

   .. image:: ../_static/create-job/newItem.png

#. Enter a name for your test job
#. Type the name of the template in the **Copy from** field at the bottom of the new item page.
   Once you've begun to type auto-completion will trigger and you'll be able to choose from existing templates, then click on
   the *OK* button to finish job creation.

   .. image:: ../_static/create-job/jobCreation.png

******************
Setting the job up
******************

After creating the job, you have to set it up.


.. _general_case_setup:

-----------------------------------------
Setting a Squash TF job up (general case)
-----------------------------------------

.. to boldly split infinitives that nobody has ever split before !
   |StarTrek (LOL edition)

#. The main required settings for a Squash TF job are the Source Code Management parameters.
   The job needs to known how to get the test project sources. The illustration below shows the
   setup of a mercurial hosted project, but any SCM will do as long as it is supported in jenkins.
   Once you've entered the SCM parameters, hit the **Save** button.

   .. image:: ../_static/create-job/scmSetup.png


#. The next - and last - step is to enable the job by clicking on the **Enable** button.

   .. image:: ../_static/create-job/need2enable.png

.. _robot_job_setup:

--------------------------------
Setting a Robot framework job up
--------------------------------

#. First you have to follow the  :ref:`general case setup describe below <general_case_setup>`

#. For a job based on the Squash TF Robot Framework job template, one more configuration step is needed.
   As Robot Framework tests needs an execution environment with python, then by default the job is configured to only run
   on an executor which have the label ``robotFW``. Out of the box Squash TF Execution Server doesn't have any executor with this label.
   So before launch a Robot framework job you need to :

   * Have an execution environment with python and robot installed on it (on the master or on an agent) (see `Squash TF Robot Framework Runner prerequisites <https://squash-tf.readthedocs.io/projects/runner-robot/en/doc-stable/#prequisites>`_ for more details)
   * Give to this executor the label ``robotFW``. To do so :

     * Click on ``Manage Jenkins`` (left menu)
     * Click on ``Manage Nodes`` (left menu)
     * Select the desired Node
     * Click on ``Configure`` (left menu)
     * In the ``Labels`` field add ``robotFW`` (multiple labels are separated by a space)
     * Click on the ``Save`` button

   You're now ready to execute a RobotFramework job

   .. note:: You may need to wait few minutes before Jenkins recognize this node as having the ``robotFW`` label.



---------------------------------------------
Setting a two-phased Keyword Framework job up
---------------------------------------------

.. to boldly split infinitives that nobody has ever split before !
   |StarTrek (LOL edition)

Most parameters for the two-phased Keyword DSL pipeline are setup through the Environment injector plugin.

.. image:: ../_static/create-job/pipelineParms.png

#. SCM parameters

   The first thing you'll want to set is the SCM part.

   .. image:: ../_static/create-job/pipelineSCM.png


   * ``SCM_CLASS`` : This first version of the pipeline only supports the use of the mercurial SCM. We plan to support other SCM providers
     compatible with the jenkins CSM plugin in the future : as they come up, you'll be able to select other values for the ``SCM_CLASS`` parameter.

   * ``SCM_URL`` : As shown, you may specify a repository URL through the ``SCM_URL`` parameter (the pipeline pulls from branch default).

   * ``SCM_CREDENTIALS`` : This parameter holds a credential identifier. The value must match the ID of a registered jenkins credential record (see below).

     .. image:: ../_static/create-job/jenkinsCredlist.png


#. SQL condition parameters

   The trigger of the second test execution phase is a SQL query. Here is how you may set this condition up :

   .. image:: ../_static/create-job/pipelineSQLcondition.png


   * ``SQL_CONDITION_TARGET`` : this parameter must match the name of a target defined by the Keyword Framework [#dslKwDef]_ test project.

     .. image:: ../_static/create-job/showTAtargDef.png

   * ``SQL_CONDITION_QUERY`` : this parameter defines the SQL Query executed when evaluating the condition. It may be any SQL query that selects a unique value (one column, one row).
     More columns and rows may be fetched but they will be ignored. We advise you to avoid this and only fetch the expected value for clarity.

   * ``SQL_CONDITION_EXPECTED_VALUE`` : this parameter is compared to the query result.
     If they match, the condition is considered fulfilled and test execution resumes for phase two.


#. Selection of phase I and phase II tests
   Phase I and phase II tests are chosen among tests triggered by the received |squashTM| execution order. They are picked using two filters.
   The default filters are, respectively :

   .. csv-table:: 
      :header: "Phase","Filter definition","Selected tests"

      "I","\**/sql-trigger/\*precond.ta","Any test in a sub-directory named sql-trigger with a name ending with ``precond.ta``"
      "II","\**/sql-trigger/\*postcond.ta","Any test in a sub-directory named sql-trigger with a name ending with ``postcond.ta``"

   As of now [#upcomingParameters]_ , the only way to change this is to search the pipeline code for the ``**/sql-trigger/*precond.ta`` and ``**/sql-trigger/*postcond.ta`` strings,
   (each exists in one place only) and edit them. They follow the ant-like wildcard format, where * means 'anything but the file separator, including an emtpy string', and ** means
   'any thing, even path separators'.

   .. image:: ../_static/create-job/phaseIfilter.png

   .. image:: ../_static/create-job/phaseIIfilter.png


.. [#dslKwDef] The **Keyword Framework** is the new version of our testing framework derived from the **Squash**\ |_|\ **TA DSL** framework.

.. [#speakOf2phase] This pipelined job class allows an asynchronous wait between two test phases. Between these two test phases, the pipeline is active but no executor is allocated.
   The trigger for phase II is a SQL query. The current version only allows tests triggered from |squashTM|.

.. [#upcomingParameters] changing the phase test filters will be made easier in the near future. This enhancement is planned for version *2.1.0-RELEASE*,
   which should be published around April 2019.

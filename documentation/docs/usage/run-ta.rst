..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

.. _ta_job_use:

########################################
Use a job based on TA / SKF job template
########################################

.. contents::
   :local:

|

*****************
Selecting the job
*****************

Once you've :ref:`created a job using the Squash-TA Template <create_job>`, select it in the list of jobs available |_|:

    .. container:: image-container

        .. image:: ../_static/run-ta/skf-job-template-select-job.png

On the job's page, you'll be able to launch a build, access various informations regarding the job or make modifications.

.. Note::
   You'll have to be logged in as an administrator to modify your job (rename, delete or configure) or to launch a build.

To launch a build, click on *'Build with Parameters'* |_|:

    .. container:: image-container

        .. image:: ../_static/run-ta/skf-job-template-build-with-parameters.png

|

***********************************
Setting the parameters of the build
***********************************

You can then configure some of the parameters of the build |_|:

    .. container:: image-container

        .. image:: ../_static/run-ta/skf-job-template-build-job.png

-----------------------
Operation and TestList
-----------------------

You can specify two types of goals to execute in the *'operation'* field: **list** or **run**. 

    **> list** |_|: This will generate a *json* file listing all the tests present in your project.
    
    This file is used by |squashTM| in the context of the **TM-TF** link to determine the tests that can be executed.

    **> run** |_|: This will run all the tests specified in the *'testList'* field or in a user created *json* file.

    If you are using the **TM-TF** link, |squashTM| will generate and transmit to |squashTF| a *testsuite.json* file containing the list of tests to execute.
    In that case you don't have to alter the field *'testList'* or specify a *json* file.

    Otherwise, if you want to provide manually to your **run** the list of tests to execute, you can procede in two ways |_|:

        * Enter the relative path (to the *'tests'* folder of your project) of the test files you wish to execute, 
          separated by a comma (and **no space** after the comma), in the *'testList'* field. 
          You can specify the path of a folder containing test files, using |_|: **path/to/tests/\***. 
          You can also have the build execute all the test files (.ta, .txt or .test) in all the subfolders of the *'tests'* folder of you project, 
          using |_|: **\**/\*.{ta, txt or test}**.

        * Provide a *json* type file (by clicking on the *'Choose File'* button on the *'testsuite.json'* line) containing the list of tests you wish to execute,
          and fill the *'testList'* field with |_|: **{\file:testsuite.json}**.

            **Exemple of a user created json file** |_|:

                .. container:: image-container

                        .. image:: ../_static/run-ta/skf-job-template-test-tree.png

--------
Executor
--------

If you want to launch the build on an |squashTF| agent located on a distant machine and properly configured 
(see :ref:`this page <execution.agent.install.anchor>` for the agent installation), 
enter the exact name of the agent, or its label, in the *'executor'* field.
Click on the *'Show nodes'* button on the bottom right of the field to validate that you've entered a correct name |_|:

    .. container:: image-container

        .. image:: ../_static/run-ta/skf-job-template-choose-executor.png

|

*****************
Launching a build
*****************

Once you've specified the parameters of your build, click on the *'Build'* button to launch the build.

Clicking on the dot (grey, red or green depending on the status of the build) next to the build name in the *'Build History'* window will show the console output.

    .. container:: image-container

        .. image:: ../_static/run-ta/skf-job-template-build-list-building-steps.png

|

*************
Build outputs
*************

---------
Test list
---------

After the first **list** build has been performed, the generated *'Test_list'* of the last **list** build executed will be available on the job's page |_|:

    .. container:: image-container

        .. image:: ../_static/run-ta/skf-job-template-test-list-after-build.png

This is the *json* file fetched by |squashTM| if using the **TM-TF** link.

-----------
HTML report
-----------

In the same manner, the generated *'Squash_TA_HTML_Report'* of the last **run** build executed will be available on the job's page |_|:

    .. container:: image-container

        .. image:: ../_static/run-ta/skf-job-template-html-report-after-build.png

If you click directly on a **run** build's name, you'll have access to its page with the corresponding **run**'s HTML report |_|:

    .. container:: image-container

        .. image:: ../_static/run-ta/skf-job-template-build-select.png

|

----------

.. Note::
   For more indepth details about the Squash Keyword Framework, please consult its `dedicated section <https://squash-tf.readthedocs.io/projects/skf/en/doc-stable/>`_.

|

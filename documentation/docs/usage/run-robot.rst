..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

.. _robotframework_job_use:

######################################################
Use a job based on Robot Framework runner job template
######################################################

.. contents::
   :local:

|

*************
Execute a job
*************

-----------------
Launching a build
-----------------

Once you've :ref:`created a job using the Squash TF Robot Framework job template <create_job>`, select it in the list of available jobs.

On the job's page, you'll be able to launch a build, access various informations regarding the job or make modifications.

To launch a build, click on ``Build with Parameters`` |_|:

  .. image:: ../_static/run-robot/sample-robot-ini.png
      :scale: 70%

|

-----------------------------------
Setting the parameters of the build
-----------------------------------

You can then configure some of the parameters of the build |_|:

  .. image:: ../_static/run-robot/robot-job-param.png
      :scale: 70%
      :align: center

> Operation and TestList
========================

You can specify two types of goals to execute in the ``operation`` field |_|: **list** or **run**.

    * **list** |_|: This will generate a *json* file listing all the tests present in your project.
      This file is used by |squashTM| in the context of the **TM-TF** link.

    * **run** |_|: This will run all the tests specified in the ``testList`` field or in a user created *json* file.
      If you are using the **TM-TF** link, |squashTM| will generate and transmit to |squashTF| a *testsuite.json* file containing the list of tests to execute.
      In that case you don't have to alter the field ``testList`` or specify a *json* file.

      Otherwise, if you want to provide manually to your **run** the list of tests to execute, you can procede in two ways |_|:

        * Enter the path to the tests cases you wish to execute, separated by a semicolon (and **no space** after the semicolon), in the ``testList`` field.
          For the path, use the test suite and test case name provided in the json test list (generated by the **list** goal) to prevent problems.
          You can also execute all the tests present in you project by using in the ``testList`` field |_|: **\**/\***.

        * Provide a *json* type file (by clicking on the ``Choose File`` button on the ``testsuite.json`` line) containing the list of tests you wish to execute,
          and fill the ``testList`` field with |_|: **{\file:testsuite.json}**.


> Executor
==========

If you want to launch the build on specific |squashTF| agent executor, enter the exact name of the agent, or its label, in the ``executor`` field.
In this robot framework template job, a default label ``robotFW`` is set by default (see :ref:`Robot Framework job setup <robot_job_setup>` for explanation on this default label).
Click on the ``Show nodes`` button on the bottom right of the field to validate that you've entered a correct name |_|:

  .. image:: ../_static/run-robot/robot-executor.png
     :scale: 70%
     :align: center

|

---------------
Start the build
---------------

Once you've specified the parameters of your build, click on the *'Build'* button to launch the build.

Clicking on the dot (grey, red or green depending on the status of the build) next to the build name in the *'Build History'* window will show the console output.

  .. image:: ../_static/run-ta/skf-job-template-build-list-building-steps.png
     :align: center

|

*************
Build outputs
*************

---------
Test list
---------

After the first build of type **list** has been performed, the generated **Test_list** of the last **list** build executed will be available on the job's page |_|:

  .. image:: ../_static/run-robot/robot-list-report.png
      :scale: 70%
      :align: center

This is the *json* file fetched by |squashTM| if using the **TM-TF** link.

------------
HTML reports
------------

In the same manner, the **Squash_TF_HTML_Report** generated by the last **run** build executed  will be available on the job's page |_|:

  .. image:: ../_static/run-robot/robot-run-report.png
      :scale: 65%
      :align: center


Those run html reports are also accessible directly in the **run** build's page ( which is convenient for the historization) |_|:

  .. image:: ../_static/run-robot/robot-run-report-direct.png
      :scale: 70%
      :align: center


|

----------

.. Note::
   For more indepth details about the Robot Framework runner, please consult its `dedicated section <https://squash-tf.readthedocs.io/projects/runner-robot/en/doc-stable/>`_.

|

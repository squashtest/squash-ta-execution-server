..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.


.. _link_tm_tf_configure_tm:


############
Configure TM
############

.. contents::
   :local:

.. _TM_callback:

Configure Squash\ |_|\ TM properties
------------------------------------

1. Open the ``squash.tm.cfg.properties`` file located in the conf folder of Squash TM installation folder (ex: C:\\Squash-TM\\conf).
#. Find the line with ``tm.test.automation.server.callbackurl`` and uncomment it
#. Add the url of |squashTM| (ex: http://192.168.2.138:8080/squash). This url will be used by |squashTF| Server to notify |squashTM| of the execution progress.
#. Restart |squashTM|.

.. warning::
   The |squashTM| URL used in this configuration should be accessible from  |squashTF| Server location

.. warning::
   If you update this |squashTM| callback URL, then don't forget to update all the |squashTF| execution server using this :ref:`endpoint <TM_TM_endpoint>`

|

Create a ``Test Automation Server`` user
----------------------------------------

In |squashTM| there is a special user group called ``Test Automation Server`` which has the right to send automated test execution feedback to TM from the outside.
To link a |squashTM| server with a |squashTF| server, you have to create a user which belong to this special group. To do so :

1. In |squashTM|, click on the link ``[Administration]`` (in the upper corner) then click on ``[Users]``.
#. Add a new user with the button ``[Add New User]``. The ``[Add User]`` popup shows up :

    .. image:: ../_static/tm-tf-link/ajouter_user_serveur_EN.jpg
       :align: center

#. In the ``Group`` ComboBox choose ``Test Automation Server``.
#. Fill the ``Login`` field with the login you have configured in the |squashTF| ``conf.properties`` :ref:`file <TM_TM_endpoint>`.
#. Fill the ``Password`` field with the password you configured in the |squashTF| ``conf.properties`` :ref:`file <TM_TM_endpoint>`.

.. warning::
   The credentials of this user is used on |SquashTF| side to define the :ref:`endpoint in the conf.properties <TM_TM_endpoint>`.
   If you update the credentials of this user, then don't forget to update the execution server endpoint which use it.

|

Add a Squash\ |_|\ TF Execution Server to Squash\ |_|\ TM
---------------------------------------------------------

#. In |squashTM|, click on the link ``[Administration]`` (in the upper corner) then click on ``Automation servers``
#. Add a new server with the button ``[Add a server]``
#. The ``[New test automation server]`` popup shows up :

    .. image:: ../_static/tm-tf-link/Ajout_serveur_auto_EN.jpg
       :align: center

#. Fill the ``URL`` field with the |squashTF| url (ex : http://192.168.2.138:9080/jenkins).
#. Fill the ``Login`` field with the login of the user in |squashTF| dedicated to automation.
#. Fill the ``Password`` field with the password of the user in |squashTF| dedicated to automation.
#. If you use a Master server with slave(s), you can check the ``Manually choose the server at execution lanching (Implies this server is a master one)`` checkbox.
   This option will allow you to choose, each time you run your tests, on which agent they run.
#. If you want to add another server, click ``[Add another]`` and repeat steps 4-8, otherwise click ``[Add]``.

   .. Attention:: Login must be unique for each URL.

|

Configure a Project to use automation
-------------------------------------

**> Link an Automated Server to a Project**

#. Click on the link ``[Administration]`` (in the upper corner) then click on ``[Projects]``.
#. Select an existing project, scroll down to ``Test automation management``.

   .. image:: ../_static/tm-tf-link/test_auto_management_no_server_EN.jpg
      :align: center

#. Click on ``No server``. You'll see the list of available servers.

   .. image:: ../_static/tm-tf-link/test_auto_management_choix_server_EN.jpg
      :align: center

#. Choose the server and ``[Confirm]``.
#. In the ``Test automation management`` you should see a new part called ``Jobs``.

|

**> Link a** |squashTF| **job to a** |squashTM| **project**

#. Click on the link ``Administration`` (in the upper corner) then click on ``Projects``.
#. Select an existing project with an automation server associated, scroll down to ``Test automation management``.

   .. image:: ../_static/tm-tf-link/test_auto_management_no_job_EN.png
      :align: center

#. Click on ``[+]``. The New job popup will shows up with all the jobs you have in |squashTF|.

   .. image:: ../_static/tm-tf-link/Ajouter_job_a_un_projet_EN.png
      :align: center

#. Select the job(s) you want to add. You can change their label in |squashTM| (by default the name are the associated |squashTF| job name).

   .. image:: ../_static/tm-tf-link/new_job_EN.png
      :align: center

   .. Attention:: Job's name can't be blank and must be unique.

#. Click ``[Confirm]``.

#. You can edit your job (pencil button) if you want to further configure it.

   .. image:: ../_static/tm-tf-link/edit_job_EN.png
      :align: center

#. In the field ``Possible execution servers``, you can choose which ``slaves`` servers can be used for this job. Type their name separated with semicolon (ex: SlaveServ1; SlaveServ2).

   .. Attention:: The Possible execution servers field is only for SLAVES servers, the master will alway be displayed in the list of possible servers.

**> Enable Gherkin execution for a job**

When you linked a |squashTF| job to a |squashTM| project, by default in the job configuration in TM, the ``Can run Gherkin`` option is set to No. In order to this job can run gherkin you have to enable it.

#. In the project administration page section ``Test automation management``, edit your job (pencil button) :

   .. image:: ../_static/tm-tf-link/edit_job_conf.png
      :align: center

#. Check the box ``Can run gherkin``

   .. image:: ../_static/tm-tf-link/add_run_Gherkin.png
      :align: center

#. Click on ``Confirm``

   .. image:: ../_static/tm-tf-link/run_Gherkin.png
      :align: center

Now the project is related to |squashTF| server. In this project you can relate automated test script to TM test cases. You can then execute these automated tests from the campaign space and read their execution results.

..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.



#############################################
Associate Squash\ |_|\ TM and Squash\ |_|\ TF
#############################################

********
Overview
********

|squashTF| could be used as |squashTM| automated test execution server.

The process is as follows:

* In |squashTM|, select a list of test case and launch the execution
* |squashTM| ask |squashTF| to execute a list of automated tests
* |squashTF| execute the list of test
* |squashTF| generate a report
* |squashTF| send back status and report to TM
* Results are then available in TM

Of course as prerequisite, you should have created the automated tests and associate them to the |squashTM| test cases you request the execution.

Before using this process some configuration is needed on both part


**************************
Configure the TM - TF Link
**************************

.. toctree::
   :hidden:
   :maxdepth: 0

   configure-tf.rst
   configure-tm.rst


* **Configure Squash TF:** :ref:`link_tm_tf_configure_tf`

* **Configure Squash TM:** :ref:`link_tm_tf_configure_tm`



..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

.. _link_tm_tf_configure_tf:


############
Configure TF
############

.. contents::
   :local:

**************************************************************
Create a user with the rights to launch job execution remotely
**************************************************************

On |squashTF| **Execution Server** you have to create a user (ex : tmLauncher) able to launch remotely the execution of the job(s) who handle the tests.

* Login with a user having admin rights on your |squashTF| **Execution Server**
* Go to ``Manage Jenkins \ Manage Users``
* Click on ``Create User`` in the left menu
* Fill the field :

   .. image:: ../_static/tm-tf-link/tfCreateUser.png
      :align: center

* Click on the button ``Create User``

.. Note::
   This user credentials will be needed when you will define this automation server in |squashTM|. Keep them near you.

.. Warning::
   By default ``Logged-in users can do anything`` strategy is selected for access control in Jenkins. You may want to use another access control strategy.
   To do so, go to ``Manage Jenkins \ Configure Global Security`` section ``Access Control``.
   In such a case, don't forget to give the rights to launch job execution remotely to the user we've just created.


.. _TM_TM_endpoint:

************************************
Configuring Squash\ |_|\ TM callback
************************************


When |squashTM| asks the execution of automated tests to |squashTF| Execution Server, the |squashTM| request also contains it's callback URL.

|squashTF| **Execution Server** use this callback URL in two ways :

* As base url to send to |squashTM| feedback about the progress of the execution
* To retrieve the right credentials to use to send feedback to |squashTM|

On |squashTF| **Server** side, the association between the callback URL and the credentials is done in a properties file. To modify this file:

* In your jenkins interface go to ``Manage jenkins / Managed files``
* Edit the file taLinkConf.properties

.. _link_tm_tf_conf.properties:

.. image:: ../_static/tm-tf-link/edit_conf.properties.png
   :align: center

* In this file, you should define an endpoint for each TM Server using this |squashTF| **Execution Server**.
  For each endpoint you have to define the |squashTM| callback URL and the credentials of a |squashTM| User authorized to send feedback to |squashTM|.
  For example a TM Server which callback URL is : http://myServer:8080/squash and the credentials are tmFeedbackLogin / tmFeedbackPassword then you should define the three properties below: ::

    endpoint.1=http://myServer:8080/squash
    endpoint.1.login=tmFeedbackLogin
    endpoint.1.password=tmFeedbackPassword


.. Note:: The credentials associated to the callback URL are those of a user define in |squashTM|. This Squash\ |_|\ TM user should belong to the ``Test Automation Server`` group in order to have right to send feedback to Squash TM. See the TM configuration page for more details.

.. Warning::
   The callbackURL define in Squash\ |_|\ TM :ref:`configuration file <TM_callback>` should **perfectly** match the one define in your endpoint.
   If you wonder why, the answer is Squash\ |_|\ TM put the callbackURL from its configuration file in the execution request it sends to Squash\ |_|\ TF Execution Server.
   And this last one, use it to retrieve the good endpoint. So if they not match, no endpoint is found.

***********************
Configuring JENKINS_URL
***********************

Squash\ |_|\ TF provides the URL of the execution report to Squash\ |_|\ TM. To do so, the JENKINS_URL variable should be set as follows:

* Go to "Manage Jenkins / Configure system"
* In the "Jenkins location" section, set the correct value for the "Jenkins URL" property
* Click on "Save" or "Apply"

.. note::

   When you first start Jenkins, even if the "JENKINS_URL" field contains the right value, you have to click on "Save" or "Apply". Otherwise the JENKINS_URL property won't be set.

.. todo: Est-ce que cette note est encore valide ?
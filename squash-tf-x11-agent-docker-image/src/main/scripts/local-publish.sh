#!/bin/sh
#
#     This file is part of the Squashtest platform.
#     Copyright (C) 2011 - 2020 Henix
#
#     See the NOTICE file distributed with this work for additional
#     information regarding copyright ownership.
#
#     This is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Lesser General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     this software is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Lesser General Public License for more details.
#
#     You should have received a copy of the GNU Lesser General Public License
#     along with this software.  If not, see <http://www.gnu.org/licenses />.
#

if [ $# -lt 1 ]; then
	version=${project.version}
else
	version=$1
fi

if [ $# -lt 2 ]; then
	if [ "$IS_RELEASE" = "true" ]; then
		tag=LATEST_RELEASE
	else
		tag=LATEST_SNAPSHOT
	fi
else
	tag=$2
fi

# Guard to avoid naming collision while building on a non cleaned workspace.
# This situation can arrise when building using pipeline reactor on a single agent. 

if [ -f target/squash-tf-x11-execution-agent.docker.${version}.tar ]; then 
    rm target/squash-tf-x11-execution-agent.docker.${version}.tar
fi

if [ -f target/squash-tf-x11-execution-agent.docker.${tag}.tar ]; then 
    rm target/squash-tf-x11-execution-agent.docker.${tag}.tar ar
fi

docker save -o target/squash-tf-x11-execution-agent.docker.${version}.tar squash/squash-tf-x11-execution-agent:${version}

# don't waste resources by docker save-ing the tag tar which is bit-identical to the versionned tar
# It will also be transferred only once to the repository then hardlinked there, 
# saving computation resources, I/O bandwidth and build time.
ln target/squash-tf-x11-execution-agent.docker.${version}.tar target/squash-tf-x11-execution-agent.docker.${tag}.tar 
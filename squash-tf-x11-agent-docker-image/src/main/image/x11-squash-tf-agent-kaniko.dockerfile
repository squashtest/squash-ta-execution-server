#
#     This file is part of the Squashtest platform.
#     Copyright (C) 2011 - 2020 Henix
#
#     See the NOTICE file distributed with this work for additional
#     information regarding copyright ownership.
#
#     This is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Lesser General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     this software is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Lesser General Public License for more details.
#
#     You should have received a copy of the GNU Lesser General Public License
#     along with this software.  If not, see <http://www.gnu.org/licenses />.
#

ARG TF_AGENT_VERSION
FROM docker.squashtest.org/squash-tf-1/squash-tf-execution-agent:${TF_AGENT_VERSION}

USER root

RUN apt-get update; apt-get clean

# Install x11vnc.
RUN apt-get install -y x11vnc

# Install xvfb.
RUN apt-get install -y xvfb

# Install fluxbox.
RUN apt-get install -y fluxbox

# Install wget.
RUN apt-get install -y wget

# Install wmctrl.
RUN apt-get install -y wmctrl
USER ${JENKINS_USER}

COPY x11-bootstrap.sh ${HOME}/scripts/x11-bootstrap.sh
COPY x11-jenkins-slave.sh ${HOME}/scripts/x11-jenkins-slave.sh 

USER root

RUN chmod +x ${HOME}/scripts/x11-bootstrap.sh \
  && chmod +x ${HOME}/scripts/x11-jenkins-slave.sh \
  && ln -f ${HOME}/scripts/x11-jenkins-slave.sh /usr/local/bin/jenkins-slave

EXPOSE 5900

USER ${JENKINS_USER}



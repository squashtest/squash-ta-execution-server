#!/bin/bash
#
#     This file is part of the Squashtest platform.
#     Copyright (C) 2011 - 2020 Henix
#
#     See the NOTICE file distributed with this work for additional
#     information regarding copyright ownership.
#
#     This is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Lesser General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     this software is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Lesser General Public License for more details.
#
#     You should have received a copy of the GNU Lesser General Public License
#     along with this software.  If not, see <http://www.gnu.org/licenses />.
#

# Usage x11-jenkins-slave.sh [options] -url http://jenkins [SECRET] [AGENT_NAME] .
#
# This script is the entry point of the Squash Execution Server X11 agent. 
# The agent provides a viable Jlnp Jenkins agent, which can run Squash-TA automation scripts. 
# It also provides a X Virtual Frame Buffer as X11 server, and a Fluxbox window manager.
# This agent can serve as a basis for GUI testing agents that are able to run in a headless
# manner browsers for instance.
# 
# Options are :
#     * Jenkins slave launch options.
#     * --x11-vnc-debug : This options is for debugging purposes and launches a vncserver
# in the background listening to the port 5900. This allows the remote access of the X session
# by a vnc client if port 5900 is exposed.

# First bootstrap X11
source $HOME/scripts/x11-bootstrap.sh
# Then call standard jenkins slave bootstrap script 
$HOME/scripts/std-jenkins-slave.sh "$@"

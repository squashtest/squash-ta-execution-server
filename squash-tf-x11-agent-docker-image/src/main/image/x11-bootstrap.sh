#!/bin/bash

# MIT License
#
# Copyright (c) 2018 Stephen Fox Jr.
# Modified by Henix 2018
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Based on: http://www.richud.com/wiki/Ubuntu_Fluxbox_GUI_with_x11vnc_and_Xvfb
#
# Original source code can be found at https://github.com/stephen-fox/chrome-docker
# Modified by Henix 2018

readonly G_LOG_D='[DEBUG]'
readonly G_LOG_I='[INFO]'
readonly G_LOG_W='[WARN]'
readonly G_LOG_E='[ERROR]'

# For debugging purpose. Holds the position of the --x11-vnc-debug option in the argument list if used.
# Set this to a number greater than 1 to enable VNC server at bootstrap. 
X11_VNC_DEBUG=-1

setup() {
    echo "${G_LOG_I} launching X11 bootstrap"
    parse_debug_args "$@"
    launch_xvfb
    launch_window_manager
    run_vnc_server
}

parse_debug_args() {
echo "Parsing args $@"

ARG_POS=0;
while [ $# -ge 1 ]
do
    ARG_POS=$((ARG_POS+1))
    case $1 in
	--x11-vnc-debug)
	    X11_VNC_DEBUG=$ARG_POS
	    shift
	    ;;
        *)
	    shift
	    ;;
    esac
done

}

launch_xvfb() {
    # The last run has most probably let us som dreggs to trip on...
    if [ -f /tmp/.X1-lock ]; then
        rm /tmp/.X1-lock
    fi

    # Set defaults if the user did not specify envs.
    echo "${G_LOG_I} Starting X Virtual Frame Buffer"
    export DISPLAY=${XVFB_DISPLAY:-:1}
    local screen=${XVFB_SCREEN:-0}
    local resolution=${XVFB_RESOLUTION:-1280x1024x24}
    local timeout=${XVFB_TIMEOUT:-5}

    # Start and wait for either Xvfb to be fully up or we hit the timeout.
    Xvfb ${DISPLAY} -screen ${screen} ${resolution} -nolisten unix &
    local loopCount=0
    until xdpyinfo -display ${DISPLAY} > /dev/null 2>&1
    do
        loopCount=$((loopCount+1))
        sleep 1
        if [ ${loopCount} -gt ${timeout} ]
        then
            echo "${G_LOG_E} xvfb failed to start."
            exit 1
        fi
    done
}

launch_window_manager() {
    echo "${G_LOG_I} Starting fluxbox window manager"
    local timeout=${XVFB_TIMEOUT:-5}

    # Start and wait for either fluxbox to be fully up or we hit the timeout.
    fluxbox &
    local loopCount=0
    until wmctrl -m > /dev/null 2>&1
    do
        loopCount=$((loopCount+1))
        sleep 1
        if [ ${loopCount} -gt ${timeout} ]
        then
            echo "${G_LOG_E} fluxbox failed to start."
            exit 1
        fi
    done
}

run_vnc_server() {
    if [ "${X11_VNC_DEBUG}" -ge 1 ];
    then
        echo "${G_LOG_D} Starting debug vnc server"
        local passwordArgument='-nopw'

        if [ -n "${VNC_SERVER_PASSWORD}" ]
        then
            local passwordFilePath="${HOME}/x11vnc.pass"
            if ! x11vnc -storepasswd "${VNC_SERVER_PASSWORD}" "${passwordFilePath}"
            then
                echo "${G_LOG_E} Failed to store x11vnc password."
                exit 1
            fi
            passwordArgument=-"-rfbauth ${passwordFilePath}"
            echo "${G_LOG_I} The VNC server will ask for a password."
        else
            echo "${G_LOG_W} The VNC server will NOT ask for a password."
        fi

        x11vnc -display ${DISPLAY} -forever ${passwordArgument} -bg
    fi
}

setup "$@"

# Do a little bit of cleaning up to avoid messing with possibly other scripts...
if [ "${X11_VNC_DEBUG}" -ge 1 ]
then
    echo "${G_LOG_D} Removing option --x11-vnc-debug located at ${X11_VNC_DEBUG} "
    set -- ${@:1:$(($X11_VNC_DEBUG-1))} ${@:$(($X11_VNC_DEBUG+1)):$(($#-1))}
    echo "${G_LOG_D} Cleaned parameters $@"
fi

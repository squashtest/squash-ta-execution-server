# MIT License
# 
# Copyright (c) 2018 Stephen Fox Jr.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Original source code can be found at https://github.com/stephen-fox/chrome-docker
# Modified by Henix 2018-2019
ARG TF_AGENT_VERSION
FROM squash/squash-tf-execution-agent:${TF_AGENT_VERSION}

USER root

RUN apt-get update; apt-get clean

# Install x11vnc.
RUN apt-get install -y x11vnc

# Install xvfb.
RUN apt-get install -y xvfb

# Install fluxbox.
RUN apt-get install -y fluxbox

# Install wget.
RUN apt-get install -y wget

# Install wmctrl.
RUN apt-get install -y wmctrl
USER ${JENKINS_USER}

COPY x11-bootstrap.sh ${HOME}/scripts/x11-bootstrap.sh
COPY x11-jenkins-slave.sh ${HOME}/scripts/x11-jenkins-slave.sh 

USER root

RUN chmod +x ${HOME}/scripts/x11-bootstrap.sh \
  && chmod +x ${HOME}/scripts/x11-jenkins-slave.sh \
  && ln -f ${HOME}/scripts/x11-jenkins-slave.sh /usr/local/bin/jenkins-slave

EXPOSE 5900

USER ${JENKINS_USER}



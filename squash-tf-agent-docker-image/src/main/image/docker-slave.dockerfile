# The MIT License
#
#  Copyright (c) 2015-2017, CloudBees, Inc. and other Jenkins contributors
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in
#  all copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#  THE SOFTWARE.
#
#  Orignial source file can be found at https://github.com/jenkinsci/docker-jnlp-slave
#  Modified by Henix 2018
FROM openjdk:8u171-jdk-stretch

ARG TF_AGENT_VERSION 

ARG user=jenkins
ARG group=jenkins
ARG uid=10000
ARG gid=10000

ENV JENKINS_USER ${user}
ENV HOME /home/${user}

RUN groupadd -g ${gid} ${group}
RUN useradd -c "Jenkins user" -d $HOME -u ${uid} -g ${gid} -m ${user}

LABEL Description="This image aims to  provide a viable Squash Execution Server agent which connects Jenkins via JNLP protocols" Vendor="Squash TF Project" Version=${TF_AGENT_VERSION}

ARG AGENT_WORKDIR=/home/${user}/agent

RUN mkdir /usr/share/jenkins
COPY agent.jar /usr/share/jenkins/slave.jar
RUN chmod 755 /usr/share/jenkins \
  && chmod 644 /usr/share/jenkins/slave.jar

RUN mkdir /home/${user}/scripts

COPY jenkins-slave /home/${user}/scripts/std-jenkins-slave.sh
RUN chmod +x /home/${user}/scripts/std-jenkins-slave.sh \
   && ln -s /home/${user}/scripts/std-jenkins-slave.sh /usr/local/bin/jenkins-slave


RUN mkdir -p /usr/share/maven /usr/share/maven/ref  

COPY apache-maven /usr/share/maven

RUN chmod -R 744 /usr/share/maven ; chmod 755 /usr/share/maven/bin/mvn ; find /usr/share/maven -type d -exec chmod a+x {} \; ;ln -s /usr/share/maven/bin/mvn /usr/bin/mvn

ENV MAVEN_HOME /usr/share/maven
ENV MAVEN_CONFIG "$HOME/.m2"


RUN mkdir /home/${user}/.jenkins && mkdir -p ${AGENT_WORKDIR}

RUN chown -R ${user}:${user} /home/${user}/.jenkins ; chmod 755 -R /home/${user}/.jenkins ; 
RUN chown -R ${user}:${user} ${AGENT_WORKDIR} ; chmod 755 -R ${AGENT_WORKDIR} ; 

USER ${user}

ENV AGENT_WORKDIR ${AGENT_WORKDIR}
ENV SQUASH_TA_HOME ${AGENT_WORKDIR}
ENV JENKINS_HOME ${AGENT_WORKDIR}

VOLUME /home/${user}/.jenkins
VOLUME ${AGENT_WORKDIR}
WORKDIR /home/${user}

ENTRYPOINT ["jenkins-slave"]
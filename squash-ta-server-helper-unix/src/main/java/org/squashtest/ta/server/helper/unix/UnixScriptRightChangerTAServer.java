/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.server.helper.unix;

import java.io.IOException;
import org.squashtest.ta.custom.izpack.components.helpers.UnixScriptRightChanger;

import com.izforge.izpack.panels.process.AbstractUIProcessHandler;

public class UnixScriptRightChangerTAServer extends UnixScriptRightChanger {
	
	
	@Override
	public void run(AbstractUIProcessHandler handler, String[] args) throws IOException {
		//INFO LOG OUTPUT in Izpack Process Panel
		String langue = args[0].toString();
		String installPath = args[1].toString();
		if (("fra").equals(langue)) {
			handler.logOutput("\u2022Les raccourcis de Squash-TF Serveur sont créés et placés dans :\n"
					+ "   \u25E6" +  installPath + "/shortcuts\n\n"
					+ "\u2022Vous pouvez les placer sur votre Bureau pour une utilisation ultérieure.", false);
		} else {
			handler.logOutput("\u2022The Squash-TF Server shortcuts are created and placed in:\n"
					+ "   \u25E6" +  installPath + "/shortcuts\n\n"
					+ "\u2022You may want to place them to your Desktop for further use.", false);
		}
		
		noInputArgWarning(args, 1);
		
		callMarkScripts(1, args, "u+x");		
	}
	
}

# MIT License
# 
# Copyright (c) 2018 Stephen Fox Jr.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Original source code can be found at https://github.com/stephen-fox/chrome-docker
# Modified by Henix 2018
ARG TF_AGENT_VERSION
FROM squash/squash-tf-x11-execution-agent:${TF_AGENT_VERSION}

ARG CHROME_MAJOR_VERSION=71

USER root

# Set the Chrome repo.
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
    && echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list

# Install Chrome - Fixed major version for selenium driver compatibility purpose
RUN VER=${CHROME_MAJOR_VERSION} apt-get update \
    && export CHROME_VERSION=$(apt-cache show google-chrome-stable | grep "Version: $VER" | sed 's/Version: //g' | head -n 1) \
    && echo 'Installation of Google Chrome' $CHROME_VERSION \
    && apt-get -y install google-chrome-stable=$CHROME_VERSION

#Install Chromedriver - Fixed version (hte one given in third parties lib) for Chrome binary compatibility
COPY chromedriver.zip /tmp/chromedriver.zip

RUN unzip /tmp/chromedriver.zip \
  && mv chromedriver /usr/local/bin/ \
  && rm /tmp/chromedriver.zip

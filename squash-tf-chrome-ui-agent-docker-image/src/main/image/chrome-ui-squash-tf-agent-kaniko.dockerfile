#
#     This file is part of the Squashtest platform.
#     Copyright (C) 2011 - 2020 Henix
#
#     See the NOTICE file distributed with this work for additional
#     information regarding copyright ownership.
#
#     This is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Lesser General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     this software is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Lesser General Public License for more details.
#
#     You should have received a copy of the GNU Lesser General Public License
#     along with this software.  If not, see <http://www.gnu.org/licenses />.
#

ARG TF_AGENT_VERSION
FROM docker.squashtest.org/squash-tf-1/squash-tf-x11-execution-agent:${TF_AGENT_VERSION}

ARG CHROME_MAJOR_VERSION=71

USER root

# Set the Chrome repo.
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
    && echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list

# Install Chrome - Fixed major version for selenium driver compatibility purpose
RUN VER=${CHROME_MAJOR_VERSION} apt-get update \
    && export CHROME_VERSION=$(apt-cache show google-chrome-stable | grep "Version: $VER" | sed 's/Version: //g' | head -n 1) \
    && echo 'Installation of Google Chrome' $CHROME_VERSION \
    && apt-get -y install google-chrome-stable=$CHROME_VERSION

#Install Chromedriver - Fixed version (hte one given in third parties lib) for Chrome binary compatibility
COPY chromedriver.zip /tmp/chromedriver.zip

RUN unzip /tmp/chromedriver.zip \
  && mv chromedriver /usr/local/bin/ \
  && rm /tmp/chromedriver.zip
